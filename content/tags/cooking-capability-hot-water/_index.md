+++
title = "Cooking Capability: Hot Water"
default-hide-tag.cooking-capability-hot-water= false
+++

Pages in Adaptable Cooking listed here are only relevant if you have access to drinkable hot water. Note that access to a kettle implies that you have access to hot water (as boiling water can be cooled down).
