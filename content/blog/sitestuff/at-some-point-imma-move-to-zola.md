---
title: "At Some Point I'm Going to Move to Zola"
date: 2022-06-20T00:02:41+01:00
tags: ["site-meta", "programming", "rust", "golang"]
finalised: true
---

In the previous blog post - {{< tref "a-weird-hugo-bug-i-found" />}} - I discovered an awful, awful bug that I have no idea of the cause.

This, combined with other issues, means that at some point I intend to switch this site from Hugo, to [Zola](https://www.getzola.org), which is written in Rust. In major part this is a frustration with my difficulties at both understanding and debugging errors with hugo especially once you start digging into source code, but also my frustration at my own inability to extend the site with highly customised logic for template ordering and such, that is much more doable in Rust due to the clean abstractions and, well, the fact I can actually usefully work in Rust.

Right now, if you take a look at my git repository for this site, you'll see that my theme has an absolutely horrific, abominable setup where I essentially re-create function argument registers and return registers using Scratch variables. This is not something I would recommend to anyone with any sense, but I was set on using hugo when I designed them.

However, I think it's getting to the point where the language hugo is written in is a serious obstacle to my further refinement of this site, and more notably, the poor abstractions in go make navigating the codebase very difficult for a newcomer when trying to diagnose errors (and makes it easier to introduce them as well). This is not shade on the Hugo team at all, they do an excellent job, but the structure of GoLang itself, in my view, provides significant obstacles to both robust modifications and maintaining correctness, along with readability.

So, at somepoint in the future, I intend to switch my site over to Zola, which is like Hugo but written in Rust (with a much less complex template system as well). This will happen whenever I have the time - right now I'm very busy with a number of things, but hopefully my economic and social stressors will reduce with time and I can work on that properly. Hopefully this will allow me to implement functionality in a proper programming language, that I am actually comfortable in, rather than doing horrific and disgusting things with the template system - and hopefully I will not get any more extremely strange and obscure errors!
