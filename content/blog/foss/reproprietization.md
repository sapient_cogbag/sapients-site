---
title: "reproprietization"
tags: ["terminology", "foss", "philosophy", "technology"]
type: "word"
draft: false
---
"Reproprietization" is a term I came up with for the process by which open-source software can be recontained in proprietary silos or otherwise be used in proprietary products. In my view, it is in many ways a form of reterritorialization.
