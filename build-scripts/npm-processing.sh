#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
#IFS works much better with bash arrays nya

set -euo pipefail
IFS=$'\n\t'

# This script exists for doing  npm-related things. On running it always checks for
# the presence of npm nya.
# Note that working directory is hugo root so we must use paths relative to that
source build-scripts/little-utils.sh

# Allow for obtaining nonzero ret values without crashing nya.
set +e
which npm
NPMWHICH_RET=$?
set -e

if [[ ! $NPMWHICH_RET -eq 0 ]]; then
    log "No NPM found - this is a required dependency for katex preparsing"
    exit 1
fi

# Do an npm-install for the current "project" to get katex into a local `node_modules` nya
function npm_install_components {
    log "Running npm install to ensure the presence of katex - will create a node_modules folder if not present."
    npm install
}

# Ensure that appropriate katex files are copied over into assets/katex/ nya, or
# just present where needed.
function npm_ensure_katex {
    log "Ensuring existence of ./node_modules/.bin/katex"
    if [[ ! -e './node_modules/.bin/katex' ]]; then
        log "File not found."
        return 1
    fi
    log "This script uses the --reflink=auto option for space and time savings."

    log "KaTeX built and ready. Preparing to copy files over into assets/katex/ when necessary"
    mkdir -p "assets/katex"
    cp -p -v -u --reflink=auto "node_modules/katex/dist/katex.min.js" "assets/katex"
    cp -p -v -u --reflink=auto "node_modules/katex/dist/katex.min.css" "assets/katex"
    cp -p -v -R -u --reflink=auto "node_modules/katex/dist/contrib" "assets/katex"

    log "Preparing fonts to copy over to static/katex/fonts"
    mkdir -p "static/katex/fonts"
    cp -p -v -R -u --reflink=auto "node_modules/katex/dist/fonts" -T "static/katex/fonts"

    log "Done ensuring katex is set up."
}
