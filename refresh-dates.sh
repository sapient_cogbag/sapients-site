#!/usr/bin/env bash 
#
# Call out to the date updater :) nya
# See: https://longair.net/blog/2011/04/09/missing-git-hooks-documentation/
# This finds files that have been added or modified and then scans through them for dates meow
git diff-index --ignore-space-change --name-only --diff-filter=AM HEAD 'content/*' | python3 "./build-scripts/apply-file-date-update.py"
