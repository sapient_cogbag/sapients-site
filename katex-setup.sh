#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
#IFS works much better with bash arrays nya
set -euo pipefail
IFS=$'\n\t'

source build-scripts/little-utils.sh
log "Running this script ensures KaTeX is set up properly and need only be run after a katex update."
log "Any run of build-and-deploy.sh will have the same effect."

check_for_hugo_root
source build-scripts/npm-processing.sh
npm_install_components
npm_ensure_katex
