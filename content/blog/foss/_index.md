---
title: "The Free and Open Source Software Section"
draft: false
tags: ["foss", "technology"]
flatten: true
shorttitle: "FOSS"
cascade:
    tags: ["foss", "technology"]
---
This section of the site is dedicated to blogposts primarily about Free and Open Source Software and related topics.

In case you are unaware, FOSS software is software in which the source code is available (generally upon request), and free to 
modify and redistribute.
