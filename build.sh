#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
#IFS works much better with bash arrays nya

# SOURCE this file then call generate_hugo_html
# The environment variable $SAPIENTS_SITE_BASE_URL will be used as the base URL for RSS
# and other things that really need a base url...
# By default - if unset - it will error (if you can't set it, / is a good alternative :) )

# NOTE: IPNS simply does not work... so don't bother with it nya
set -euo pipefail
IFS=$'\n\t'

HUGO_EXECUTABLE="${HUGO_EXECUTABLE:-"hugo"}"

source build-scripts/little-utils.sh
check_for_hugo_root

log "Ensuring KaTeX is appropriately prepared."
source build-scripts/npm-processing.sh
npm_install_components
npm_ensure_katex
log "KaTeX step done!"


function postprocess_html {
    # -name before -type to avoid stat() call with -type nya.
    # Also see https://unix.stackexchange.com/questions/156008/is-it-possible-to-use-find-exec-sh-c-safely
    # for why we don't just inject the filename directly into the command nya.
    # the second sh is the command string ^.^
    local temp_parsed="$(mktemp --tmpdir 'tempfile.XXXXXXXXXXXXXXXXXXXX')"
    find ./public -name '*.html' -type f \
        -exec sh -c 'echo "Postprocessing " "$1" 1>&2; cat "$1" | ./build-scripts/maths-feedthrough.py 1>"$2"; cp "$2" "$1"' sh \{\} "$temp_parsed" \;
    rm "$temp_parsed"
}

# Generate associated hugo html, check for username leakage, 
# (and also postprocess that html, before the checks)
function generate_hugo_html {
    # Construct the site in /public meow
    log "Using hugo executable $HUGO_EXECUTABLE"

    log "Generating site in ./public with $HUGO_EXECUTABLE --minify --baseURL=\"$SAPIENTS_SITE_BASEURL\""
    $HUGO_EXECUTABLE --minify --baseURL="$SAPIENTS_SITE_BASEURL"

    log "Entering HTML postprocessing step! nya!"
    postprocess_html

    # Attempt to find any accidental name leakage and quit if found nya
    # Exclusion avoids other similar words :)
    if [[ $(grep -r "$USER[^a-zA-Z]" public | wc -l) -ne '0' ]]; then
        log "Found username in outputted files... INFOLEAK WARNING"
        log 'Printing `grep -r "$USER" public`'
        grep -r "$USER" public
        exit 2
    fi
}

