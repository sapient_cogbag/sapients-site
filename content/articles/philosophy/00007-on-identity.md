---
title: "Beyond a Binary Notion of Identity"
date: 2022-07-24T20:07:25+01:00
draft: true
tags: ["philosophy", "transhumanism", "future", "futurism"]
---

A common plot in many a science-fiction story is what I call "The Transporter Problem", in particular the question of if, say, the result of extracting the state of your body in sufficient detail and replicating it elsewhere is you, or is not you. This problem is in some ways related to the ideas involved in the Ship of Theseus problem, and is not just relevant to weird science fiction stuff but is also relevant to questions of identity related to when you sleep, or whether or not if you reassembled your neural patterns on a computer that would also be you or "just a copy", or even if a coherent "you" exists at all.

Existing discussion appears to be restrained, in some sense, by the question itself. That is, the insistence upon answering the question "is (one thing that might be you), you?" with a binary answer of true or false - something either is or isn't "you". However, I contest that this is necessarily a coherent question and that a binary answer can be coherently defined (other than the extreme cases in which there is a new "you" every single alteration of your internal state, or everything can be classified as "you", or "you" is itself refuted as a concept).

Currently, then, I would argue that discussion is limited to thinking of the identity function between two "states of self" as being in the form $I: S\times S\mapsto \\{0, 1\\}$; - that is, a binary result in which two states of self are either the same person or a different person. At least in my own personal - admittedly amateur - observation of this conversation. What I want to do is shift this discussion to one where the *identity function* is more complex and indicative of a degree of similarity, while accounting for changes over time and coherent notions of identity persistent over time (perceived and actual).

It should be noted that my own conception of what makes something a person does come into play here, but the concepts are general enough that if you can make your own concept of personhood and self work with the abstractions defined here, the theory and conceptualisation will work just as well. For me, intelligence and sapience are defined in relation to sufficiently recursive and complex neural networks, and are an emergent property of such in combination with appropriate rules for changes in those networks of information.

Before the full discussion, I define $\Reals^+$; as the set of all *zero or positive real numbers*, which is primarily used here for distances between specific instances you would consider "yourself" - I also define the collection $S$; to be the collection of instantaneous instances that encapsulate all information about a consideration of what makes someone themselves (for me this is partially down to self definition and I have specific ideas on what that constitutes for me that I think may be useful for others, which I will discuss later).

Over time, we will develop a sort of "branching like" conceptualisation of identity, using the concept of maximum deviation over some period of perceived time.

### $\Delta$; Function
The first component for my more radical notion of the identity function is built by encapsulating the overall distance between two instantaneous states as $\Delta : S \times S \mapsto \Reals^+$;. In a sense, this encompasses the difference in internal state between, say, your brain and your neighbours brain, or your brain and a hypothetical digital representation of your brain, or your brain and your brain 100 hours into the future, or something similar. Naturally, for a single $s \in S$;, $\Delta(s, s) = 0$; - a state is itself and so the delta function must return 0. 

It is possible to modify this model slightly in case some particular aspect of one of the states makes them irreconcilably different, by extending the set of possible outputs to include $\infty$;

#### Defining the $\Delta$;
Actually defining this function can be a challenge, and it is a question that depends on how you view identity. One potential route - which I take, as a result of my own conceptualisation of how conciousness or self works - is something like the following, which is oriented primarily around self-definition:
* First, define a *significance* function $v: S \mapsto N$; where $N$; is a set of pairs of (vector of all relevant internal states, vector of significances of how much each of those makes you yourself). If something is "infinitely" significant, then it is possible to include $\infty$; into the field over which the vectors of elements of $N$; are defined, and extend that to the $\Delta$; function output. 
  
  Naturally, this is a mathematical formalism representing the fact that what is encoded by various aspects of your internal neural network is of varying importance to what makes you, you, and in practise this would be understood to be representing that the parts of your self-state encoding, say, programming, or your sense of self, or your gender identity, your political goals and ideas, or similar such things, are of varying importance to what you consider "yourself".

  For instance if I forgot the English language, I would still consider me to be myself mostly (and whatever parts of my brain encode that are not super important to what makes me "me"), but I am quite attached to, say, my transhumanist political leanings and ideas. If I stopped being that way, or those values would change drastically, it would make me *much* less myself.

* Then,  define $$\Delta(s\_0, s\_1) = \sum_{k}{(\vert {v(s\_0)\_1}\_k \vert + \vert {v(s\_1)\_1}\_k \vert) \cdot \vert {v(s\_0)\_0}\_k - {v(s\_1)\_0}\_k \vert}$$; - that is to say that the difference is defined in respect to the degree by which the changes in neural state are significant to either $s_0 \in S$; or $s_1 \in S$;, and in proportion to the magnitude of each change. 
  
  In the case of some kind of hypothetical infinitely complex state with nonfinite vector components it becomes a little more complex unless the differences are a convergent infinite sum, but if they aren't then almost by definition the states are too different anyway and we can just tack $\infty$; into the produced set of possible values, if that is a risk.

This definition I came up with is a simple one. It is conceivable to construct much more complex ones (for instance relating to error boundaries or similar, where some region of deviation is treated as identity equality on, say, mind upload), but this simple variation I would actually consider sufficient.

### Substrate/Network Rules
So far, we have covered a more formalised definition of calculating the difference between two instantaneous states, and potential methods to roughly speaking do so. However, we have not covered how states change over time, how people experience time, and similar such things.

Generally speaking, the self-state will evolve according to a number of (potentially probabalistic) rules. Ultimately, if we go down far enough, these, in some sense, correspond to whatever the true laws of physics are, but in practise we abstract those away. For instance, if you have a digital brain, that would be evolving along whatever rules the program is coded to enable (which should probably match the behaviour of biological neurons). 

Self-state may also affect these rules partially - for instance, if your brain stimulates the production of hormones and they affect the way you think for some period of time, or perhaps more explicitly you adjust a neural simulation parameter while you're running as that simulation. Other examples of these rules may include adding some form of change upon 
