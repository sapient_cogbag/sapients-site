---
title: "Notes on Product Metric Spaces and N-Cuboid Analogues"
date: 2023-04-23T14:54:29+01:00
draft: true
tags: ["maths", "programming"]
---
Currently, I'm writing a plugin for neovim that needs to generate a random colour following a specific probability distribution - using rejection sampling. This distribution may be constructed from thousands of distinct functions, and calculating every function every single time would be inefficient. Hence, it is desirable to segment a colour space into cuboidal regions along each axis and store lists of only the relevant parts of the distribution function in each region.

However, distance within a colour space is not so simple. In particular, [metrics based on human perception][colour-diff] can pose a challenge to identifying when specified distribution patterns (e.g. "avoid colours within this region of space") are relevant to a specific region of the colour space (e.g. the region $(r, g, b) \in [16,32] \times [80, 96] \times [176, 192 ]$;)

This page is basically my notes on solving that by formalising the notion of a *cuboidal region* within a [product metric space][product-metric-space], then determining the types of pseudonorms-without-homogeneity that allow for intersection calculation. More specifically, this page explores what I term FSPNs (finitely-segmented pseudonorms) without homogeneity, corresponding to metric functions similar to that of the nonlinear RGB metric documented [here][colourdiff-superpage]:
$$
\def\rgb{\it{RGB}}
\begin{aligned}
&(r_1, g_1, b_1), (r_2, g_2, b_2) \in \rgb \\\\
&R = r_2 - r_1 \\\\
&G = g_2 - g_1 \\\\
&B = b_2 - b_1 \\\\
&\bar{R} = {(r_2 + r_1) \over 2} \\\\
&d = \begin{cases}
    \sqrt {2R^2 + 4G^2 + 3B^2} &\text{if } \bar{R} \lt 128 \\\\
    \sqrt {3R^2 + 4G^2 + 2B^2}  &\text{otherwise}
\end{cases}
\end{aligned}
$$;

In particular, note the following properties:
* Each branch of the metric is relatively simple and doesn't include any significant interaction between the metrics on each axis - i.e. it should be comparatively simple to check for collisions with some cuboid defined in terms of these axes.
* There are a small (finite) number of these branches, which correspond to subsets within $\def\rgb{\it{RGB}}\rgb \times \rgb$; - in particular, the subsets of colour pairs that resolve to evaluating with each branch.
* Between branches, a sort of partial inversion becomes more difficult. Furthermore, the outer function is not homogeneous on $\overrightarrow{(R, G, B)}$; or any other separation from per-dimension metrics to product metrics, like squaring each axis delta and passing into $N(x, y, z) = \sqrt {2x^2 + 4y^2 + 3z^2}$;   i.e. $d(c_1, c_2)$; as a product metric is composed not of a typical norm but something akin to a pseudonorm - this is because of the sharp divider.

This page attempts to perform some partial formalisation of this, and related structures. It may not be perfectly correct and formally cover every single case - it's notes rather than a full set of proofs, and more an attempt to determine the most relevant properties needed in an extension of product metrics to allow for segmentation. 

## Metric Product Space
To start with, we define a finite ordered collection of metric spaces $(M_1, d_1), ..., (M_n, d_n)$; for some $n \in \natnums$; and we begin by using a simple - non-segmented - norm function $N: \reals^n \to \reals$; composed to form a new metric space $(M_p, d_p)$; - known as the [Product Metric][product-metric-space].

The set component $M_p$; is the following:
$$
M_p = \prod_{i = 1}^{n}{M_i} \\\\
$$;

And, using the following function definitions of $E_k$; (a function extracting the $k^{th}$; component in the crossed sets), $d'_k$; (the metric $d_k$; applied to the $k^{th}$; components of the two elements of $M_p$; passed as parameters), and the function $d_×$; which constructs a vector $\bold{\vec v} \in \reals^n$; of per-axis metric function results from it's two inputs: 
$$
\begin{aligned}
&E_k (\bold{v}: M_p) \to M_k = v_k \newline
&{d'}_k (\bold{v_1}, \bold{v_2}): M_p \times M_p \to \reals = d_k(E_k(\bold{v_1}), E_k(\bold{v_2})) \newline
&{d_× (\bold{v_1}, \bold{v_2}): M_p \times M_p \to \reals^n} = (d'_1(\bold v_1, \bold v_2), ..., d'_n(\bold v_1, \bold v_2))
\end{aligned}
$$;

We can then define the traditional notion of a Product Metric space using the metric function: 
$$
\def\lv#1{\bold{v_{#1}}}
d_p (\lv{1}, \lv{2}) = N(d_×(\lv{1}, \lv{2}))
$$;

### Pushing The Limits of The Norm Function 
A product metric isn't quite what we need for our purposes. For a start, if we were to try and use for instance the RGB perceptual metric defined above, the outermost function is dependent on each actual $r$; component, as opposed to a simple function of $d_r, d_g, d_b$; (the metric functions of each of the axes' associated metric spaces) applied to the relevant components of a colour pair.

To understand the types of spaces we will deal with, we're going to experiment with removing constraints on $N$; - that is, removing some of the conditions that make $N$; a norm.

A norm $N:\mathbb{V} \to \reals$; on a vector space $\mathbb{V}$; (in our case, $\reals^n$;) over field $\mathbb{F}$; requires the following properties $\def\vb#1{\bold{\vec{#1}}}\forall \vb{x},\vb{y} \in \mathbb{V}, \forall s \in \mathbb{F}$;:
* $\def\vb#1{\bold{\vec{#1}}}N(\vb{x} + \vb{y}) \leq N(\vb{x}) + N(\vb{y})$; - Triangle Inequality:
    * This can be turned into $\def\vb#1{\bold {\vec{#1}}}N(\vb{y}) \le N(\vb{y} - \vb{x}) + N(\vb{x}) \implies N(\vb{y}) - N(\vb{x}) \leq N(\vb{y} - \vb{x})$;
* $\def\vb#1{\bold{\vec{#1}}}N(s\vb{x}) = |s|N(\vb{x})$;
    * We can weaken this property for our pseudonorms to state that the function $\def\vb#1{\bold{\vec{#1}}} f(s): \mathbb{F} \to \reals = N(s\vb{x})$; must be a non-decreasing (weakly-increasing) function on $|s|$; That is, for any two $s_1, s_2 \in \mathbb{F}, |s_1| \leq |s_2| \implies f(s_1) \leq f(s_2)$;. 
    * However, further experimentation on this property leaves it too weak to be useful. In particular, there is no analogue for equivalence, which is a necessary component for certain proofs.
    * This property implies that $N(-\bold {\vec x}) \in [\lim_{|s| = 0 \to 1}{N(s\bold {\vec x})}, \lim_{|s| = \infty \to 1}{N(s\bold {\vec x})}]$;. As long as there is no discontinuity at $N(\bold {\vec x})$; in the $\bold {\vec x}$; direction, this implies that $N(-\bold{\vec x}) = N(\bold {\vec x})$; because both upper and lower limits should be the same.
    * In a real norm, this is also true because the norm is always $\ge 0$; for a given $\bold{\vec x}$;, hence $|s|N(\bold {\vec x})$; is weakly increasing with $|s|$;
* $\def\vb#1{\bold{\vec{#1}}}N(\vb{x}) = 0 \iff \vb{x} = \vec{0}$; - This can be weakened so the arrow only goes backwards in a seminorm - i.e. the norm of zero is zero.
* An implied property: $\def\vb#1{\bold{\vec{#1}}}N(\vb{x}) \ge 0$; - Positivity. This is implied by $N(s\bold {\vec x})$; being a weakly increasing function in $|s|$; - in particular, $|0| = 0$; and $|s| \ge |0| \space \forall s \in \mathbb{F}$;, and therefore $N(0 \bold {\vec x}) \leq N(s \bold {\vec x}) \space \forall \bold {\vec x} \in \mathbb{V}$;, including when $s = 1, |s| = 1$;. $0 \bold {\vec x} = \bold {\vec 0}$;, and we know $N(\bold {\vec 0}) = 0$;, so $N(\bold {\vec x}) \ge 0$;

A metric space $(M, d)$; requires the following of $d, \forall x, y, z \in M$;: 
* $d(x, y) \geq 0$; - Positivity
* $d(x, y) = 0 \iff x = y$; - Making this so that distinct elements can have difference 0 (i.e. removing the forward arrow) turns this into a semimetric
* $d(x, y) = d(y, x)$; - Symmetry
* $d(x, z) \leq d(x, y) + d(y, z)$; - Triangle inequality

#### Radial Subequivalence Lemma - Modified Version of Finite Norm Equivalence
We are going to prove that any pseudonorms like those specified with the increasing outward condition (as a weakening of general homogeneity) on a finite-$n$; $\reals^n$; vector space within a closed ball $\mathbb{B}(\bold {\vec 0}, r)$; is subequivalent to any other within a modified variant of the radius. In particular, we demonstrate subequivalency to a given pseudonorm, as well as a similar type of superequivalency, then use the superequivalency to generate proofs for subequivalency of all other pseudonorms.  

The statement, for some fixed upper bound radius $r_2$;, is that two pseudonorms $N_1, N_2$; are subequivalent $\forall v \in \reals^n, \space N_2(v) \leq r_2$;. That is, $\exists C > 0$; such that the following is true for all $v$;:

$$
N_2(v) \leq CN_1(v) \implies {1 \over C}N_2(v) \leq N_1(v) \space (\text{superequivalency with a corresponding}\space r_1 = {r_2 \over C} )
$$;

To prove this theorem we first prove that all pseudonorms as-following-our-specifications are both subequivalent and superequivalent to the norm $N_1(\bold {\vec v}) = ||\bold {\vec v}||_{\infty} = \max(|v_1|, ...., |v_n|)$; - then use this as a bridge to prove total subequivalence between any two pseudonorms.

##### Radial Sub- and Super- Equivalence To $|| \cdot ||_{\infty}$;
To prove some of these properties, we use some techniques from [this maths stackexchange question](https://math.stackexchange.com/questions/57686/understanding-of-the-theorem-that-all-norms-are-equivalent-in-finite-dimensional).

In particular, we prove sub- and super- equivalency of pseudonorms  to $|| \cdot ||_{\infty}$; by induction on a 1-dimensional space for a given radius $r_2$;.

###### 1-D Subequivalency
Consider a 1-dimensional pseudonorm $N_1$; For us to prove subequivalency to $N_2(\bold {\vec v}) = \max{|v_1|}$;, we must find a constant $C$; such that $N_2(\bold {\vec v}) \leq C\max{|v_1|}$; within a radius $r_2$;
 
{{< aside "1-D pseudonorms are weakly increasing w.r.t the absolute of the value passed in." >}}
We know (from our pseudonorm constraints) that $f(s) = N_2(s\bold{\vec{z}})$; is a weakly monotonic function w.r.t $|s|$;  Simply picking $\bold{\vec z} = 1$;, then, proves that $N_2$; is weakly monotonic w.r.t $|s|$; because $f(s) = N_2(s)$;
{{< /aside >}}

First, take the image $I_2 = N_{2}^{-1}([0, r_2])$; Such an image is the union of any combination of the following two pairs of sets:
* For the negative side - $v_1 \leq 0$; - we have either of the following:
    * The entire negative real line $\\{v \in \reals | v \leq 0\\}$; - for when the negative side of $N_2$; is bounded entirely by $r_2$;
    * An interval from the last point where $N_2(v) \leq r_2$; for negative $v$;, to zero. It is an interval because $N_2$; is weakly monotonic with respect to the absolute value passed in. Said interval is closed in all cases unless there is a discontinuity approaching the exact point $N_2$; "would have" output $r_2$; 
* For the positive side - $v_1 \geq 0$; - we have either of the following:
    * The entire positive side of the real line (including zero).
    * An interval from zero to the "last" point where $N_2(v) \leq r_2$; on the positive side. This interval is closed unless there is a discontinuity exactly as the input approaches a point where $N_2$; "would have" produced $r_2$;

For each side of this, we then perform the following procedure to extract a constant - understanding that, for 1-dimension, $||v||_{\infty} = |v|$;


#### Constructing Actual Product Spaces

When constructing a standard product metric space, there are several relevant deductions:
* The symmetry of $d_p$; is implied by:
    * The symmetry of the individual $d'_k$;, which itself is implied by the symmetry of $d_k$;
    * Of note, this is dependent on the fact that the norm function is not dependent on the specific location in the input space $M_p \times M_p$;. To preserve the (important) symmetry property, any modification must preserve order-independence.
    * For a location-dependent modified norm generator $G: M_p \times M_p \to (\reals^n \to \reals)$;, it must be true that $\forall a, b \in M_p, \space G(a, b) = G(b, a)$;
* The triangle inequality of $d_p$;, which requires that $N(d_×(x, z)) \leq N(d_×(x, y)) + N(d_×(y, z))$;, is implied by the following:
    * The triangle inequality along each axis and their associated $d_k$; implies that, $\forall x, y, z \in M_p, \forall k \in [1, n]$;: 
        * $d'_k(x, z) \leq d'_k(x, y) + d'_k(y, z)$;
        * That is, every axis in $d_×(x, z)$; is $\leq$; the same axis in $d_×(x, y) + d_×(y, z)$;
    * The triangle inequality of the norm $N$; allows us to rewrite the required inequality in a stricter fashion - $N(d_×(x, y) + d_×(y, z)) - N(d_×(x, z)) \geq 0$;
    * Define the following constant: $\Delta = d_×(x, y) + d_×(y, z) - d_×(x, z)$;. Because of the inequalities, we know that every axis of $\Delta$; is $\ge 0$;
    * Define the following: $\sigma(t): [0, 1] \to \reals = N(d_×(x, z) + t\Delta)$;. This function draws a curve from the original distance vector to the summed triangle-inequality distance vector, taking the norm.  
    * 


## Product Space Cuboids
Using our (for now, half-baked) expanded metric product space, we want to define the notion of a cuboid within it and then use some basic means to generalise collisions with spheres or other shapes within this expanded notion.

The space of points in our metric product space is a product set - $M_p = \prod_{i = 1}^{n}{M_i}$; For our most primitive notion of cuboid, we use a simple *product subset* - that, is picking subsets $U_1 \sube M_1, ..., U_n \sube M_n$;, and taking our product subset cuboid to be $U_p =  \prod_{i = 1}^{n}{U_i}$;. We will call this notion a *primitive product cuboid*, and as long as there are efficient indicator functions $\chi_{U_1}: M_1 \to \\{1, 0\\}, ..., \chi_{U_n}: M_n \to \\{1, 0\\}$; then it is easy to determine if a given single point $\bold{x} \in M_p$; is a member of $U_p$; by simply checking each axis.

We then add some finiteness criterion:
* For any $\bold{x} \in M_p$;, there exists a finite maximum distance $\sup_{\substack{q \in U_p}} d_p(\bold{x}, q)$; - by triangle inequality, this also implies a finite maximum distance between any two points within $U_p$;
* The product metric $d_p$; is composed of *some* sort of function $N: \reals^n \to \reals$; that looks norm-ish, and the associated distance functions $d_1, ..., d_n$;

### Axis-Modular Minimisation Theorem
For simplicity, we want to prove that minimising the distance along each axis to a point will also result in the overall closest point within a cuboid set to a point $\bold r \in M_p$;. This is actually a simple constraint problem (convex), but we want to be a little more flexible so we write using the language of metrics here.

Suppose first we have a point $p_A \in U_p$;, such that $\forall k \in \\{1, ..., n\\},\space \nexists \zeta_k \in U_k \space \text{s.t.} \space d_k(\zeta_k, E_k(\bold r)) \lt d'_k(p_A, \bold r)$;. That is, along each axis, the point is the closest possible to the given $\bold r$; along each axis.

We want to then show that there does not exist a point $p_U \in U_p$; where at least one axis is at a greater distance from $\bold r$; *along that axis*, but $d_p(p_U, \bold r) \lt d_p(p_A, \bold r)$;

To start with, assume such a point $p_U$; exists. This implies that $\exists k \in \\{1, ..., n\\}$;, where $d_k(p_{U_k}, r_k) \gt d_k(p_{A_k}, r_k)$;. 



[colour-diff]: https://en.wikipedia.org/wiki/Color_difference
[product-metric-space]: https://en.wikipedia.org/wiki/Product_metric
[colourdiff-superpage]: https://www.compuphase.com/cmetric.htm


