+++
title = "nsfw"
default-hide-tag.nsfw = false
+++

All pages tagged NSFW.

This tag is used to indicate a number of things. It's primary association is with sexual or violent content, but it is also used for other things which you may want to avoid looking at in an environment where others can see your screen, like a page identifying fascist symbols or (with an emphasis on the "work" part of nsfw) a page on unionisation.

For the tag more specifically geared towards adult leaning topics (not necessarily sexual, but usually adjacent in some way), see the [over-18 tag](/tags/over-18)

