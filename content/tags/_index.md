+++
title = "All Tags"

default-hide-tag = {}
+++

### Summary
All content on my site has a series of tags, as listed here. You can see them under 
the title of whatever page you happen to be on (though some, like this page, have no tags).


### Filtering and Tagged Lists
When you look at a list of pages anywhere on the site, you'll notice that the pages have 
little tags after the links to them. 

They also probably have a list of all the tags on the pages 
collected in the list - at least those tags that are not implied by being on the list page 
in the first place (for instance, if you are on the {{<tref "/tags/personal" />}} page then
the "personal" tag will not be listed) - at the top of the page list.

On list pages, the list of all tags at the top of the page list can be used to filter what pages
are shown in the page list. Some tags are also hidden from the list by default as well. Simply 
clicking on a tag at the top will toggle the visibility of pages with that tag.
