---
title: "Adaptable Cooking"
date: 2022-01-07T06:37:29Z
draft: false
flatten: true
tags: ["food", "cooking"]
shorttitle: "ad/cook"
cascade:
  tags: ["food", "cooking"]
---
So. You're in an accommodation bind, staying in temporary accommodation and lacking access to a kitchen. Or, perhaps, you're in shared accommodation and attempting to self-isolate. Or maybe your oven is just broken, or perhaps you live in a flat small enough that there is not room enough for an oven, microwave, *and* stove - and as such you only have access to a microwave. Hell, maybe you're staying in a hotel on a holiday and don't want to spend money in the cafeteria.

Most recipes (and sites that provide them) presume access to a modern, fully-equipped kitchen, with oven, fridge and freezer, stove, microwave, and all that stuff. Which is of course the ideal situation to have, but there are a lot of us, who, for one reason or another, do not live in an *ideal situation* in regards to access to basics. 

I myself have for some (luckily no longer than a few weeks, usually) periods in my life lacked access to most of the standard kitchen fare for one reason or another, and had to learn from experience how to cook in this sort of environment, what equipment was most important, and how to get the most use out of things like milk (soy milk, in my case) in that situation with the limited storage capabilities present.

This section of my site is an attempt to convey that knowledge to those who need it for one reason or another, so you aren't forced to live off shitty, expensive, unfilling corner-store/petrol-station junk. 

My main experience is living in an urban area with access to proper grocery stores within an hour or two walk, rather than in an area that could be classified as a [food desert]({{% relref food-desert %}}), though a lot of information here should remain applicable.
