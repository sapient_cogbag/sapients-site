#!/usr/bin/env python3

# This takes a list of files as input nya, as output from
# `git diff-index --ignore-space-change --name-only --diff-filter=AM HEAD 'content/*'`
# That is, all changes (adding or modifying) where we might want to *update the
# dates in the files*. Since we don't want to have to manually manage all of the
# dates whenever we make changes.

# This is intended to update the dates in the start of hugo files, when it
# doesn't contain a `finalised` directive.

import sys
import subprocess
import datetime
import os.path as path
join = path.join


def log(*args, **kwargs):
    """
    Like print but always outputs to stderr
    """
    kwargs["file"] = sys.stderr
    print(*args, **kwargs)


def delimiter_in(line: str):
    """
    Return the delimiter in the given string, if it is = or :.
    If none is found return :
    """
    try:
        return line.index("=")
    except ValueError:
        return ":"


def delimited_header_lines(
        lines: list[str],
        value_delimiter: str,
        for_key_selector
    ) -> list[tuple[int, tuple[str, str], str]]:
    """
    Filter and split a series of frontmatter lines into their delimited keys and values,
    filtering when the key (stripped of whitespace) gets a True from the function.

    Returns a list of (index relative to the first line, (full pre-delimiter text, full post-delimiter text), full line )

    Note that this is a dumb method of detecting delimited values. While it's 
    good for find/replace, it won't handle e.g. `-` prefixed values in yaml that are subvalues
    very well.
    """
    maybe_delimited_text_iter = (
        (idx, raw_line.split(sep=value_delimiter, maxsplit=1), raw_line)
        for (idx, raw_line) in enumerate(lines)
    )

    delimited_text_iter = (
        (idx, sv, v)
        for (idx, sv, v) in maybe_delimited_text_iter
        if len(sv) == 2
    )

    key_matches_iter = (
        (idx, (predel, postdel), full_line)
        for (idx, (predel, postdel), full_line) in delimited_text_iter
        if for_key_selector(predel)
    )

    return [v for v in key_matches_iter]


def maybe_append_newline(line: str):
    """
    Append a newline to the given line if it does not already have it nya
    Empty strings also get newlines.
    """
    if len(line) == 0:
        return "\n"
    else:
        if line[-1] == "\n":
            return line
        else:
            return line + "\n"


git_working_dir = join(subprocess.run(
        ["git", "rev-parse", "--git-dir"],
        capture_output=True,
        encoding="utf-8",
        text=True
).stdout.strip(), "..")

# see https://stackoverflow.com/questions/2150739/iso-time-iso-8601-in-python
# NOTE - I cannot get python to use the correct timezone, but the `date` command
# works just fine! nya
raw_datetime_string = subprocess.run(
    ["date", "--iso-8601=seconds"],
    capture_output=True,
    encoding="utf-8",
    text=True
).stdout.strip()

log("Using raw datetime string '{}'".format(raw_datetime_string))

while True:
    line = sys.stdin.readline()
    if line is None or len(line.strip()) == 0:
        break
    fname = join(git_working_dir, line.strip())

    site_file_lines = None
    log("Reading lines from ", fname)
    with open(fname, "rt") as reading_file:
        # We do not want to remove anything other than excess newlines ^.^ 
        # if not, it makes whitespace changes always cause changes in the file
        # so it gets modified again, added to a commit, changed again, etc., 
        # recursively forever unless you stash and erase.
        try:
            site_file_lines = [fline.rstrip("\n") for fline in reading_file.readlines()]
        except UnicodeDecodeError:
            log("File was not unicode - probably binary or something. Skipping.")
            break

    if len(site_file_lines) == 0:
        continue

    log("Attempting to detect frontmatter delimiter (+++ or ---)")

    # Detect the +++ or --- delimited bit at the top of the file.
    info_start_header_idx = None
    header_delimiter = None
    # ":" or "="
    value_delimiter = None
    delim_type = None
    # Format string to put the new date value after the delimiter...
    post_delim_format_string = None
    try:
        info_start_header_idx = site_file_lines.index("---")
        header_delimiter = "---"
        delim_type = "YAML"
        # Raw dates work in hugo stuff nya
        post_delim_format_string = "{}"
        value_delimiter = ":"
    except ValueError:
        try:
            info_start_header_idx = site_file_lines.index("+++")
            header_delimiter = "+++"
            delim_type = "TOML"
            # TOML needs quotes.
            post_delim_format_string = "\"{}\""
            value_delimiter = "="
        except ValueError:
            continue

    log("Found frontmatter delimiter '{}' - identified as {}".format(header_delimiter, delim_type))

    # Now we have a start idx and delimiter, look for the end index.
    info_end_header_idx = None
    try:
        info_end_header_idx = site_file_lines.index(
                header_delimiter,
                info_start_header_idx + 1
        )
    except ValueError:
        continue

    # Lines relevant to search for info are
    # [info_start_header_idx + 1:info_end_header_idx]
    inner_header_lines = site_file_lines[info_start_header_idx+1:info_end_header_idx]

    is_finalised = False
    # Test for finalisation
    for (_, (_, unstripped_v), _) in delimited_header_lines(
            inner_header_lines,
            value_delimiter,
            lambda k: k.strip().lower() in {"finalised", "finalized"}
    ):
        is_finalised = is_finalised or (unstripped_v.strip() in {"1", "True", "true", '"true"', '"True"', "'true'", "'True'", "yes", "Yes", "Y", "y", "'Y'", '"Y"'})

    if is_finalised:
        log("File ", fname, " was found to be finalised, aborting any modification for this file")
        continue

    # Not finalised - look for date kv and replace the date.

    # This only contains lines split on delimiters where the stripped pre-delimiter == date
    date_lines = delimited_header_lines(
        inner_header_lines,
        value_delimiter,
        lambda key: key.strip() in {"date"}
    )

    if len(date_lines) == 0:
        log("No date info found in frontmatter for {}, skipping further processing".format(fname))
        # No dates in here, skip everything
        continue

    for (frontmatter_inner_idx, (pre_with_key, post_delim), _) in date_lines:
        # Update with current date:
        # see https://stackoverflow.com/questions/2150739/iso-time-iso-8601-in-python
        new_datetime_end_string = post_delim_format_string.format(raw_datetime_string)
        # Simply change the value to the new one :)
        new_line = pre_with_key + value_delimiter + " " + new_datetime_end_string
        # Calculate the index in the main set of lines
        full_idx = info_start_header_idx + 1 + frontmatter_inner_idx
        log("Updating datetime on line #", full_idx)
        site_file_lines[full_idx] = new_line

    site_file_lines = [maybe_append_newline(line) for line in site_file_lines]

    log("Writing back to file ", fname)

    # Try write back out the lines
    # Note that we check if lines end with "\n" and if not append that :) nya
    with open(fname, "wt") as fout:
        fout.writelines(site_file_lines)

    log("Modifications done for {}".format(fname))

