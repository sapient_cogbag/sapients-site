---
title: "Programming, Transhumanism, Cooking and More - sapient's site"
draft: false
meta-description: "Articles and blogposts on transhumanist philosophy, rust programming, climate and politics, and cooking in both kitchen and kitchenless environments"
---

Find articles and blogposts on transhumanist philosophy, climate, politics, cooking tips, and programming languages on this lovely site! Some pages you might be interested in immediately are as follows:
* The {{< tref "tags/trans" >}}transgender{{< /tref >}} tag contains posts relating to my experience of being trans as well as philosophy and politics that are relevant to that.
* The {{< tref "tags/neurodiversity" />}} tag contains posts relating to my experiences as a neurodivergent person (as well as philosophy and politics around that). 
    Some related tags are {{< tref "tags/autism" />}} and {{< tref "tags/adhd" >}}adhd.{{< /tref >}}
* The {{< tref "tags/transhumanism" />}} tag has posts on transhumanist philosophy, ideas about transhumanism, and my thoughts as a transhumanist. Similarly, the {{< tref "tags/philosophy" />}} tag and {{< tref "tags/politics" />}} tag have many of my thoughts and ideas and commentary on philosophy and politics - oftentimes these overlap with the transhumanist parts of this site.
* The {{< tref "tags/climate" />}} tag contains my thoughts, ideas, politics, and potential future-paths on how to deal with and reduce the effects of climate change. This often overlaps with politics, {{< tref "tags/futurism" />}} and others.
* The {{< tref "tags/programming" />}} tag has various posts and articles about programming - practical programming, runthroughs of how I did something, or more abstract and philosophical ideas about programming and programming languages.
* The {{< tref "tags/cooking" />}}, {{< tref "tags/food" />}}, and {{< tref "tags/recipes" />}} contain things relating to cooking, though this part of the site is somewhat underdeveloped. 
* The {{< tref "tags/site-meta" />}} tag contains posts about this site as well as an exploration of technologies that I used to build it.

{{< aside info block "RSS Feeds and Personalisation" >}}
It is possible to subscribe to RSS feeds individually for:
* A given tag
* A given section of the site
* A given series on the site

The RSS links are provided on the series/section page as well as on links to that page in wider listing pages (e.g a section containing the series/sections).

If you're only interested in some topics, subscribing to only the topics you care about is a good strategy.
{{</ aside >}}

The main sections of the site are in the navigation bar above, and there is also a filterable list (with toggleable filter buttons) on each listing page. 

Major sections of the site can be explored using the bar at the top of all the pages, and each listing page also has all the series that are below that page (for all series, see the list of series on this page just below).




