#!/usr/bin/env bash
# https://stackoverflow.com/questions/17850991/how-to-make-website-searchable-for-different-search-engines

echo "This script will ping search engines with sitemap.xml"
url_netlify="https://sapients-site.netlify.app/sitemap.xml"
url_neocities="https://sapient-cogbag.neocities.org/sitemap.xml"

function google {
    curl "http://www.google.com/webmasters/sitemaps/ping?sitemap=$1"
}

function bing_yahoo {
    curl "http://www.bing.com/webmaster/ping.aspx?siteMap=$1"
}
echo
echo "pinging google"
google "$url_netlify"
google "$url_neocities"

echo "pinging bing/yahoo"
bing_yahoo "$url_netlify"
bing_yahoo "$url_neocities"
