+++
title = "Climate Adaptation"
date = "2021-10-16T21:03:17+01:00"
draft = false
series = ["climate-adaptation"]
flatten = true
finalised = true
tags = ["series", "climate", "politics", "future", "technology"]
shorttitle = "climate-adaptation"
[cascade]
tags = ["climate", "politics", "future", "technology"]
+++
This series explores methods of adapting to the environmental chaos of climate change along with their wide implications. Read the {{< tref "introduction" />}} for more. 
