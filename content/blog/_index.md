+++
title = "Blogposts"
draft = false
flatten = true
shorttitle = "blog"
+++
This is where I blog about random stuff, including politics and programming. It's honestly fairly similar to the [articles](/articles) section.
