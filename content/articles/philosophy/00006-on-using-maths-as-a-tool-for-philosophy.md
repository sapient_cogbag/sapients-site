---
title: "Maths as a Philosophical Tool"
date: 2022-07-24T19:35:50+01:00
draft: true
tags: ["philosophy", "maths"]
---
Much of philosophy is 


On top of this, maths contains more tools for analysis than logic alone, which means it's use is a strict benefit while preserving a fair degree of rigorousness - or rather, it defers fallibility to the point where you define terms and concepts and fit them into mathematical structures while providing a richer language for managing uncertainty and more complex definitions, than pure logic alone - along with possessing some of the best tools to make generalisable conclusions and encapsulating abstractions. 

Even if mathematical statements themselves can be said to some degree to be true or false within some set of axioms and symbols with meaning and manipulation rules, it makes it easier to express variation and degrees in inner terms of the logical statements, or allows us to define alternate metrics than a binary true or false, easier.

On a personal level, I enjoy maths and philosophy, so I choose to attempt to combine them. 

### 





Note that this article uses terminology from [category theory](https://en.wikipedia.org/wiki/Category_theory) to attempt to examine arbitrary structures. Basic ideas are as follows:
* Category: a class (set that doesn't contain itself) of arbitrary structures, and a collection of morphisms (things which take objects in the category and make other objects in the category with some various other properties).
* Functor: Takes objects from one category and converts them to another category, while *preserving* the structure of morphisms that can apply to the objects within each category by providing an associated morphism in the target category for every morphism in the originating category.
  Essentially these are morphisms on the category *of smaller categories*.
* CoFunctor (covariant functor): Like a functor except it has reverse directions for the output morphisms and the inmu
