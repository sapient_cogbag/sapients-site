---
title: "The Journey to P2P Git"
date: 2022-01-20T21:11:47Z
draft: false
flatten: true
finalised: true
shorttitle: "p2p-git"
series: ["journey-to-p2p-git"]
tags: ["series", "programming", "p2p", "git", "rust", "decentralisation"]
cascade:
    tags: ["programming", "p2p", "git", "rust", "decentralisation"]
---
In which I talk about my journey towards creating a flexible and fully-featured p2p git overlay, [p2p-sourcenet/p2psn](https://gitlab.com/sapient_cogbag/p2p-sourcenet/), as well as the things I learn about git conceptually and its internals as well.
