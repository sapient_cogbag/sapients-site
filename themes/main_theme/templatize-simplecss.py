#!/usr/bin/env python3

import re

# Prepend or set variable components nya.
css_vars_unthemed = {
    'sans-font': 'pre',
    'mono-font': 'pre',
    'base-fontsize': 'set',
    'header-scale': 'set',
    'line-height': 'set'
} 

# Set themed (light vs dark) components. These are all "set"
css_vars_themed = {
    'bg', 'accent-bg', 'text', 'text-light', 'border', 
    'accent', 'accent-light', 'code', 'preformatted', 
    'marked', 'disabled'
}

def make_variable_line_regex():
    """
    Create a regex for matching any of the variables with appropriate prefixes meow.

    Returns a regex with two match regions - the first is the variable name (no -- prefix),
    the second is the value it takes in the original simple.css nya
    """
    themed_var_bit = r"\s*--(" + '|'.join(css_vars_themed | {a for a in css_vars_unthemed.keys()}) + r")\s*:\s*([\"#a-zA-Z0-9.,\-\s]+)\s*;"
    return re.compile(themed_var_bit)

BRACKET_DEPTH = 0
IS_IN_DARKTHEME = False
def line_enables_darkmode(line): 
    return re.match(r"\s*@media\s+\(prefers-color-scheme\s*:\s*dark\s*\)\s*{", l) is not None


def generate_hugo_conditional(outer_param_map, inner_param_name, pfmt, default):
    conditional = '{{ if ($.Param "' + outer_param_map + '") }} '
    conditional += '{{ if (isset ($.Param "' + outer_param_map + '") "' + inner_param_name + '") }}'
    conditional += pfmt.format(' {{- index ($.Param "' + outer_param_map + '") "' + inner_param_name + '" -}}')
    conditional += "{{ else }} " + default + " {{ end }} {{ else }} " + default + " {{ end }}"
    return conditional

with open("simple.css/simple.css", 'rt') as source:
    with open("assets/css/core.css", 'wt') as templatized_output: 
        vardef_pat = make_variable_line_regex()
        for l in source.readlines():
            if l.strip().endswith("{"):
                BRACKET_DEPTH += 1
            elif l.strip().endswith("}"):
                BRACKET_DEPTH -= 1
            if line_enables_darkmode(l):
                IS_IN_DARKTHEME = True
            if BRACKET_DEPTH < 1 and IS_IN_DARKTHEME:
                IS_IN_DARKTHEME = False

            var_defines = vardef_pat.match(l)
            if var_defines is None:
                templatized_output.writelines([l])
            else:
                var_name = var_defines.group(1)
                var_default_value = var_defines.group(2)
                print("\nFound variable {}".format(var_name))
                print("Default value: {}".format(var_default_value))
                nl = None
                if var_name in css_vars_unthemed:
                    print("Identified as light/dark theme independent.")
                    if css_vars_unthemed[var_name] == 'pre':
                        print("Variable value is prefixed by hugo param.")
                        nl = "  --" + var_name + ": " + generate_hugo_conditional("css", var_name, "{}, ", " ") + var_default_value + ";\n"
                    elif css_vars_unthemed[var_name] == 'set':
                        print("Variable value is set to hugo param only, if present.")
                        nl = "  --" + var_name + ": " + generate_hugo_conditional("css", var_name, "{}", var_default_value) + ";\n"
                else:
                    if IS_IN_DARKTHEME:
                        print("Variable is within a dark theme section - using dark theme param map.")
                        nl = "    --" + var_name + ": " + generate_hugo_conditional("css-dark", var_name, "{}", var_default_value) + ";\n"
                    else:
                        print("Variable is within a light theme section - using light theme param map.")
                        nl = "  --" + var_name + ": " + generate_hugo_conditional("css-light", var_name, "{}", var_default_value) + ";\n" 

                templatized_output.writelines([nl])
