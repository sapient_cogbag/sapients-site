# sapients-site

[THE SITE](k51qzi5uqu5dk60qn7nw7mbao7dkfvht4ympe21rxtmptfe5reuj4b772g05at.ipns.dweb.link)

My website (hugo). Meow.

Note on licensing:
 * The textual content of my pages - that is the stuff you see when you load it - is under CCO license.
 * The theming - specifically the pure-css tag and filter system - is under GPLv3+ as visible in the associated subdirectory

FOR DEVELOPERS (Future Me):
* Hugo lacks support for proper functions - which also makes it really hard to 
 compare tags for equality.
* However we can approximate it with partials and scratches
* The calling convention is as follows:
 * Any function-partial will take a scratchpad as it's first (only) argument.
 * Functions will empty the scratch of all contents as it's used - unless the special argument "\_preserve" is set to a true value.  
 * The result of the function if it exists will be the only thing left in the scratch, under name "res", 
    except for functions prefixed with "mut_" (for mutability)
 * The above thing also means we can make a cheap knock-off of hugo pipes, with functions consuming "res".
 * Passing other functions is done by passing a partial name as a string (full path).
 * It is better (and much safer) to use a local `newScratch` as compared to using the page's .Scratch.
    * This is because clearing out the args requires obtaining the underlying Scratch map, which Hugo docs
        says should not be done with page Scratches due to concurrency issues. Even though we only read from
        that map, there is still the problem of race conditions and similar if you use it.

Currently we pin to an older hugo version (v0.96) by building hugo ourselves from git and passing the relative path as HUGO_EXECUTABLE environment variable to the build scripts
