#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
#IFS works much better with bash arrays nya

set -euo pipefail
IFS=$'\n\t'

# This script simply provides useful wrappers (e.g. for outputting to stderr nya)

log() { echo "$@" > /dev/stderr; }
eprintf() { printf "$@" > /dev/stderr; }
do_nothing() { printf ''; } 

# Check that we are in the hugo root directory and exit if not
function check_for_hugo_root {
    if [[ ! -f 'config.toml' ]]; then
        wd="$(pwd)"
        log "Directory $wd is not a Hugo root directory."
        exit 1
    fi
}
