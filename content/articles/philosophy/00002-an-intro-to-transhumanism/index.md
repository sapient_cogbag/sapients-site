---
title: "Introduction to Transhumanism"
date: 2022-05-07T01:04:54+01:00
draft: false
tags: ["philosophy", "transhumanism",  "politics"]
---
It is fairly common for people to ask the question, "what is transhumanism?", especially to me as a transhumanist. And it can be difficult to answer for a lot of people, and more difficult when there are conflicting definitions and an awful number of people who think transhumanism is as simple as sucking up to techbro billionaires who have suborned the idea of more general futurism (as you can tell, I have contempt for this attitude and consider it a horrific stifling of appreciation for the liberatory potential of transhumanist movements).

The most important thing to understand is that transhumanism is not merely a technological movement but one with philosophical implications and ideas - at least for me and most other transhumanists I know, which is obviously a biased sample. And by philosophical implications, I don't mean weird conceits like Roko's Basilisk or the LessWrong crowd, but much more radical conceptualisations of identity and agency.

#### My Definition
I can say then, that when I call myself a transhumanist, I am saying the following:
> I reject the notion that our biology has some intrinsic, essential value, other than that it can can generate our sapience and thoughts, and instead embrace the radical notion that individuals have the right to determine their own form and identity and biology (if any) free of coercion of any kind, be that by other human beings, by biology and "nature" as it currently exists, or by any other construct such as religion and prescriptive social roles. 
 Furthermore I believe that, as societies, it is of extreme priority and importance to develop and understand the technology, social structures, and distribution capabilities necessary to bring forth this capability of self-determination as rapidly as possible, to every person, while respecting the very notions we are trying to implement in the process of doing so.

##### Morphological Autonomy
Morphological Autonomy (or Morphological Freedom) is a transhumanist extension to the notion of bodily autonomy.

Bodily autonomy is roughly the idea that no-one else should be able to dictate what happens in your body. Usually it is brought up when discussing things like abortion or consent for sexual behaviour - this is the notion the fascist US Supreme Court is planning to overturn at the time of writing of this article.

Morphological autonomy is a radical extension of this that essentially codifies the notion that people have the right to determine how their body should be and their very identity, and furthermore that every person has the right to the means to align what their body is like with what they want it to be - this latter part, the access to means, is why I use the term "autonomy" rather than merely "freedom", as autonomy implies meaningful ability to enact a choice, rather than just in theory. 

It is a rejection of the notion that the default prescribed by our biology is somehow preferable or otherwise superior to what people actually want for themselves, or that the biological default should determine someone's identity - instead maximising people's ability and choices to define themselves. In particular and for example, involuntary death, aging, disease, starvation, information deprivation (for you cannot make informed choices without information), lack of shelter, or similar constitute violations of morphological autonomy even though they are "natural", whereas many people would not consider those violations of bodily autonomy, or even if they did, would refuse to accept efforts to fight them because they are considered inevitable (in particular death and aging are victims of the latter argument).

#### The Implications - For me and others who would describe their beliefs with this definition or similar
My definition and understanding of transhumanism have important implications towards things like technological routes and general anticapitalism.

For me, in particular, the notion that people should be free of coercion - and free of deprivation of the means of self-determination and self-modification - is critically important as it is ultimately the thing that drives my anarchist views and the implication is that any transhumanist - by my definition - must be at least a social democrat and most probably a libertarian socialist of some kind, if not an outright anarchist as I am, as anything less goes against the entire and fundamental notion of morphological autonomy that underpins the definition.

This definition also codifies my undying hatred of all involuntary death other than in self-defence, and my view of it as an abomination against sapient life. Pretty aggressive but the number of people who turn out to think involuntary death is somehow a good thing or at the very least "natural" and acceptable and not worth fighting is... well, shocking. At least in my experience. For me, then, anti-aging technology is an essential and core part of transhumanist goals as a direct component of morphological autonomy.

It also quite encompassing, and I think this is a good thing. Transhumanist praxis, then, for me, is any technology or political activism that allows (or fights to allow) people to have more ability to survive, modify themselves, and identify as themselves - for example, the first human being to develop shelter or use a tool and change their own environment to promote their own ability to survive and live longer when they would otherwise be killed by nature was arguably engaging in the first transhumanist praxis in human history, and the first group of people that built accommodations to allow disabled people to survive autonomously (without having to beg for help) was engaging in another.

The last bit of wording in the definition and description of my views of transhumanism, that is a very important part, as it is intended to prevent the kind of horrors that occured in colonialism in the name of "natural law". The real core of this idea of transhumanism is the notion of self-determination, and trying to increase future self-determination by subjugating people is not something I'd consider transhumanist in any meaningful sense (it allows for continuous and eternal justification of oppression by particularly nasty regimes as well, for example if you look at things like "socialism with Chinese characteristics" where the socialism is always far in the future).

#### Modern Transhumanist Praxis
Transhumanism - as a consequence of the flawed notion that it is merely about new technology for human modification, rather than a broader movement including philosophy - is often described as something for navel-gazers and meaningless to everyday people. This is very far from the truth, and to prove it, I will include examples of modern transhumanism:

* The fight for abortion rights - any self-consistent transhumanist will fight for abortion rights, as it is part of the core notion of bodily autonomy and hence morphological autonomy.

* Transgender Rights & Transgender Movement - Arguably, transitioning of any kind and fighting for gatekeeping-free access to transition, and the right for all trans people to determine their own identities, is one of the most radically transhumanist[^trans2] things possible to do today. There's even a [(half-meme, half-serious) subreddit][transtrans] for this, but the movements are connected on a more fundamental level than a lot of people realise, enough that weird bioessentialists realise the fact and cite it as if it's a negative. {{< tweet user="Newsweek" id="1111216374297382912" >}}

* Development of general self-modification techniques and improving accessibility - even now, there are already quite a few biomodifications people can obtain. Improving access to them is an important aspect of transhumanism. Some examples:
  * Implantable magnets enable people to get a sense of magnetic fields
  * Special cochlear implants can actually let people get a sense of signal intensity for things like wifi
  * There exist some (still fairly unsophisticated) biomods for adding or removing features on your face or body
  * People can develop extra qualia from sensory input on their back
  * It is possible to get eye injections that enable you to see infrared light - though it turns your eyes black, if I remember right
  * Implants that let someone store cryptographic private keys like an external token, or implants that can act as a USB stick

* The fight for legalisation of drugs and a medical model for addiction - In particular, the idea that people should be able to consume psychoactive substances at-will (and see a doctor if it significantly harms them), rather than the idea that anything other than socially preapproved drugs like alcohol, caffeine, tobacco, etc. should be illegal or socially shamed.

* Miscellaneous fights to reduce discrimination against those with more minor body modifications (tattoos, hair dye, piercings, etc.)

* Sexual liberation movements, LGBT+ movements, and polyamory movements are all about enabling multiple models of sexuality and relationships as opposed to social dictation of a "traditional nuclear family" model of sex, gender, and relationships, all things that enable people to define themselves and their identity, and control what they do with their own bodies, rather than have it be dictated by either social roles, biology, or enforcement of notions of the previous two by state violence/law.

* The fight for universal healthcare in the USA is a fight for people's right to live - and any research and improvement to healthcare anywhere comes under this general reasoning for transhumanist nature, though the dysfunctional system in the USA makes it a more brazen example.

* Pushes and progress in anti-aging technology are major modern transhumanist goals (and there has been tangible progress on countering the mechanisms of aging, especially in the past 3 or 4 years, research-wise - it's still an early field though).

* Research into 3D biomaterial printing is a transhumanist goal as that technology once fully developed would allow radically-customisable body modification.

* The development of Virtual Reality of increasing sophistication, capability, and accessibility, is a useful milestone towards morphological autonomy online - that is, full choice of self-presentation - for those with access.

* Sophisticated genetic modification techniques and the distribution of technology to construct transmission vectors en-masse.

* Development of direct Brain-Computer Interfaces - especially in the context of humans being highly neuroplastic, enough to develop new senses when given appropriate direct neuron input.

Many more technologically oriented developments and projects (for instance vertical farming) are transhumanism-adjacent as they increase hyperabundance of various resources and hence enable better global survival capabilities in the face of climate change - as well as general automation and a lot of other things with various contexts. This is most definitely not a complete list.

#### Some Terminology

##### Bioessentalist or Bioreactionary
A bioessentialist is someone who views the human form (or other forms of other things, like "natural" plants) as something good or to emulate, or worse, as some determinant of identity. For instance, opposition to genetically modified or "processed" food often comes from the angle that it is not "natural" and this makes it bad (despite it's environmental benefits via increased efficiency and some other stuff) - this is an example of a less harmful bioessentialist idea. 

More examples include claims that being trans is not natural or that trans <men/women> are "biologically" <women/men>, or that the natural role of (cis) women is in childbirth, or similar. Essentially, arguments based around an appeal to some essential "biological" or "natural" state. 

Bioreactionary is not quite the same as bioessentialist - it usually implies more aggressive support of typically reactionary policies, whereas occasionally you'll find people making bioessentialist arguments against reactionaries (though it is not super common), expanding the definition of what is "natural" rather than challenging the notion that what is "natural" is something we should even accept as a determinant of acceptability or identity - e.g. "being gay is natural so we should accept gay people", which still fundamentally limits us to notions of identity that have been deemed sufficiently "natural" in the face of an ultracomplex humanity with near-infinite fractal notions of identity, and constantly improving computational technology.

Bioreactionaries in particular - most fascists come under this but especially ecofascists - are direct political enemies of any sensible transhumanist as their fundamental idea of how the world should work is in direct opposition to transhumanist concepts and at it's core is radically repressive. 

Bioreactionary elements exist in most parties right-of-centre but they are especially prominent and less hidden in theofascist and otherwise theocratic groups (who usually treat "godly/god-ordained/god's will" and "natural" as synonyms as well) - even nominally liberal religious groups often still promote self-repression and prescriptive roles based on "nature"/"god's will", or shame people for deviation, but say "we're all sinners, it's ok".

##### Longevity Escape Velocity (L.E.V)
This is an interesting term with a weird history but essentially concerns the rate of advancement of longevity technology versus the time passed. 

It refers to the time at which longevity technology advances faster than people age - and sufficiently fast that it is not merely a shift in the average age of death (i.e., lots of people still die, but live slightly longer on average), but a radical shift where the vast majority of people will not die any longer because the rate is high enough to encompass most of the population.

I consider it probable that we will reach it within the next 50 years, maybe earlier, with the current rate of improvement of longevity tech, though estimates like this are hard to reach - and there's the big issue of poverty potentially limiting access, which may result in geographically variable reaching of L.E.V.

##### Singularity
A certain portion of transhumanist communities are a little bit in love with the idea of a superintelligent AI that accelerates technological development faster than humans can keep up. They argue that AI will eventually cause a "singularity" in both the AI increasing it's own intelligence, and the mass-scale development of extreme technological improvements. There are various questionable aspects of this and I would not classify myself as someone who believes such a singularity will occur and I find it too close to religion (and too dependent on a notion of singular "intelligence" and a certain type of AI architecture) for my liking.

However, I personally suspect that if projects like Sci-Hub and Library Genesis (which promote free sharing of knowledge and information) continue unabated - in combination with the increased access to internet and general education across the planet, and automation of more menial labour - we may see a technological singularity (arguably, we're already close, as the research produced each year is more than even a pretty large group of people would ever be able to read and comprehend at once) where technology exponentially improves - not from an AI, but from sheer mass of humanity and rapid idea sharing.

In combination with 3D printing technology and hopefully a full self-replicating high-sophistication machine this would truly push things far (it's one of the reasons I am very enthusiastic about [self-replicating 3D printing](https://reprap.org)), as it would enable rapid prototyping, testing, improvement, and sharing in a fully digital format distributed amongst millions of people, in the same way that Free Open Source Software development has radically accelerated the development of software.

##### Mind-Uploading/Digitization
Mind-Uploading is the notion of essentially converting the network and processes currently encoded by your biology - in your brain and neuroinfluencing organs (things that produce mind-altering hormones), into information and programs that can be run by one or more computers - essentially virtualizing your conciousness. Obviously, this is not possible today, but exploring the implications and capabilities of this is an important aspect of transhumanism (especially as, hopefully, longevity escape velocity ensures many people today *do* live long enough to see the day it becomes possible).

There are some pretty heavy discussions to be had on things like whether you'd be willing to upload yourself onto a computer someone else was in control of (I wouldn't - FOSS all the way), and once you digitize, you need to consider the notion of backups, and implications for your notion of identity. These are all interesting topics, but somewhat out of the scope of this article.

###### Moravec Transfer
One of the issues some people have with the idea of mind uploading is the thought of being a "copy". Naturally this depends on your view of identity, but it is a common objection. One solution to this is something called a "Moravec Transfer" (first proposed by [Hans Moravec](https://en.wikipedia.org/wiki/Hans_Moravec)), in which, instead of essentially reading your brain, destroying it, then running the information in a program, you instead perform a continuous transfer - neuron by neuron - while still remaining awake and otherwise functional. It preserves continuity of conciousness.

{{< aside "🗒 Note on the Notion Of Rights" >}}
Some people may have noticed my use of the term "right" in this page. There have been many (sometimes good, sometimes bad (and sometimes fascist)) criticisms of the notions underlying traditional liberal systems of "human rights" or treating them as some essential property of the universe. 

When I use the term "right", I generally do not mean it this way - instead I use it to describe a property which any society that I or someone subscribing to my beliefs should have for it to be remotely acceptable to me, and that should at the very least be accessible by everyone even if only by leaving their current society for one where the right is upheld (the notion of "exit" as espoused by some postmodernist thinkers may be relevant here even if not completely identical).

This is generally speaking a complex topic that is really a bit off-topic for this article, but hopefully this should give the general idea.
{{</ aside >}}

[transtrans]: https://www.reddit.com/r/transtrans
[^trans2]: or as I call it, trans²humanist 
