---
title: "Transfem Diy HRT Info"
date: 2022-02-03T05:26:27Z
draft: false
tags: ["trans", "hrt", "diyhrt"]
---
This is mostly a little archive of information on DIY Hormone Replacement Therapy for transfeminine people. 

It is based on my own experiences. This means that information is most accurate for estrogen as tablets and spironolactone, I don't know dosage information very well for other methods though I will update as time goes on if I feel like it.

Obviously it is preferred to go through a less legally-grey route or via informed consent clinics if possible (for both cost and reliability/safety reasons), but in many places this is inaccessible or illegal or has often humiliating requirements and delays (like the UK).

### Resources

[wikipedia page for HRT](https://en.wikipedia.org/wiki/Transgender_hormone_therapy)

[list of sites to buy HRT](https://hrt.cafe/) - the personal experiences I have are with InHousePharmacy.

[subreddit for trans DIY HRT](https://reddit.com/r/transDIY) - they have other resources in the sidebar but I have found the community to be occasionally transmedicalist and gatekeepy about dysphoria, though I hardly interact with them so I don't have a good sample.

[github repository with resources for accessing HRT](https://diyhrt.github.io/) - this is a site linking to a very useful google document. 

It's one of the most thorough explanations of things I've read though and is probably the most valuable resource here for dosage information and medication types as well as sourcing. 

I'll be writing out a simplified version of some of this information and of course some of this document is more US centric (pricing wise especially). 

This site hosts a {{< tref "diyhrt/" >}}local copy{{< /tref >}} of both transmasc and transfem guides, both raw originals and versions localised to the site theme. I encourage you to archive them as well since they've reportedly been taken down before and may be again.


## The shortest summary of what I would say if you are indecisive about specifics
Always use bioidentical estrogens - estradiol hemihydrate or estradiol valerate.

If you can afford it get the expensive GnRH agonists (proper ones). However in practise the ones you'll find are not good enough for this (nasal spray) usually and are annoying to take (3x day due to short biological halflife).

### Anti-Androgens in Practise
* 100mg/day spironolactone - Comparable (slightly cheaper) than cyproterone and much safer. Can increase strain on kidneys due to being a diuretic and is a poor antiandrogen but it will usually do the trick. 

    * If you don't have access to blood tests at all then this might be a better option but usually the others are preferrable if you can access them and some bloodtests.

    * This is what I went with when starting my DIY HRT but now I'd encourage bicalutamide and slightly higher estrogen dosage if possible. 

    * I only went for spiro because it was dirt cheap on the site of my supplier (InHousePharmacy.vu) - I'd recommend bicalutamide if at all possible because of the static risk levels as long as you can get at least 2 blood tests in the first year.

* 50mg/day bicalutamide - slightly more expensive, but you only need safety bloodtests in the first year about liver and lung toxicity (after which risks are very small).
    This one will result in high testosterone levels but it's fine because it stops that from acting.

* 12.5mg/day cyproterone acetate - use pill cutters since normal doses are usually too high.
  * This has cumulative risks and you should get blood tests. Though the risk is still low.
  * Definitely still a good option in practise if you have access to blood tests, and only mildly more expensive than spiro.
* See the anti-androgen section in {{< tref "diyhrt/ultimate-transfem-guide" />}} for details.


### Estrogen Dosing in Practise
I go with 2x2mg/day estrogen tablets sublingually/cheek dissolving. 

This may result in shifting E levels due to the spike from directly transferring to the bloodstream without a pass through the digestive system. 

If you're worried about that, either immediately swallow the doses (and up it to 3mg/dose), or take 3x1mg doses instead spread around the day (it's slightly less but will be more stable in levels).

Pills are easy to obtain and easy to take, but may be more expensive. If you're more confident, take a look at the parts about injection in {{< tref "diyhrt/ultimate-transfem-guide" />}} (also have a search for autoinjectors) and patches are a good option if you struggle with taking pills. I don't really have any experience with them so I won't comment here but take a look in the guide and see.

It is also possible to do *monotherapy*, in which you take a higher dose of estrogen (6-8mg/day) to suppress testosterone while having feminising effects without an anti-estrogen. This is a good option for those on injections but it can work for people using other methods.

### Supply
In practise, I have used inhousepharmacy.vu and they did their job well, though being an international provider orders would usually take ~3 weeks and were more susceptible to logistics issues especially in the age of the CoVID-19 pandemic. 

Mostly I'd recommend taking a look at https://hrt.cafe for info and do consider homebrew sellers especially if they are domestic producers as within-country transport reduces the risk of capture and destruction by customs people at the border.



