---
title: "License Classification Summary"
date: 2021-10-26T11:52:57+01:00
draft: false
---
This blogpost provides a summary of how I think of the spectrum of licenses from copyneutral/permissive licenses to copyleft/viral licenses. Of course, it is more complex than this, but this gives a good idea for use in discussions where the exact specifics are less relevant.

Generally, the idea behind copyleft licenses is that some organisation using and distributing a project dependent on the copyleft licensed project must also license their own with similar requirements to redistribute source code. 

In practise, this is a spectrum as the requirements for redistribution of source can vary from nonexistent (ultra-permissive), to things such as "if you use it to host a network service, you must distribute it's source to any users of that service if requested" (ultra-viral).

I describe licenses as varying degrees from copyneutral to viral based more granularly on the following two primary factors:

* How much credit is needed to be given to the author of the original in any project that uses it:
  * The most extreme permissive licenses like the [Unlicense][unlicense] do not require any credit be given.
  * Most others like the [MIT License][mit] require credit to the original project by dependent projects.
* How deeply integrated the project need be before the dependent is required to be made open source to those who have received the dependent's finalised build:
  * Licenses generally considered "permissive", like the [MIT License][mit] and [Unlicense][unlicense], usually do not impose any requirements for open-sourcing dependents.
  * Softer copyleft licenses, like the [LGPL license][lgpl], may only require open-sourcing if the code is directly used from within a project rather than accessed through interfaces (as in dynamic library linking).
  * Middling copyleft licenses such as [GPL][gpl] typically require any inclusion of the project in a dependent that is distributed to result in licensing the dependent under the same license or a related license with similar source code redistribution requirements. This is where the notion of a "viral license" comes from - it "infects" dependent projects.
  * Harder copyleft licenses do exist, but they are rarer. The most commonly used I am aware of is the [AGPL][agpl], which defines network transmission as a form of distribution (along with the [GPL][gpl]'s typical definition of 'distributing the program binary or scripts'). 

    In practise this means that if a company such as Google were to use an [AGPL][agpl]'d project as a dependency, they would have to license all of their code under [AGPL][agpl]. Consequentially they would have to release the source code of all of their projects to their users since those programs transmit over the network (for instance, in the form of search results). [GPL] dependencies would not infer the same requirements, as the binary was not itself distributed to the users in any form.

[unlicense]: https://unlicense.org/
[mit]: https://en.wikipedia.org/wiki/MIT_License
[lgpl]: https://choosealicense.com/licenses/lgpl-3.0/
[gpl]: https://choosealicense.com/licenses/gpl-3.0/
[agpl]: https://choosealicense.com/licenses/agpl-3.0/
[gnuallpermissive]: https://www.gnu.org/prep/maintain/html_node/License-Notices-for-Other-Files.html
[cc0]: https://creativecommons.org/choose/zero/
[apache]: https://www.apache.org/licenses/LICENSE-2.0


This isn't in any way a full description of the licenses, but for the purposes of reasoning about their strength-of-copyleft, it is a decent metric even if it misses some of the specifics (for instance conflicts between FOSS licenses due to certain restrictions and the efforts of FOSS communities to prevent it via both multiversioning of licenses like "GPLv2+", or changing of the licenses themselves to allow for it).

For me personally, the "stack" of licenses I tend to see and use (depending on how copyleft I think a project should be), with a focus on compatibility from top (most permissive) to bottom (most viral), is as follows:
* [The Unlicense][unlicense] and [CC0][cc0]
* [MIT/X11 License][mit] and [GNU All Permissive License][gnuallpermissive]
* [Apache License][apache] (this one has some useful protection against attacks on software freedom based on patents)
* [LGPLv3+][lgpl]
* [GPLv3+][gpl]
* [AGPLv3+][agpl]

For some larger licensing lists, take a look at the [Free Software Foundation list](https://www.gnu.org/licenses/license-list.html#SoftwareLicenses) and the [Choose A License list](https://choosealicense.com/licenses/#unlicense).
