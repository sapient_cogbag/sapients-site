---
title: "Maps and Territory"
date: 2022-07-24T19:35:50+01:00
draft: true
tags: ["philosophy", "epistemology"]
---
In this article, we will discuss representation and analysis of the concepts of "map" and "territory", and how they relate to our ability to reason about reality, supposing that some form of reality exists, with the concept of "map" and "territory" referring roughly to those concepts as referred to by the phrase "the map is not the territory".

### Defining a Map
The first thing to do is formalise how a "map" exists, such that we can actually analyse the concept properly, in particular by defining the following three components:
* $\overset{E}{\rightarrow}$; - an Encode process, that occurs or can-have-occurred upon whatever can be said to be reality and points to some part of whatever reality is that can be said to be the map. 
* $\overset{D}{\rightarrow}$; - a Decode process, that occurs upon whatever piece of reality the Encode process produces or refers to - which produces a potentially nonfinite set of all possible realities that this map encodes. If, for a given map encoding $K$;, reality as-it-exists is part of this collection of all possible realities that would be produced by this process, the map is said to be accurate.
* $\overset{M}{\leftrightarrows}$; - an optional process that converts encodings of the map into abstract symbolic information and vice-versa, if such a concept is coherent. if it is, then it should be possible to bidirectionally perform it i.e. go from abstract symbolic information to some portion of reality that encodes that information.

This is a very abstract definition, as it is intended to be usable upon any notion of reality that may exist, and really could theoretically apply to almost any hypothetical object that can (or can't) be conceived of. Importantly, the way we define and use $E$; and $D$; does not depend upon being able to coherently unravel the inner structures of reality (in some sense this is related to certain parts of [category theory](https://en.wikipedia.org/wiki/Category_theory), and to some degree we are attempting to define [universal properties](https://en.wikipedia.org/wiki/Universal_property). 



