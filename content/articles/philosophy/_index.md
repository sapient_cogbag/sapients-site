---
title: "Philosophy"
date: 2021-11-01T15:46:03Z
flatten: true
cascade:
    tags: ["philosophy"]
---
Section dedicated to my musings and thoughts and other such related to more abstract philosophy and ideas for general argument forms and structures.

Note - I have a degree related to maths. This means that a lot of my philosophy stuff is formally argued by framing it in mathematical terms to ensure rigourous analysis as well as making my meaning clearer.

