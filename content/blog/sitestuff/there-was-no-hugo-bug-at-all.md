---
title: "There Was No Hugo Bug at All"
date: 2022-06-21T02:30:52+01:00
draft: false
finalised: true
tags: ["site-meta", "hugo", "programming", "golang"]
---
So it turns out, {{< tref "a-weird-hugo-bug-i-found" >}}there was no hugo bug whatsoever {{< /tref >}}, only user error.

For a short summary - when I commit to this site's git repository, I run a pre-commit hook that updates the datetimes of any modified pages that *have* dates to match the current time, unless that page is marked finalised.

This is done by a python script in the folder of the repository `build-scripts`, which gets run on the list of files output by a `git diff-index --ignore-space-change --name-only --diff-filter=AM HEAD 'content/*'`, does a bunch of parsing, and modifies any frontmatter keys that are close to "date" to have datetimes matching the current time, if they aren't marked with a finalised value that is true-ish.

To replace this datetime, it has to retrieve the current datetime. Previously, it was using python's internal datetime library to do this.

Unfortunately, this was not including the required '+01:00' timezone output information in the resulting datetime string that indicates that the time is one hour ahead of UTC (I live in the UK, which does the accursed practise of DST).

This meant that the pages would now have a timezone of UTC as according to hugo when it read the files. It's own time includes the correct +HH:MM timezone identifier, which it then translates to UTC internally by *subtracting* that +HH:MM timezone identifier from the datetime before it.

This means that the UTC time according to hugo, is before the reported UTC times in the pages (which have the same datetime identifier but lack the timezone information that got subtracted from `hugo`'s internal time).

Hugo does *not* render pages that are in future periods of time! This is actually a useful feature if you want to get content out after a specific date but want to write it earlier, however in this case it worked against me, because `hugo` simply thought all my pages were in the future and did not render them at all - unfortunately, this was not indicated in even the most verbose logging I could manage, which would have been helpful, I think.

Either way, at the end of the day, the fix was simple - instead of using python's inbuilt `datetime` library, I called out to `date --iso-8601=seconds` and used the output of *that*, since that produces the fully correct time string.... or at least consistent to what hugo produces, which I *hope* is correct, but you never know with datetime.

So what does this mean for the potential {{< tref "at-some-point-imma-move-to-zola" >}}move to Zola (a static site generator similar to hugo but written in rust){{< /tref >}}? Well, the particularly nasty and difficult to diagnose error thing *in-specific* was user error, but the issue of me being able to actually understand, debug, and perhaps fork the code to add custom features, as well as template system builtins for my hive of disgusting template messes currently implemented with a horrible abomination in hugo, means the move is still on the table, though at a much lower urgency.

Either way, I hope this serves as a useful little poke at the weird inconsistencies in datetime libraries, and how often they can be difficult to diagnose. Also the fact that it is always better to have more logging than less, all else considered equal, especially when your project is large and has obscure features (like the "delaying posts for the future" thing).
