+++
title = "{{ replace .Name "-" " " | title }}"
date = "{{ .Date }}"
draft = true
tags = ["series"]
[cascade]
tags = []
series = ["{{- .Name -}}"]
+++

