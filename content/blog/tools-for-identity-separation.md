---
title: "Writing Tools for Identity Separation"
date: 2022-04-12T00:38:05+01:00
draft: true
tags: ["programming", "rust", "git", "infosec", "technology", "foss"]
---
People who know me well will probably tell you, when asked, that I take digital privacy very seriously, and I'm going to tell you about a core aspect of that, and some progress in a little tool I am making to enable it.

The key component of digital privacy and anonymity is *identity separation* - that is, minimising the ability of third parties of various adversarial capabilities (e.g. casual malicious individuals, to a very dedicated malicious group (like fascist orgs or recruiters searching over social media), to local police departments, to larger tech corporations, to nationstate-level threats) to link the identity used to perform one collection of actions, to the identity used to perform another set.

For me at least, I have two primary identities - my IRL + semi-professional identity, and my online identity. For me I actually consider them both "myself", and the primary adversaries who I want to prevent from linking these are either casual internet browsing folks, or more dedicated adversaries on the scale of fascist organisations or the people employed at corporations to scan through social media. Several people know of the connection but I trust them enough not to leak it in a public place.

The major reasons for avoiding linking these identities are as an issue of employment and safety from dedicated internet trolls and fascists (think the denizens of KiwiFarms) - given my very outspoken leftist/anti-capitalist/anarchist political beliefs and being part of a few minority groups, which make me a prime target for harassment and potential refusal for employment.

I've mostly got this down pretty well, but as someone working in a commandline environment and who programs a lot, I want to design far more reliable tooling for this. In particular, I want to design tooling for `git` (and in the process provide a simple framework for other tools and generic environment variables or generated config directories on a per-identity basis with intuitive, safe defaults) that will let me check for and prevent identity leaks either in things like commit text or committed changes, or in the GPG keys used to sign git commits.

Right now, all my code is in the name of `sapient_cogbag`, which is fine but poses a significant obstacle to myself in providing useful evidence for employment - I believe that my strong views on unionisation, worker's rights, the destruction of capitalism and my favourability towards the leaking of proprietary information and code, if publically linked to my IRL identity (which to be clear would be permanent if I fucked it up), would permanently and significantly damage my ability to seek any employment in any company.

Ergo, I'm not willing to risk things until I have better tools for preventing identity leakage through `git` processes, and hence my need for identity management is critical, and I intend to develop the tooling for that.

#### Sensible Defaults
One of the most important aspects of this process is the idea of sensible defaults. I should not, when I put in a name, email, and GPG key, have to fill out all the information for `git` signing keys if it matches that default, or worry about configuring some of the weird options `git` has around `sendemail` and that whole workflow. 

If I were to, say, näively use Rust's `Default` trait when configuring defaults, it would result in errorprone code. `Default` is not so useful for anything but the most simple default construction when working with configuration options, where in complex configurations you'll often want to derive default options from the value of previous options if present - for instance using a globally specified email as default for email fields - and it is much more reliable and less repetitive (hence less prone to errors, see DRY[^DRY] principle).

To help with this, I developed a trait that looks like the following:
```rust
/// Trait indicating that a type can have various defaults filled in, when provided with other
/// types.
pub trait FillableDefaults<'a> {
    /// What types (as tuple) are needed to fill in all the defaults.
    ///
    /// # Good Usage
    /// Make this contain types that themselves are a [FillableDefaults::Finalised]. That way you
    /// can ensure that you never pull values from types that have not already had their defaults
    /// filled in.
    ///
    /// If we had real variadic generics I could implement this as a bound but sadly, no :(
    type Needs: 'a;
    /// The finalised type that has all defaults filled in ^.^
    type Finalised;

    /// Function to finalise all the defaults of this type.
    fn flush_local_defaults(&'a self, other_values: Self::Needs) -> Self::Finalised;
}
```
{{< aside info block >}} The lifetimes here are based around the fact that config options can be stored as simple `&'a str` or raw value types for most config file objects, enabling zero copy semantics that are still very value-ish {{< /aside >}}

Now, I am actually considering shifting to a design that looks more like the following - because it might end up being more clean (also, perhaps, it may in the long term be better to switch to a `From` or `Into` implementation):
```rust
pub trait CascadingDefaults<'a, Prerequisites: 'a> {
    type Completed;
    fn with_cascaded_prerequisites(&'a self, prerequisites: Prerequisites) -> Completed; 
}
```

This essentially just allows you to derive a new collection of defaults from any prerequisite collection of defaults and produce a finalised, flat value with no more defaults to fill in (that may of course be dependent on arbitrary trees of values that themselves may need to be filled in by providing or deriving further defaults).

In my program, for all the configuration options, I essentially have a `ConfigStruct` version - which can have many empty fields or weird stuff needed for deserialization to work - and a `ConfigStructFinalised`, which removes any cases where defaults must be filled in. The former is near-exclusively used by the `serde` parsing step, and the latter is used by the program to generate any output.

It's a very useful method of deriving defaults sensibly.

#### Documenting Config Files
Something I have always found frustrating writing CLI tools (or tools in general) is documenting configuration files automatically (or at least, in line with the code), as I lacked a good method to even do it. 

However, while I was writing this, [I did some searching and found an excellent blog post](https://tarquin-the-brave.github.io/blog/posts/generating-config-reference-rust-cli/) that suggests the usage of *JSON schemas* (something I was unaware existed until now) as generated by [this cool crate that matches `serde`](https://docs.rs/schemars/latest/schemars/index.html), with any number of JSON schema -> HTML/alt format/etc. tools once you've gotten that far.

So I annotated all of my parsing structures with this crate - which automatically matches `serde`, too! - to help me in future, and wrote a command to dump the relevant JSON schemas out.

{{< aside note >}} I had to annotate any struct member containing `#[serde(default = ...)]` rather than a simple `#[serde(default)]` with `#[schemars(default)]` to override the serde annotations. 

`schemars` seems to produce weird errors about closure specification when providing default functions, unfortunately. 
{{</ aside >}}

Then, I installed [json-schema-for-humans](https://coveooss.github.io/json-schema-for-humans/#/) via `sudo pip install json-schema-for-humans` (sadly, there were neither any Rust libraries for generating docs from json schema, nor did this python package have a version in my distro repository or the AUR, so I had to install it with `pip` :/), and wrote a little integration into my documentation generator subcommand to use this program if present.

[^DRY]: Stands for "Don't Repeat Yourself", which is important as it makes it harder to introduce bugs by only updating code in one place when it needs updates in multiple.

