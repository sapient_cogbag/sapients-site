---
title: "A Practical Examination of Mind Uploading"
date: 2022-09-10T10:07:11+01:00
draft: true
tags: ["futurism", "future", "technology", "politics", "transhumanism", "philosophy"]
---
Mind uploading, or digitization, is a term that broadly refers to transferring "you" - your brain, your nervous system, or whatever else you consider that makes yourself *you* - into a representation that can exist on a digital computer in some shape or form, and be run on that same computer. Naturally, once this is done - as long as you build redundancy into whatever happens to hold your mind - you are effectively immortal in most implementations of such a system. It is the ultimate life extension, and leaves you essentially immune to starvation, disease, and similar such things - at least as long as you have some power source or access to electricity.

### The Bare-Bones Technology - Uploading and Execution
Uploading a human brain to a digital representation is no small feat, and running such a thing on a sensible number of chips, in a sensible power budget, is arguably even more challenging.

#### The Initial Upload
There are two abstract methods by which the initial upload may occur - bulk upload, or continuous upload (also known as a moravec transfer).

##### Bulk Upload
The more commonly known method of mind uploading is, abstractly, described as freezing your brain in a specific state, disassembling it, and measuring the state of every neuron and connection and storing that digitally.

This method is one that is definitely less complex in terms of implementation - as it does not require facilitating constantly-changing connectivity with biological neurons - and it can work if someone preserves their brain appropriately even before the technology is developed.

##### Continuous/Moravec Transfer
This is a method of mind uploading is intended to reduce some of the issues people have with continuity of identity. It involves essentially slowly consuming your brain neuron-by-neuron, converting neurons and connections to digital form over a short period of time - as opposed to "pausing" yourself, tearing down your brain to extract neural connection information, and transferring it to digital format all in one go, which is the case with a bulk upload.

It is more speculative, because it requires maintaining connectivity with existing neurons while extracting the information one-by-one at the interface boundary, and performing replacements at the interface boundary as well.

#### Technological Paths
For the initial upload, there are several relevant technological discussions to be had for both Bulk and Continuous/Moravec methods of mind transfer, and each has actually got pretty different requirements for how to do such a thing.

##### Technological Paths - Bulk Upload
With bulk uploads, there are several necessary technologies. 

###### Brain Preservation
The first is a technology to preserve the brain - in particular the neurons and the various receptors present in neuron connections - in sufficient detail that the pattern of neurons and connections and connection sensitivities to various neurotransmitters is reconstructible. 

This technology arguably exists *right now*, in the form of certain types of bodily plasticisers that essentially slowly convert your bodily fluids into solid polymers, with all the more complex molecules preserved in-place - naturally they may decay, but because the decay products are not super reactive they should not disrupt the preservation. The more common "trope" for preservation is in the form of cryogenics, in which you flash-freeze your body (or just the head) immediately after death to attempt to preserve neurochemical information, and in fact this is a large industry today. 

I consider the plasticisation method superior, though, even though it's not really in the popular conciousness, for several reasons:
* Cryogenics damages cell walls and may disrupt other chemical information in a way that is not recoverable
* Cryogenically preserved brains need to be kept in a sub-0°C environment, which requires constant maintainance - the plasticisation/polymerisation method does not require this
* Cryogenics, practically, can only be applied after death. This gives time for cells to begin dying and neurochemical connections and neurons to begin dying and breaking down, unless it is applied within seconds to minutes. The plasticisation method can be applied just before death, preserving a frozen brainstate before loss of oxygen and other decay factors result in destruction of vital information.

One of the benefits of bulk upload is that in theory even people on the verge of dying right now can benefit from it, if they have appropriate philosophies of identity that would allow the uploaded version of themselves to be considered "themselves". The technology for preservation exists, even if it is not yet widely available, and as long as the required future technologies are developed and the preserved brains are not thrown away, those people can live (again, under certain philosophical concepts of identity).

###### Brain Simulation
The second necessary technology is some method to actually *run* an uploaded person. That is, to model the data in sufficient resolution and with correct-enough behaviour that the person in question would consider themselves, themselves.

This would be in the form of a computer program, probably with specialised hardware that is better suited for simulating the neurochemical networks that were digitised in the process. This is unlikely to match current processor architectures, because current architectures have the [von-neumann bottleneck](https://en.wikipedia.org/wiki/Von_Neumann_architecture#Von_Neumann_bottleneck) which is a huge issue when talking about networks with unpredictable, hyper-distributed and asynchronous, large-scale access patterns.

This issue can be solved by several means:
* Attempting to use memory access prediction to reduce some amount of overhead for accessing e.g. neuron connections, neuron activation states, etc.
* Using the internet itself as a network such that each computer only handles a small amount of the work.
* Developing a network-on-a-chip with specialised internal protocols and localised memory and storage to essentially recreate the internet more efficiently and with less latency in hardware. This is essentially avoiding the use of a single monolithic memory chip - where access to that chip by 10s of billions of simulated neurons becomes a chokepoint - and instead providing much smaller but easily accessible in parallel chips.

In practise, all three of these methods can be combined, for example, by networking multiple specialised chips together over the internet to join those neural sub-networks as one, which would even allow traditional CPUs to take part in the process.


