#!/usr/bin/env python3
"""
Processing step to convert maths in <raw-maths> and <raw-display-maths> tags to proper html
if <PARSE-FOR-MATHS-RENDERING-PLEASE-AND-THANK-YOU-NYAA> in either upper or lower case is present
(it gets removed). Only maths past that is actually parsed meow.

Note that this must be called from the hugo root directory with a 
built KaTeX to correctly access the KaTeX thingy ^.^
"""

import html, subprocess, sys, re

MATHS_TRIGGER="PARSE-FOR-MATHS-RENDERING-PLEASE-AND-THANK-YOU-NYAA"
KATEX_BIN_PATH="./node_modules/.bin/katex"
inline_maths_tags=("<raw-maths>", "</raw-maths>")
display_maths_tags=("<raw-display-maths>", "</raw-display-maths>")

# Note that these matches *include* the tag - we want to get rid of it in substition 
# after all nya. *? is non-greedy *
INLINE_REGEX_COMPONENT=r"<raw-maths>(?P<inlinemaths>.*?)</raw-maths>"
DISPLAY_REGEX_COMPONENT=r"<raw-display-maths>(?P<displaymaths>.*?)</raw-display-maths>"
# Tags: Multiline, Dot matches all (including \n), Unicode matching nya.
MATHS_TAG_REGEX=r"(?msu)" + r"(?:" + INLINE_REGEX_COMPONENT + r")|(?:" + DISPLAY_REGEX_COMPONENT + r")"
# Like above but with $$; and $$$$;
# Does not match if interrupted by proper html tags nya, or if the starter point
# is an unmatched endpoint nya
DOL_INLINE_REGEX_COMPONENT=r"\$(?!;)(?P<inlinemaths>[^<]*?)\$;"
# This one must come *first* for the multidollar thing nya
DOL_DISPLAY_REGEX_COMPONENT=r"\$\$(?!;)(?P<displaymaths>[^<]*?)\$\$;"
DOL_MATHS_REGEX=r"(?msu)" + r"(?:" + DOL_DISPLAY_REGEX_COMPONENT + r")|(?:" + DOL_INLINE_REGEX_COMPONENT + r")"

# Commands nyaaaAA
INLINE_KATEX_COMMAND=[KATEX_BIN_PATH, "-t"]
DISPLAY_KATEX_COMMAND=[KATEX_BIN_PATH, "-t", "-d"]

def log(*args, **kwargs):
    kwargs["file"] = sys.stderr
    # if args is empty just make newline nya
    if len(args) == 0 or (len(args) > 0 and len(args[0]) != 0):
        return print(*args, **kwargs)

def maths_preprocessing(raw_tagpair_content):
    """
    Turn raw tag content into something that ./node_modules/.bin/katex can consume nya

    Notably:
    * Unescape HTML characters
    * Then remove trailing and preceding whitespace and then $ signs if present meow.
    """
    return html.unescape(raw_tagpair_content).strip().strip('$')
   
def compile_inline_maths(raw_maths):
    """
    Run KaTeX with appropriate arguments to generate inline maths meow
    """
    log("Converting raw inline maths:\n{}".format(raw_maths))
    output_result = subprocess.run(
        INLINE_KATEX_COMMAND,
        check=True, 
        capture_output=True,
        input=raw_maths,
        text=True,
        encoding="utf-8"
    )
    log(output_result.stderr.strip())
    return output_result.stdout.strip()

def compile_display_maths(raw_maths):
    """
    Run KaTeX with appropriate arguments to generate display maths meow
    """
    log("Converting raw display maths:\n{}".format(raw_maths))
    output_result = subprocess.run(
        DISPLAY_KATEX_COMMAND,
        check=True, 
        capture_output=True,
        input=raw_maths,
        text=True,
        encoding="utf-8"
    )
    log(output_result.stderr.strip())
    return output_result.stdout.strip()

def display_katex_version():
    """
    Displays KaTeX version information (doubling as a check for the existence of appropriate 
    katex nya)
    """
    output_result = subprocess.run(
        [KATEX_BIN_PATH, "--version"], 
        check=True, 
        capture_output=True,
        text=True,
        encoding="utf-8"
    )
    log(output_result.stderr.strip())
    katex_ver = output_result.stdout.strip()
    log("Processing mathematics with KaTeX version {} at {}".format(katex_ver, KATEX_BIN_PATH))
    


def generate_single_substitution(special_regex_match):
    """
    Function for turning regex match objects into maths meow.
    ?P<inlinemaths> should contain the still-htmlized-and-unstripped inline mathematics to 
    parse, if present.
    ?P<displaymaths> should contain the still-htmlized-and-unstripped display mathematics to
    parse, if present nya.
    """
    if special_regex_match.group("inlinemaths") is not None:
        clean_maths = maths_preprocessing(special_regex_match.group("inlinemaths"))
        return compile_inline_maths(clean_maths)
    if special_regex_match.group("displaymaths") is not None:
        clean_maths = maths_preprocessing(special_regex_match.group("displaymaths"))
        return compile_display_maths(clean_maths)
    log("Match without either inlinemaths or displaymaths group found??")
    return ""

def parse_and_write_maths(maths_section):
    """
    Parse all the maths in a section of a HTML document.
    """
    display_katex_version()
    log("Looking for maths using regex {}".format(DOL_MATHS_REGEX))
    log("Then, looking for maths using regex {}".format(MATHS_TAG_REGEX))
    dol_maths_regex_pattern = re.compile(DOL_MATHS_REGEX)
    maths_regex_pattern = re.compile(MATHS_TAG_REGEX)
    log("Inline maths KaTeX command: {}".format(" ".join(INLINE_KATEX_COMMAND)))
    log("Display maths KaTeX command: {}".format(" ".join(DISPLAY_KATEX_COMMAND)))
    log()
    subsfn = generate_single_substitution
    return maths_regex_pattern.sub(subsfn, dol_maths_regex_pattern.sub(subsfn, maths_section))

def main():
    """
    Scan forward with juicy regexes
    """ 
    file_content = sys.stdin.read()
    divided = re.split(r"<(?:" + MATHS_TRIGGER + r"|" + MATHS_TRIGGER.lower() + r")>", file_content)
    # pretrigger to reduce computation nya
    (pre_maths_content, divided) = (divided[0], divided[1:])
    sys.stdout.write(pre_maths_content)
    sys.stdout.flush()
    
    if len(divided) > 0:
        to_substitute_parsed_maths = ''.join(divided)
        final_res = parse_and_write_maths(to_substitute_parsed_maths)
        sys.stdout.write(final_res)
        sys.stdout.flush()
        

if __name__ == "__main__":
    main()

