---
title: "So Now This Site Has Real RSS"
date: 2022-06-21T00:52:44+01:00
draft: false
---
A couple of people have been using janky RSS feeds by accessing `<some-base-url>/<some-part-of-the-site>/index.xml`. Because, up until now, I have always used relative URLs in everything (for ease of distribution in a folder), this meant the URLs in the RSS feeds would essentially have to be pasted on the end of one of my [hosting locations](/about#where-is-this-site-hosted).

Not the most user-friendly experience, but you shall suffer no longer, because I made this site have *real* RSS on all pages that list other pages. Note that almost all links in the site still use relative locations - and this will remain the case apart from where strictly necessary, like RSS feeds - but I now generate the website with a new baseURL for each hosting location.

Furthermore, the RSS links are also embedded on the entry in each listing page - thanks to compressed transfers, this should have minimal performance impact - not just in the titles. 

Hopefully this will make subscribing to the parts of the site you care most about much easier.

Other additions include:
* Expandable sections on listing pages.
* Unfolding the page type sections on listing pages when there is only one section, for ease of navigation when opening simpler sections of the site.
* On each listing page, a separate section of the list for every series underneath it (that is not nested further within a series).
* Ordering series in reverse order in various places, so that the start of the series is the first thing seen rather than the latest post as with other parts of the website.

All sorts of fun stuff, really. Subscribe to RSS feeds liberally :)
