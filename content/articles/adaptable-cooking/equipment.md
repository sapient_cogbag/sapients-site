---
title: "Equipment"
date: 2022-01-07T08:06:20Z
draft: false
---
One of the most important things in making food is the tools needed to do so. While not having access to a proper kitchen might at first seem to make thinking about this pointless, the opposite is in fact true - it means you need to think more about both what equipment you have and what equipment you can get that you can use within your accommodations.

#### Considerations When Obtaining Equipment
Several considerations need to be made before obtaining equipment needed for cooking that is adaptable to your access to a kitchen.

##### Does it require power, and if so how much?
One question to ask yourself when buying equipment is about where it will get power from (if it needs it). Does it take electrical power or gas power - and is that easily available in whatever environment is preventing you from reliably having access to a kitchen?

For instance, if you're lacking access to a kitchen because you're living in a shack 100 miles from the nearest civilisation, then anything electrical is out of the window unless you want to haul a whole generator setup with you, and you should prefer gas-powered equipment. 

However, if like most people you live in an urban area and have at least some way to get electricity from a plug socket for some part of the day, then obtaining electrical devices is almost always a better bet due to both ease of applying power (just plug into a socket), and portability (no need to haul around a tank of gas). I'd argue *{{< special "electrical devices should pretty much always be preferred." />}}*

If something requires a lot of power - 1000+W - consider whether you'll get in trouble for using that from the room or place you're staying in. Some places will not let you have devices over a certain wattage, though in practice I doubt they are regularly checking consumption on a per-room basis. It's still something to be aware of, however.

##### How portable is it?
Another to ask is about the portability of the device. Depending on your accommodation and kitchen situation, this question has various degrees of importance. The more frequently you need to move it from it's position - presumably due to changing accommodation circumstances - the more portable the device needs to be.

If you're moving around a lot, large bulky pans and a whole microwave are not really viable to take with you - and as such you need to prioritise what is most important. 

On the other hand, if your kitchen-situation is a simple lack of physical space in whereever you're living, this question is mostly irrelevant, though more portable items do often take up less space and if your living situation does become more unstable, portable items are useful to have.

With a car or a larger trolley, the importance of this question is reduced somewhat due to increased ability to transport personal affects.

##### How essential is it for the cooking I am able to do?
In an environment where your storage options are limited - think a lack of fridge or freezer, or limited space because you need to be able to carry things by yourself when moving locations - then it's important to recognise that cooking some things becomes impossible or very difficult.

In these cases it's worth considering whether a peice of equipment is useful to haul around if it is not typically used for cooking things that you are storing. 

{{< aside example >}}
Most of the time pans are primarily used to cook *fresh or frozen* ingredients.

If you lack access to a fridge or freezer or other forms of cooled storage, pans are not as useful and you should consider not obtaining them and hauling them around.
{{< /aside >}}

##### Is it worth the monetary cost?
For the case when you intend to obtain a new peice of food preparation equipment, it is useful to determine how long it is likely to see use for and how much food and drink you're actually likely to make in it.

{{< aside example block "EXAMPLE: PORTABLE STOVE">}}
Consider if you are going to be stuck in a budget hotel/motel accommodation for the next two months, and you're aware of that fact in advance so you can buy things to make it easier (within a budget of course).

While you could, say, buy a portable stove off Amazon or Ebay for £70 (price made up for example here), there's a good chance you won't actually make very much food (or any food at all) with it in the time you stay there, for several reasons:
* Likely poor access to refrigerated and frozen storage for the ingredients you'd make on the stove
* The amount of space the portable stove will take up in your travel bags may be exorbitant
* Owners of the accommodation may see it as a fire hazard.
* Cooking on a stove in an area not designed for it is likely to result in issues with ventilation in any enclosed space without a fan as well as the general comfort issues of trying to cook in an area not designed for it.

It is *likely* not worth the hassle to obtain, carry, and attempt to cook with a portable stove unless you have a large excess of space to carry it and are comfortable cooking in cramped and poorly-ventilated areas (while managing to avoid triggering fire alarms!). 

Furthermore, on a limited budget, there are other portable cooking utensils that are probably more value for money in the context of how you're going to use them.

However, if you are staying in places you know are well ventilated (or have an outdoor zone with power access), *and* you have decent access to ingredients to cook with on a stove (either because you're buying and cooking immediately, or have refrigerated storage), and you know you're likely to use it frequently.... it may be worth it.

As the price goes down, of course, the case for "I might want to use it so I should buy it" gets much better.
{{< /aside >}}

#### My Important Equipment List
The first part of this article was discussing the thought that goes into picking good equipment to have for when access to a kitchen is restricted in some way.

Here I list what I consider to be the order of importance for different peices of equipment - if the prices seem too high, it may also be worth looking for people selling used items.

Several of these are so essential most people already have them, but it's important to note them here because if you need to leave quickly and forget them you'll feel it really soon.

##### Can Opener
This is beyond essential. It provides access to sealed, microorganism-free food whether you're in the Antarctic or the Sahara, power or not, as long as you can get to basically any shop at all (hell, people throw out canned stuff all the time).

If you don't already have a can opener that works well, you can get a cheap heavy-duty one for one or two pounds, or you can do what I did and get one which pulls apart the seal of the can to reduce the difficulty of opening it to almost nil. 

If you have weaker muscles for whatever reason or just want to avoid having to deal with constant hassle and risk of slicing open your hands, I'd choose the latter since it's only a few quid more - and the lower (basically nil) contact with the actual content of the cans almost completely removes the risk of corrosion.

##### Cutlery 
Something to actually eat food with that isn't your hands. Most important is a spoon (most things can be eaten with that alone), but they usually come in sets - a teaspoon, tablespoon, fork and knife. 

##### Mug/Cup and Bowl
A container to eat food in or drink from - the mug is the more important of the two, but both are really useful in general for making and eating tasty food even with minimal resources, especially considering the next item in the list.

##### Foldable/Portable Kettle
This item is one that many people are not aware exists. A portable kettle is like a normal kettle, but a single-peice construction (rather than the kettle being removable from the heating element), and typically made of silicone that can be foldedd up for compactness.

Typically you can find these online for £15-£25 and I recommend obtaining one if there's even the slightest chance you'll be in unstable accommodation in future. They are a wonderful invention for several reasons:
* If you don't have access to hot water and boiling water, but can get electricity, this will give you boiling water (and by proxy, hot water)
* It can sterilize local water if it's unsafe
* It allows for using dried ingredients in cooking - dried ingredients typically keep basically forever, and there are many base ingredients that are sold dried and can be rehydrated

{{< aside note block "NOTE" >}}
This is where my knowledge of equipment becomes a little more limited in terms of personal experience, though there is not too much beyond this point before moving into "partial kitchen" equipment of lower portability.
{{< /aside >}}

##### Microwave - or Portable Equivalent
The next part up is obtaining some way of heating food, as it makes proper cooking (or an approximation) far more possible, and opens up the world of microwave meals for a food source as well. You can cook a lot more in a microwave than many people would expect.

The difficulty comes in portability. Searching for "portable microwave" on the internet tends to give you extremely low-power (~40-200 Watt) items (primarily designed for cars) which are incapable of actually cooking food in an efficient manner 

A better search is "electric heated lunchbox", which from my skimming are in the range of 100W to 600W - the latter being equivalent to a low-end microwave and capable of real cooking. However unfortunately, unlike the "portable microwave" searches, electric heated lunchboxes appear to be made of stiff material that is not easily foldable/compressable.

They are certainly more portable than hauling around an unplugged microwave, however, and that's what matters here. If you have regular access to a microwave oven, however, I would recommend just using that instead when possible, but long-term accommodation instability may prevent this. 

These items typically run from £35-£80, the more expensive ones having better power ratings that are viable for cooking with. Finding the power ratings can be quite difficult as well, so be careful if you choose to go the electric-heated lunchbox route.

Take a look at the [Microwave Cooking Guide]({{< relref "guides/microwave-cooking-guide" >}}) for information on the kind of things you can do with a microwave (it's quite a bit more than you might expect). 

##### Portable Fridge
A portable fridge is a useful extension to your capabilities, because it allows keeping some amount of perishable ingredients or storing prepared food. However, like the microwave situation, these typically are not as foldable or compact as the earlier equipment listed and are more difficult to carry around.

These seem to run up into £30-£50 range (at least on Amazon).

##### Portable Stove
These are not something I'd particularly recommend, for the reasons I outlined above in the example. They require pans and other more complex cooking equipment as well as proper ventilation and accommodation where it is safe to stand near open heat.

#### Beyond This Point
Beyond this point you get into constructing an actual kitchen with lower portability and flexibility needed when living in unstable accommodation.

In the adaptable cooking section of this site, many of the pages are tagged with `cooking-capability: <capability>` tags indicating what capabilities are needed for that page to be relevant to the reader.

For instance, {{< tag-annotations "cooking-capability: hot-water" >}} would indicate that the page is relevant to any people with access to running hot water (safe to drink).
