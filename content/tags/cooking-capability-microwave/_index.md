+++
title = "Cooking Capability: Microwave"
default-hide-tag.cooking-capability-microwave= false
+++

All pages here are relevant if you can heat up things in something approximating a *microwave oven*, preferrably using microwaves as a heating mechanism (as opposed to PCT thermal chip heating) as my data comes from the use of real microwaves - though many of the ideas are probably adaptable.
