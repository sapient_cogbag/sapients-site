---
title: "About"
date: 2021-10-16T08:50:43+01:00
draft: false
finalised: true
tags: ["personal", "site-meta"]
---

### What is this page?
It's a page about both me and this website I made. Sort of an FAQ.

### Where can I contact you?
Well, I tend to be accessible on free platforms as well as a couple of proprietary ones (not that I don't try to escape the latter, heh):
* lemmy/threadiverse: `@sapient_cogbag@infosec.pub` and `@sapient_cogbag@sh.itjust.works`
* email: `sapient_cogbag at protonmail dot com`
* matrix: `@sapient_cogbag:artemislena.eu`
* discord: `@sapient_cogbag`. I prefer to use FOSS communication tools if possible, I only maintain this because of my existing social relationships
* [sufficient velocity (I write dodgy worm fanfic)](https://forums.sufficientvelocity.com/members/sapient_cogbag.63618/)

### How did you make this site?
I used [hugo](https://gohugo.io). You can see the source code on [gitlab](https://gitlab.com/sapient_cogbag/sapients-site). Note that the theme part is NOT licensed under CC0, but GPLv3+.

### Who even are you?
Well, that's a question I ask to myself too sometimes. 

If you're asking about the things I'm interested in, well, the answer is honestly pretty close to "everything" but my particular preferences and special interests are more prevalent on this site. Technology, privacy, opsec, philosophy, politics, and STEM are all subjects of deep interest (not just in an academic sense but as a trans person in a very real, visceral sense), as well as cuddling.

If you're asking about my history on the internet, I have a long and complex one. If you're asking what my IRL name is, sorry, but you don't get to have it :).

If you're asking about my identities in the LGBT+ space (and similar), then that also gets complicated. But if you just want pronouns then they/them, x-based neopronouns, and z-based neopronouns are all appropriate. In practise this means I go by they/them or xe/xem or ze/zem.

I am also autistic and have ADHD, which makes for a wonderful pile of executive dysfunction.

### Why did you make this site?
A number of reasons:
* I have said more times than I can count "I should write this down" about random philosophy/maths/political shit and I finally got sick of not having somewhere to put it.
* If I die I want my thoughts to get out there (not that I intend to die, of course, even if the statistics are not in my favour).
* I hope people eventually appreciate my random shit enough to donate so I can survive in Capitalist Hellworld despite being an aforementioned dysfunctional mess.

### Where do you live?
First off, that's a creepy thing to ask. 

Secondly, I live in TERF Island, also known as `<insert prime minister of the day here>`'s personal kingdom, the 51st state, the land of shit food, or the United Kingdom. I don't intend to give out anything more precise than that, however.

### What are your politics?
Far left-libertarian. More precisely, egoist-influenced anarcho-transhumanist urbanist in favour of radical information freedom. That's the simplified version.

### Where is this site hosted?
An interesting question if you've already found the site, but currently it is hosted in two places (more pending), as well as on IPFS (though trying to get that in the public is... hard because IPNS (the name system embedded with IPFS) is unreliable at best and I could not make it work. A Tor Hidden Service is pending access to an always-on machine (like a raspberry pi) at a decent price that is also actually available for shipping.

The current addresses of the site are:
* [sapients-site.netlify.app](https://sapients-site.netlify.app)
* [sapient-cogbag.neocities.org](https://sapient-cogbag.neocities.org) - Neocities *does* automatically place their sites on IPFS, so if you want to grab a copy or pin it, this is the better option than going with my own IPFS stuff.

The site itself doesn't call out to external locations to function - other than the donation widgets - so if you want to archive it, it should be a very simple job.

### Where did you get the logos?

##### RSS
I modified the [svg image](https://www.svgrepo.com/svg/25140/rss) to use my own css attributes and cleaned it up a bit :)

##### Social Media Logos
I obtained the social media logos from https://www.vectorlogo.zone
 
