---
title: "Some Notes on Calculating Arbitrary Dimensional Measures via Parameterisation"
date: 2023-04-10T21:32:01+01:00
draft: true
tags: ["maths", "programming"]
---
Currently, I'm working on a rainbow highlighting plugin for neovim, capable of generating arbitrary new colours while avoiding collisions with the background colour or other items in the colourscheme. This requires generating random points in an n-dimensional distribution while avoiding certain chunks. The space of possible colours is large - on the order of $2^{24}$; bytes in the bare minimum without including overhead per-colour, should we construct any kind of datapoint for every colour when randomly selecting colours via a flat segmentation method (e.g. with [n-dimensional forms of inverse transform sampling](https://en.wikipedia.org/wiki/Inverse_transform_sampling)) as opposed to bulk binary tree methods or similar. 

In practise, it's pretty much required to use some form of n-dimensional integration for almost any method. For simplicity, we pick probability mass functions (functions that are not normalised like a probability distribution function) that are $0$; if they are inside any of a collection of n-dimensional geometric shapes, and 1 otherwise. Integrating this over some hypercuboidal subregion of the n-dimensional space of colours to calculate it's probability weight, then, becomes a task of calculating the *measure* (generalised version of volume) of the union of these shapes within the region, and subtracting it from the total measure of the region. 

Theoretically simple. However, in practise, it remains difficult to do this without doing computational approximations by slicing the space up into small chunks and iterating over them. What we want, is to analytically derive the volume without double/triple/etc. counting where two shapes intersect and represent the shapes in a form that enables algebraic, rather than analytic, integration. 

This page is kind of a post but also kind of a working series of notes as I try to solve this problem.

## Generalized Stokes Theorem
The [generalized stokes theorem](https://en.wikipedia.org/wiki/Generalized_Stokes_theorem) relates integrating over some manifold with integrating over the boundary of that manifold. In it's most aggressively general form, it looks like the following:

$$\int_{M}{d\omega} = \oint\_{\partial M}{\omega} $$;

One important aspect of this is that $\partial M$; has no boundary $\partial \(\partial M\)$; (or rather, that boundary is the empty set).

The version most relevant to our purposes is the n-dimensional (generalised) version of the [divergence theorem](https://en.wikipedia.org/wiki/Divergence_theorem#Multiple_dimensions) 

## Note
We have switched to a simpler sampling method.... I don't like it, but it is what it is (see [here](https://en.wikipedia.org/wiki/Rejection_sampling)) 
