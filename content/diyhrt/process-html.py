#!/usr/bin/env python3

import html, fileinput, sys
from html.parser import HTMLParser

frontmatter_tags = "[\"trans\", \"hrt\", \"diyhrt\"]"

class RawDataDump(HTMLParser):
    """
    Takes raw data from contents of html and outputs it.

    This will remove things like <head> and <body> and attempt to take the inner tag 
    contents meow.

    It also removes classes from elements.
    """
    def __init__(self):
        # Int to prevent multi-level bodys from being an attack.
        self.bodydepth = 0
        # only output if = 0 and .inbody > 0 
        # This allows for non-conflicting output stuff
        self.allowingdepth = 0
        self.final_data = []

        self.denied_tags = {"script", "iframe", "style", "object", "audio", "video"}
        self.denied_attrs = {"class"}
    
        return super().__init__()

    def output(self, raw_txt):
        self.final_data.append(raw_txt)

    def preprocess_attrval(self, attr, val):
        if (attr == "href" or attr == "src") and val.startswith("images"):
            return "raw/" + val
        else:
            return val

    def process_attrs(self, attrs):
        """
        Strip out classes and delimit.
        """
        return [
            (attr, html.escape(self.preprocess_attrval(attr, val), quote=True)) 
            for (attr, val) in attrs 
            if attr not in self.denied_attrs and self.valid_attr_val(attr, val)
        ]

    def valid_attr_val(self, attr, val):
        if val.startswith("javascript:"):
            return False
        return True

    ### Actual tag handling - this accounts for e.g. if the tag is a short tag and how to output properly ^.^ nya
    def condshort_handle_starttag(self, tag, attrs, is_short):
        if tag == "body":
            if not is_short:
                self.bodydepth += 1
            return

        # Don't include bad tags
        # This will also 
        if tag in self.denied_tags:
            # Contained values
            if not is_short:
                self.allowingdepth += 1
            return

        tagstring = " ".join([tag] + ["{}=\'{}\'".format(attr, delimval) for (attr, delimval) in self.process_attrs(attrs)])

        if is_short:
            tagstring = "<{}/>".format(tagstring)
        else:
            tagstring = "<{}>".format(tagstring)

        if self.should_output():
            self.output(tagstring)

    def should_output(self):
        return self.bodydepth > 0 and self.allowingdepth == 0


    def handle_starttag(self, tag, attrs):        
        return self.condshort_handle_starttag(tag, attrs, False)

    def handle_startendtag(self, tag, attrs):
        return self.condshort_handle_starttag(tag, attrs, True)

    def handle_endtag(self, tag):
        if tag == "body":
            self.bodydepth -= 1
            return

        # Don't include JS and stuff meow
        if tag in self.denied_tags:
            self.allowingdepth -=1
            return
        
        if self.should_output():
            self.output("</{}>".format(tag))

    def handle_data(self, data):
        """
        Simple raw content - escape first then dump if necessary ^.^ nya
        """
        if self.should_output():
            self.output(html.escape(data))

def main():
    # Parser for allll the files nyaaaaa
    title = sys.argv[1]
    
    # Frontmatter:
    fm = """---
title: "{}"
tags: {}
media_wb: true
---\n""".format(title, frontmatter_tags)

    parse = RawDataDump()
    with fileinput.input(files=sys.argv[2:], encoding="utf8") as f: 
        for line in f:
            parse.feed(line)

    parse.close()
    print(fm)
    print("".join(parse.final_data).strip()) 

if __name__ == "__main__":
    main()

