#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
#IFS works much better with bash arrays nya

# NOTE: IPNS simply does not work... so don't bother with it nya
set -euo pipefail
IFS=$'\n\t'

source build.sh

# Recursively sign all of the documents with GPG key before uploading nya
function recursive_sign {
    log "Using GPG key ID $SAPIENTS_SITE_GPG_KEY_ID. Key information..."
    gpg --list-keys $SAPIENTS_SITE_GPG_KEY_ID
    log "Bulk signing output files with find and gpg --detach-sig <path>"
    find public -type f -eprintf "Signing file at %p." -execdir gpg --detach-sign {} \;
    log "Done signing output files."
}


function deploy_neocities {
    log "Building and Deploying to Neocities"
    # log "This uses the neocities Go client @ https://github.com/peterhellberg/neocities (aur package neocities)"
    # log "WARNING - NOT PUSHING TO NEOCITIES UNTIL neocities-cli FIXES THEIR SSL STUFF"
    # Build
    SAPIENTS_SITE_BASEURL="https://sapient-cogbag.neocities.org"
    generate_hugo_html
    # 
    # Publish to neocities.org
    log 'Attempting publish to neocities with `rustcities push --prune public/`'
    rustcities push --prune -m 2 public/
    log "Done publishing to neocities."
}



function clean_netlify {
    log "unlinking directory"
    netlify unlink
    log "Netlify done."
}


function deploy_netlify {
    log "Building and Deploying to Netlify"
    # Build site
    SAPIENTS_SITE_BASEURL="https://sapients-site.netlify.app"
    generate_hugo_html
    # publish to netlify.app nyaaaa
    log "Read site ID from \$SAPIENTS_SITE_NETLIFY_SITE_ID as $SAPIENTS_SITE_NETLIFY_SITE_ID"
    trap clean_netlify EXIT
    log 'Attempting to link to the site in local directory with `netlify link --id=$SAPIENTS_SITE_NETLIFY_SITE_ID`'
    netlify link --id="$SAPIENTS_SITE_NETLIFY_SITE_ID"
    log "Done."
    log 'Attempting to deploy to unlocked production, with netlify deploy --dir="./public" --prod'
    netlify deploy --prod --dir="./public"
    log "Done."
    log 'Attempting to unlink directory'
    clean_netlify
    trap do_nothing EXIT 
    log 'Done.'
}




function deploy_ipfs {
    log "Building and Deploying to IPFS"
    # Note - we use a baseurl of /
    # IPFS won't permit updates by it's nature anyway so broken rss is fine nya
    SAPIENTS_SITE_BASEURL="/"
    generate_hugo_html
    # Publish to IPFS. Note:
    #  -r means to upload the whole directory
    #  -Q means "more quiet" and causes only the final directory link to get emitted.
    log 'Adding to IPFS with `ipfs add -r -Q ./public`'
    rawhashcid=$(ipfs add -r -Q ./public)
    eprintf 'Raw QmHash CID variant: '
    log $rawhashcid
    # Note, printf is part of a command here nya
    b36cid=$(printf $rawhashcid | ipfs cid format -v 1 -b base36)
    eprintf 'Directory CID: '
    log $b36cid

    log "IPNS Does Not Work Well - Therefore you will be provided with an IPFS link."

    #   eprintf 'Updating IPNS with ipfs name publish --key=sapients-site --ttl 60s  --lifetime "8760h" [1 year] -Q /ipfs/'
    #   log $rawhashcid
    #   rawhashname=$(ipfs name publish --key=sapients-site --ipns-base base36 --ttl 60s --lifetime '8760h' -Q /ipfs/$rawhashcid)
    #   eprintf 'IPNS name hash/CID: '
    #   log $rawhashname

    eprintf "Perma-URL: "
    eprintf $b36cid
    log '.ipfs.dweb.link'

    eprintf "Perma-URL (does not preserve domain-specifity and stuff): "
    eprintf "ipfs.io/ipfs/"
    log $b36cid
    log "Done pushing to IPFS."
}

deploy_neocities
deploy_netlify
deploy_ipfs
