+++
title = "Blazing Future-Paths"
date = "2022-06-21T14:07:37+01:00"
draft = true
finalised = true
tags = ["series", "futurism", "future", "politics"]
[cascade]
tags = ["futurism", "future", "politics"]
series = ["blazing-future-paths"]
+++
Exploring, advocating for, and trying to predict various paths and routes humanity may take towards the myriad possible futures that await us, and how we can create those we desire on an individual and collective scale, in both the current time and in the future as it comes.

This is a series that looks forward in a speculative manner, as well as attempting to discuss and explore both individual and collective actions that may enable the creation of a desirable future rather than one that could be considered more dystopian. This isn't necessarily a singular monolithic vision of the future - though naturally, there are things in the future I will advocate for strongly, being someone with anarchotranshumanist and urbanist tendencies, but this is a slightly more... transmodernist contemplation of possible future-paths.

This series will vary between scenarios and ideas with a more concrete tone close to the present, and more speculative concepts that may be possible further afield. One thing I, and this series, will advocate for, is ensuring that as many people as possible make it to the far-future - via life-extension technology and a globally accessible longevity-escape-velocity.

If you are interested in stuff specifically about climate adaptation and climate change in more rigorous detail, take a look at the {{< tref "../climate-adaptation" >}}climate adaptation series{{< /tref >}}. We will cover some stuff around climate change, environmental action, and similar such things, but that series has very deep looks at specific technology. This series will have that but not so focused.
