---
title: "Trans Rights Are Not Just a Distraction"
date: 2022-12-02T09:55:19+00:00
draft: false
finalised: true
tags: ["politics", "trans", "cw-transphobia"]
---
[^locdisclaimer]:  (and many other places, but I'm most personally familiar with those two)
[^labour-transphobia]: https://www.thepinknews.com/2021/09/24/labour-party-transphobia-keir-starmer-rosie-duffield/

In the UK and the US[^locdisclaimer] there has been a sharp spike in transphobic hate by elected officials. In the UK in particular there is a lot of it even from the supposedly "left-wing" party, Labour[^labour-transphobia] (though nowadays it's milquetoast at best), and even in the mainstream "left-wing" paper, the [UK Guardian (as criticised by the US branch of the same paper)](https://www.theguardian.com/commentisfree/2018/nov/02/guardian-editorial-response-transgender-rights-uk).

[^trans-man-not-man-statement]: https://www.thepinknews.com/2022/10/03/tory-mp-mark-jenkinson-trans-man-transphobia-rant/
[^foi-as-weapon]: https://aninjusticemag.com/transphobias-secret-attack-on-stonewall-uk-cb57e2704648?gi=d0b950edd9be

[^concrete-policy-implications-transcare]: https://www.independent.co.uk/news/uk/penny-mordaunt-liz-truss-kemi-badenoch-conservative-party-tom-tugendhat-b2124439.html 
[^cpi-transcare-nhs-teens]: https://transwrites.world/nhs-trans-kids/

[^secondary-source-CPI]: https://www.opendemocracy.net/en/5050/tory-leadership-contest-transphobia-penny-mordaunt-kemi-badenoch-rishi-sunak/

[concrete-policy-implications-worker-equality]: https://www.thepinknews.com/2022/07/14/kemi-badenoch-financial-conduct-authority-trans-inclusive-workplace/

[bathroom-fear]: https://www.independent.co.uk/news/uk/politics/kemi-badenoch-transgender-transphobia-equalities-b1922103.html

[^bbc-transphobic-letter-as-report]: [bathroom fearmongering](https://www.bbc.co.uk/news/uk-scotland-63730621?at_link_origin=BBCScotlandNews&at_ptr_name=twitter&at_format=link&at_bbc_team=editorial&at_medium=social&at_link_id=08491A42-6B43-11ED-8854-A1280EDC252D&at_link_type=web_link&at_campaign_type=owned&at_campaign=Social_Flow) and https://transwrites.world/bbc-trans-report/

[transphobic-media-watch]: https://transwrites.world/category/media-watch/

There is abundant and ever-escalating evidence[^trans-man-not-man-statement][^foi-as-weapon][^bbc-transphobic-letter-as-report] for this fact, with concrete policy implications[^secondary-source-CPI] in the form of transmedicalism[^concrete-policy-implications-transcare][^cpi-transcare-nhs-teens], pushing for [equality measurements to exclude trans people][concrete-policy-implications-worker-equality], the standard [bathroom fearmongering][bathroom-fear], transphobic propaganda [across the media as a whole][transphobic-media-watch] (even on public broadcasters like the BBC), and frankly too many more things to list here.

These issues are not what this post is about, though. They're very important, of course, and there will be more discussion of them in future as well as the many, *many* things going on the USA and other countries. But this article is about a common leftist response to mass-scale political transphobia as has become popular in the US (as it descends towards Christofascism) and the UK.

### "Attacks on Trans Rights are Just a Distraction"
This is a very common response towards continued transphobia in various countries. Usually it's something along the lines of "transphobia is a distraction from the cost of living crisis/unemployment/capitalism/corporations/etc."

To a degree, I understand their point. The right wing do attack trans people in part as a means of avoiding confronting issues around capitalism or the current model of work, and other such problems, if they dont actively embrace those issues as being positives. The problem is, that doesn't mean the hate is just some kind of front, an issue that need not be addressed and countered. Conservatives and fascists actively implement trans-hostile policies and make our lives materially more difficult and unpleasant.

Their hate emboldens public harassment and abuse of trans people, weakens support for trans people in The General Public™, and pushes forward the destruction of laws that serve to protect our basic rights. To me at least, this attitude of transphobia being a distraction only enables allies and even trans people ourselves to just ignore the issue, and comes across as extremely dismissive to our struggles and rights. 

Maybe if you aren't trans, it might seem like a distraction. But for us, it most certainly isn't.

### It's Not Just About Trans Rights
Of course, so far, I've only discussed trans people here. However, this same "the hate is just a distraction" attitude can be seen in many other discussions of minority rights. Certainly, how often have we all seen people talking about the American GOP's attacks on abortion as being a distraction from the economic problems people face? Or, what about other forms of queerphobia, homophobia, and similar. 

I think from a racial discrimination perspective - speaking here as someone who is white, so take it with a grain of salt - there is slightly less of it because of how economic discrimination has been used more directly as a weapon towards those groups (due the systemic impoverishment created by openly racist laws and people in the past such that economic discrimination measures alone can now act to perpetuate poverty in minority groups, in an often-intentional manner).

The point is, this is a common attitude towards minority rights - one of refusing to acknowledge the reality of hatred and bigotry beyond it's use as a "distraction". And, well, with the the continued advance of hatred and bigotry in the US and UK, and many other places, it ain't fucken working. Roe v. Wade has been struck down in the USA, and transphobic laws are spreading across that country. In the UK, well, I've already just scratched the surface at the start of this article, but the point is that it's a clusterfuck, and we aren't even starting on other issues beyond the barebones. 

Other countries can provide a glimpse of leaving this sort of hatred unchallenged in places where it is at a lesser stage of political success  - for example, Poland, with it's "LGBT+-free" zones. 

I can understand the dismissiveness of minority issues to a degree, as a reaction to the underfocus on economic issues by supposedly "leftist" parties in many countries due to the dominance of neoliberal thought in their political landscapes, but the fact remains that minority rights are extremely important and many of the parties criticised for not tackling economic issues while caring about minority issues, often only engage in extremely superficial attempts towards minority rights anyway, since they are also fundamentally intertwined with economic issues as induced by discrimination while still having their own life of bigotry independent of such. 

### A Milder Form of Class Reductionism 
Class reductionism is (roughly) the idea that the only axis of subjugation is via that of economic class, and that all other sources would go away when capitalism is abolished. Roughly and oversimplified, that's the concept behind it. 

I would make the case that this rhetorical strategy of focusing on discrimination as solely a means of distraction from economic issues is to some degree a form of class reductionism, except class reductionism is usually associated more strongly with Marxists and people following ideological offbranches of Marxism (at least in my experience, though I've seen it a lot from Democratic Socialists as well), whereas this sort of "hate is just a distraction" rhetoric is something I've seen from even the most milquetoast centrist American liberals who think capitalism is the greatest thing since sliced bread. 

As such, I'm terming this rhetorical strategy as {{< special >}}"Economic Reductionism"{{< /special >}}, if someone doesn't already have a better term of course. 

This is, then, the idea that bigotry solely exists as a means to distract people from economic issues as opposed to a problem in-and-of-itself, as a superset of the concept of class reductionism that can apply beyond the explicit lens of various forms of socialism that exclusively focus on issues of economic class, expanding towards anyone who dismisses bigotry, discrimination, and hatred as mere distractions from other economic issues, including people who support capitalism-with-a-friendly-face-plastered-on-top.

### In Conclusion...
The cruelty is not just a distraction, it's the point, even if some politicians happen to also use it as a distraction. 

I think it's time that the people decrying bigoted attacks on human rights as a mere distraction recognise that the hatred is often an end in-and-of-itself, such that just trying to redirect bigots towards "economic issues" will not, actually, solve the problem in the mid- and long- term, though it could be a viable strategy when dealing with people one-on-one if you can't jolt them out of their bigotry, but they aren't as invested in that as much as they are invested in economic problems, should you be so lucky.
