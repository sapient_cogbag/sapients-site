#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
#IFS works much better with bash arrays nya

# NOTE: IPNS simply does not work... so don't bother with it nya
set -euo pipefail
IFS=$'\n\t'

source build-scripts/little-utils.sh

# This script directly serves the files in ./public on port 1314
# Used for when some live `hugo serve` thing is being janky to get a good preview despite
# that nya.
check_for_hugo_root

log "Serving ./public on localhost:1314"
cd ./public
python -m http.server 1314
