+++
title = "Cooking Capability: Boiling Water"
default-hide-tag.cooking-capability-boiling-water= false
+++

Pages in Adaptable Cooking listed here are primarily relevant if you have the ability to get water to 100°C (boiling point) at least temporarily.

In practise this means that you have access to a kettle and can use it, but it is also possible to boil water with a microwave.
