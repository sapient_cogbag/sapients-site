---
title: "01 - Vertical Farming & Alt-Agriculture"
date: 2022-06-19T15:05:04Z
---
One of the major threats posed by climate change is that of agricultural supply chain collapse due to radical shifts in environmental conditions both seasonal and long-term. 

The threat is an extremely serious one - the [IPCC][ipcc-site] has projections of food supply effects, and this compounds with unsustainable agricultural practices endangering the agricultural viability of soil (similar to how the dust bowl period occured in the US in part due to poor crop cycling policies) - the long and short of it is that climate change and poor agriculture practices threaten the food security of billions of people - either through failed crop yields due to long-term environmental damage, increased chances of freak crop-killing effects, and spread of pests (and indeed, the evolution of pesticide resistence as well) - or the resulting increase in food prices.

This threat is most acute to those who have less resources or who depend on local harvest rather than global importation of food, but it applies to everyone. It not only threatens staple crops, but also less essential crops like chocolate, coffee, and spices, though the staple crop issue is probably most important for most people.

Furthermore, current agricultural techniques are dependent on nitrogen fertiliser - a wonderful invention, but one that currently is mostly produced using natural gas as a source of hydrogen for the [haber process][haber-process-wikipedia] used to produce ammonia - this is less of an obstacle, environmentally speaking, because hydrogen can also be fairly simply extracted from water, though at the moment it takes a fair amount of electricity. It is also dependent on environmentally damaging pesticides and herbicides that can leak into rivers and worse, the water table - and is extremely water inefficient due to the widespread use of mass irrigation (for rice in particular, much is still produced using paddies which are essentially giant puddles full of fresh water, which is even worse from a water usage perspective).

In a world where climate change not only threatens crop viability via chronic and acute temperature increases (and increased instability in temperature, too), but also via large-scale redistribution of freshwater availability (and also salt contamination of soil due to sea level rises and flooding), current agricultural techniques are extremely vulnerable to threat both via reduced overall yield or complete crop failure, and via more wild *variation* in yield resulting in unpredictability of food pricing.

This doesn't even start on the issue of the danger involved in people labouring in higher temperature conditions especially in heatwaves, with increased risk of heatstroke.

#### What is Vertical Farming?
Vertical farming, sometimes known as indoor farming (though that term is more general), refers to various alternate methods of agriculture primarily occurring indoors in more controlled environments, and often in urban areas as well. It can be on a large industrial scale - the most suitable for feeding the world - but also includes more personal indoor farming (nowadays it is possible to get 3D printable kits that produce very automated small-scale farming setups). 

Having a fully enclosed and controlled environment enables many things not possible in traditional agriculture, and provides an immense resilience to hostile external environmental conditions whether they be due to permanent shifts or freak events like heatwaves.

#### Relevant Technologies
There are many important technologies involved in vertical farming on both a small and large scale, that enable extreme efficiency gains and automation.

##### Photosynthesis-Optimized LEDs & Energy Reclaimation
A major part of the ability to perform vertical farming with a degree of energy efficiency comes from the use of Light-Emitting Diodes which produce light wavelengths optimised for absorption by plants, as well as the use of energy reclamation technology for light that the plants do not absorb - plants do not absorb equally on all wavelengths, and a lot of the electromagnetic spectrum from the sun goes to waste.[^improving-photosynthetic-efficiency].

For example, here is a graph[^improving-photosynthetic-efficiency] of the light actually absorbed by plants at various wavelengths in the visible and near-infrared spectrum: {{< figure alt="graph showing the absorption rate of light for various photosynthetic processes - most plants have peaks at 400 nanometres and 700 nanometres" src="images/wavelength-efficiency-graph.jpg" caption="Absorption of light at wavelengths for different photosynthetic processes - BChl b is purple bacteria, the others are plant photosynthetic processes." >}}

Using LEDs that produce light on these wavelengths, as well as using bright white or completely reflective walls, allows recycling the light that does not get absorbed, bouncing it back to the plants over and over. In theory, it would also be possible to generate energy from the waste heat produced inside a vertical farm in the case of really large setups, but I don't think that's been done in practise.

Of course, traditional agriculture has some advantage here in theory because it uses the sun directly which people often consider to be "zero cost/free" energy, even if it is less photosynthetically efficient, though the increased photosynthetic efficiency of targeted wavelengths, plus energy recycling via numerous methods such as light-reuse and use of waste heat to fuel more energy generation, and the use of solar power with high efficiency for initial input (either [photovoltaic cells][pv-cells] or [thermal solar][thermal-solar]) may result in more photosynthetic ability from the same land area than traditional agriculture.

##### Water Recycling & Efficiency
One of the major benefits of vertical farming in all setups for climate change resilience is the use of *water recycling* - not just recycling water used to irrigate the plants rather than dumping it into the ocean, but also recycling water that evaporates from the leaves of plants as they grow (this is part of the mechanism by which they pull nutrients from soil, in a process known as transpiration), as a result of growing in a closed environment.

Essentially, this means that theoretically speaking, a vertical farm can convert all water input into plant biomass. Obviously, a vertical farm cannot be 100% efficient, but in practise it can get pretty close, with companies claiming efficiency rates of around 95-99% less than traditional agriculture and various sources backing them up[^soilless-future]. From a climate perspective, this is absolutely vital, because the majority of fresh water usage today comes primarily from agriculture, and is used in a completely unsustainable fashion.

In particular, aeroponics looks to be the best technology both in terms of efficiency and simplicity of water reuse[^soilless-future] - better than hydroponics (nutrient-enriched water with rocks or other similar substrate) and aquaponics (pool of nutrient-enriched water), or even plain farming indoors with soil, though all of these are much better than traditional farming in terms of water efficiency (and some crops, like potatoes, do not mix well with aeroponics).

{{< aside aside block "Aeroponics" >}}
Aeroponics is a farming technique where you spray water with added nutrient solution onto plants with their roots in the air.

It is extremely efficient both in terms of water - you can recollect all excess water straight from the bottom and just respray it - and it makes reusing light very easy because there is no substrate (soil, water, rocks, etc.) to block the light, so as long as the surfaces are white (or mirrors) it should ensure very high absorption by plants. 

Some plants - in particular root vegetables like potatoes - currently don't take too well to aeroponic farming, though of course people are working on developing ways to fix that.
{{</ aside >}}

##### Climate Control (Air Conditioning & Heatpumps)
These two technologies are vital to the advancement of vertical farms as it allows tuning the internal environment to match that required by the crops being grown, and it is also one of the primary reasons that vertical farming provides extreme resilience to climate change and extreme weather events (as well as to global shifts in the viability of land for agriculture).

#### Benefits of Vertical Farming and Broader Indoor Farming Techniques
Vertical farming has many potential benefits especially in relation to climate change but also generally, some of which apply to more broad notions of indoor farming and climate controlled farming, and all of which are major reasons to support a technological and sociological shift towards indoor farming in general and vertical farming specifically.

##### Immunity to Extreme Weather Events
From a climate change adaptation perspective, indoor farming (of all kinds) is beyond vital, as the increase in crop-killing events like heat waves, floods, storms, freak cold snaps, and in the extreme case the salting of land via sea floods - as well as the long term issues like water supply and agriculturally-viable land area - poses an extreme risk to global food supply everywhere.

Indoor farming allows controlling the conditions inside the farm via things like air conditioning and heat pumps, and allows completely offsetting the risk of catastrophic crop-destroying events, as well as enabling continued agriculture and livability in the vast swathes of land that would otherwise become desertified or unable to sustain agriculture in the long term due to radical shifts in global environmental conditions.

This, to me, is the biggest and most important reason I push for indoor and vertical farming to such an extent, as an essential technology for future survival of humans in large areas of the planet as the climate shifts significantly in the near future.

##### Better Resilience to Pest Migration
One of the less-discussed impacts of climate change is the rapid spread of certain nasty pests that threaten agricultural security (amongst other things - there are pests in Canada that are posing a significant threat to its forests as they are no longer dying over the Winter months).

Naturally, vertical and indoor agriculture is almost entirely resistant to this problem.

##### Water Efficiency
As mentioned before, all forms of indoor farming with a water recycling system provide extreme benefits for the efficient and sustainable usage of fresh water supplies the world over.

##### Space & Land-Use Efficiency
Vertical farming in particular provides extreme land usage efficiency gains over all other forms of farming, because you can stack farms in as many floors as you can provide as high as you can safely build buildings. Even disregarding all other factors, this provides extreme yield gains of 5-20x per square metre, and combined with full automation, this could allow far greater increases still, as explained [below](#extreme-densification-with-total-automation).

Environment-wise, this means that vertical farming drastically increases the maximum population that can be fed with a given land usage, and allows us to free up more land as nature reserves, as well as providing a bigger buffer for instability in supplies and consumption of food, and allows for radical urbanisation and localisation of food production.

##### Closed-System-Induced Reduction in Environmental Contamination
Any indoor farming setups - but especially well-managed vertical farming installations - have a large benefit in that they are closed systems (in terms of water and substrate, at least).

This means that any fertilizer used is very unlikely to leak or need to be disposed of if the water is continuously reused - in traditional agriculture this is a hugely damaging issue where leaked fertilizer causes massive blooms of algae. These blooms both block out light from other aqueous plants (killing them), and when they die the microorganisms that decompose them consume all the oxygen in the waterbody, killing most animal life.

Furthermore, vertical and general indoor farming with controlled environments just straight-up requires no herbicides and pesticides, especially with more radically futuristic growing methods like aeroponics, where there is no soil at risk of contamination. This avoids all the problems involved with pesticides and herbicides leaking into the environment (yes, that includes "organic" pesticides and herbicides), such as general environmental damage and the evolution of herbicide and pesticide resistance in insects and weeds. It also reduces the added costs (monetary, environmental, and social) involved in production of these pesticides and herbicides.

##### Full Urban-Local Autonomous Food Production
Some people might not see this one as a benefit, but as someone in favour of urban environments for a number of reasons (social, environmental, and efficiency-related), indoor and vertical farming allows for fully local production of food in cities in a reliable manner, with vastly reduced transport costs and environmental damage.

The controlled environment of indoor farming also allows for growing food that by traditional agricultural methods must be imported from far away at pretty high environmental costs - this has the added benefit of a more verifiable supply chain for food (e.g. ensuring slavery is not used in its production, which is a big issue in the production of things like chocolate and spices).

##### Growth & Yield Optimisation
Because the environmental conditions, nutrient mixes, and similar, are so tightly managed in vertical farms just by the nature of their construction (especially with more futuristic techniques like aeroponics or aquaponics), it allows precise optimisation of the environment for maximum yields of any crops that are grown.

##### Automation
As we can control the built environment inside vertical farms, we can design them to be fully automated by making the environment simple for navigation by robots in any number of ways - for example, there is no need to add complex AI to robots to avoid interacting with weeds, or to navigate lumps and bumps in the ground, like is necessary for trying to fully automate traditional agriculture.

There are still a fair few challenges involved in this automation, but there is progress being made all of the time.

###### Extreme Densification with Total Automation
The total automation of growing plants - though total automation is not entirely necessary in some cases - allows for massive amplification of the [stacking effect](#space--land-use-efficiency) even beyond what normal vertical farming allows. This is because when the plants no longer need to even be accessible by humans, you can pack them to the maximum possible density - think 5+ layers of plants growing in each human-sized floor. In the case of taller plants, you can interleave the thin stems of plants with the bulbous grain or otherwise nutritious part of those a layer below to acheive the same densification effect.

This densification in many cases adds an additional extreme multiplier to the possible production of crops per square metre - dependent on the crop, we can guesstimate 2x-5x multipliers, though that is of course difficult to truly estimate without very specific details.

It should actually be possible to gain this benefit even without total plant-lifecycle automation in many cases with less complex designs, if the plants can be placed in movable rows and columns that can be slid in and out of a shelf area for human access, so in practice we can add this 2x-5x multiplier in all cases of estimations for yield/m². At this level of densification, though, you presumably have to start seriously thinking about heat dissipation.

##### All-Season Growing
One of the major limitations on the productivity of traditional agriculture is the seasonality of environmental conditions. That is, many crops can only be grown in one part of the year, once per year, because the environment changes too much from summer to winter and back again. Furthermore, as climate change progresses, the extremes within each season become worse, rendering the chance of crop failure much higher in every season.

Because of the control of the environment afforded to us by indoor and especially vertical farming, seasonal crops can be grown year-round in all hemispheres and latitudes. Depending on the crop, this can result in 2x-4x increases in yield per metre-squared, regardless of all other factors. This has major benefits beyond that of mere yield, however, as it means that *all food can be produced locally at all times*, rather than relying on global shipping in the off-months where something can't be grown in the local hemisphere, which has very large environmental costs.

This lack of seasonality is essentially a byproduct of the fact that vertical farms are resilient to extreme weather and changes in the long term viability of agriculture in various regions of the world, but comes with its own environmental and general productivity benefits too.

#### Challenges to Vertical and Indoor Farming
Vertical farming - and wider indoor farming - is not free of challenges to adoption, even despite extreme benefits to sustainability. I go over them here with some discussion and hope for solutions.

##### Initial Land/Building Cost
For urban vertical farming, a major challenge is the cost of highrises, land, and buildings in general. This is arguably one of the more difficult problems to solve, and yet also simple. The long term solution here is to destroy capitalism and commodified land, and also develop more efficient building techniques, but even assuming that doesn't happen, there are other solutions.

###### Shipping-Container Farms and Farms in Abandoned Buildings
Significant progress on a local and community level has been made by using stacked shipping containers and abandoned buildings to create vertical farms almost anywhere. Shipping containers have the benefit of being designed to stack with each other as well and enable pretty extreme modularity of vertical farms.

After all, the most important aspect of indoor farming for climate resilience is that it is an enclosed space with access to electricity to manage its internal environment - even a basic enclosure like a shipping container is enough to enable this.

Arguably, then, "shipping containers" have already hit the "efficient building technique" criteria to overcome this issue, though modular buildings more designed for vertical farming specifically may be extremely useful. 

###### Greenhouse Farming
The economic barrier of initial building and land costs to the adoption of vertical farming can also be mitigated in places where open agricultural land is cheap but urbanised land is extremely expensive, by using something called greenhouse farming (the name makes it clear what is being done here), which has a less controlled (but still pretty controlled) internal environment that uses the sun for lighting directly. 

It loses all of the extreme space efficiency gains derived from the ability to stack vertical farming and a lot of the automatability, but still has significant efficiency gains over traditional agriculture as it can farm crops in all seasons and provide resilience to external climate change (and can be used with minimal or no pesticides and herbicides depending on how enclosed the greenhouse actually is).

Greenhouse farming is less beneficial for resisting heatwaves (a greenhouse is designed to trap heat after all), but if the heatwaves are short it is still doable by temporarily covering the greenhouse up and using air conditioning. 

For conducting agriculture in environments which have become inhospitable to it in the long term due to climate change, however, it is a much less suitable technique than true indoor farming, and it also does not enable the same "universally-local agriculture" for crops that require cooler environments (it works for crops that need more tropical or otherwise warmer environments, though). 

Water-usage wise, greenhouse farming is much more leaky than full vertical farms, though again, still much better than traditional agriculture because the water can still be recaptured fairly easily.

##### Energy Usage and Costs
This is the other big challenge to vertical farming - how much energy it actually uses. In particular, vertical farming, unlike other agriculture methods, does not get its energy directly from the sun. Instead, for any amount of stacking, wavelength-optimised LEDs are typically used instead.

Long term, this is beneficial, as it enables extreme densification of agriculture via stacking layers of plants far beyond what the sun could support in the same area, and renders the growth of plants independent of solar output at the location, and that can use extremely dense energy sources like nuclear fusion, fission, wind, hydropower, or even space solar collector installations. However, right now, it means two things:
* When connected to the grid, it is necessary to pay for energy which also may be being produced by fossil fuels. This problem is common to anything requiring electricity and solving it must occur to reduce the impacts of climate change anyhow (either through proliferation of solar panels and other green energy sources, or nuclear fission and nuclear fusion, or both). Abundant energy is the ultimate solution to this issue, and at the very least reducing the scope of environmental impact to the environmental impact of electricity production makes solving the larger problem simpler.
* It is perceived as more expensive because it does not get "free" energy from the sun in the same way that traditional agriculture does.

###### Shifts in Cost
One of the core issues in vertical farming is energy cost.

Essentially, traditional agriculture does not have to pay for energy it extracts from the sun, because it comes "free" with the land they buy to use.

Vertical farming installations and the people running them *do* have to pay for energy they gain from the sun (or other places), over the grid, in most cases, even if on an output-per-metre-squared-of-sunlit-land they are more efficient due to the efficiency of combined [photovoltaic solar][pv-cells] and [thermal solar][thermal-solar], and efficiency gained by used of LEDs tuned to output photosynthetically-efficient wavelengths of light.

The only way this efficiency can truly be realised from a cost perspective is:
* Push power companies to develop more, and more efficient, solar generation facilities (or other more energy-dense renewables like fusion, wind, geothermal, and hydropower), to the point that they have to pay people to use excess energy (or at least, don't charge people for it). Under capitalism, this is directly counterposed by corporate need for profit, even though it would be beneficial to collectively build enough power generation that power becomes absurdly abundant.
* Bundle high-efficiency solar ([photovoltaic][pv-cells] + [thermal][thermal-solar] in combination) with vertical farms, to sell power to the grid and buy at night, or even bundle storage with vertical farms directly to reduce the interaction with the grid and associated costs.

Both of these are pretty desirable, though the first is ideal as it helps to counterbalance shifts in local weather. The second is good for local resilience, and a combination would be excellent.

###### Community Desire for Food Autonomy
Even with some extra energy costs, there is still a push for vertical farming because of the increased resilience to climate catastrophies, the reduced need for (and cost of) transport, and the increased autonomy it gives a society and city, as well as the increased autonomy for people within them too, and providing uses for otherwise uninhabited or abandoned buildings. I suspect that this will be a major way vertical and indoor farming moves forward, in spite of increased energy costs. This is less of a solution and more of a "we will do it even in spite of the problem", but still.

###### Greenhouse Farming
Greenhouse farming provides a useful intermediary that can, to a degree, counteract the issue with "free" solar energy being used by traditional agriculture when fully indoor farms have to separately pay for solar energy - because greenhouse farms typically have the same advantage traditional agriculture does on this front, which means it may be easier to advocate for in the short term.

However, again, it has a very hard cap on the densification of agriculture and is much less resilient to climate extremes and cannot be used to grow plants more comfortable in "colder" areas (which is especially relevant given the increase in temperature and heatwaves in a lot of areas).

##### Issues Growing Certain Crops
Certain crops - in particular, root vegetables, but also some others that require material environmental triggers for particular types of growth or that are more long-term crops like fruit trees - are not as easy to grow with the more automated and dense methods like aeroponics that do not use traditional substrate. As far as staple crops go, potatoes are one of the more difficult ones.

It's not so easy to provide a more general solution here because it's a bit more of a per-crop issue, but people are developing solutions for this. In many cases, you can always fall back to less futuristic methods like hydroponics or even soil growing, and the stacking, optimisation, and automatability of vertical farming still allow absurd efficiency gains over all traditional agriculture.

Trees are harder still, but trees require human harvesting anyway most of the time and are kind of just generally *janky* when it comes to mass agriculture. They are much less friendly to automation, though growing them in a building with very tilted floors or something similar may be a possibility, but it is not necessarily as suitable for vertical farming techniques without more radical engineering and automation, or perhaps in combination with some kind of permaculture technique, or just plain old greenhouse farming - or all of these - for climate change resilience.

More extreme solutions for tree and bush products may be genetically modifying the trees, or perhaps manipulating the plant's hormones to artificially trigger growth phases (and make it act more like crops such as wheat and corn - i.e. annual or seasonal - and have the plant produce a single stalk with the relevant product rather than an actual tree). 


#### Vertical Farming and Deep Efficiency/Automation
So far, we have limited ourselves to discussing the farming of conventional crops - corn, wheat, rice, potatoes, and all other kinds of typical plants - with conventional notions of cyclical, discrete harvests and automation around that. We've discussed the challenges, the pretty extreme benefits, and the importance of indoor and vertical farming to climate resilience.

But we can go deeper, push the boundaries for resource, energy, and climate efficiency, explore far more radical possibilities in automation, and that's where we're going.

##### Efficiency
So far, we've got the following efficiency gains:
* All-Seasons Growing - 2x-4x crops per metre-squared multiplier
* Floor Stacking - 4x-20x crops per metre squared multiplier
* Densified Stacking - ~2x-5x crops per metre squared multiplier - see [here](#extreme-densification-with-total-automation) for details
* Some gain based on the ability to optimise things like light cycling, nutrient mixes, and based on the lack of total crop failure and pests. We'll guesstimate (because I don't have data on this), that the gain here is 1x (no gain) to 2x (doubling of output).

Totalling this up, we get a "crops per metre squared" gain of 16x - 800x (that 800 isn't a typo), which we can estimate conservatively to be an expansion of ~80x in area productivity, in an average case. Of course, this comes at increased energy cost, and is naturally somewhat of a guess (though one I think is fairly well founded), but the per-area density here is absolutely bonkers, and you can in fact keep just adding more floors if you have sufficient resources to do so. There are buildings today with hundreds of floors, so the theoretical limit is completely absurd.

But there is an aspect here that has not been touched on much. So far, we've been (almost, barring [this discussion of extreme densification](#extreme-densification-with-total-automation)) exclusively examining efficiency and productivity on a per-unit-area basis. But really, we don't *directly* care about that. We care about the *volume* of useful, nutritious grown lifeform produced that people can eat.

If we then consider that most traditional crops are *hard* to set up with 100% volume utilisation - though the discussed extreme densification helps - it is worth examining more extreme solutions that would let us use the entire volume of a building (other than lighting and climate control infrastructure) for productive agriculture.

###### Algae and Other Microorganism Farming
One of the most radical up and coming technologies is the use of algae to form a basis for food production. For those who don't know, algae are essentially single celled plants that typically live in ponds, the ocean, etcetera. They can grow in what amounts to a water bath, when provided with light, CO₂, and fertilizer.

In theory, then, it should be possible to grow algae - perhaps genetically engineered with different flavours, nutrient profiles, material properties for making bulk product, and other variables - as a base material for food production, using *100%* of the volume of a building or other enclosed area.

Think about the following idea - a tank, filled with water and added fertilizer, and with thin LED strips tuned optimally for photosynthesis arranged in a 3-dimensional cubic grid that is built of white material - or perhaps, a series of poles of similar white colouring used to hold the LEDs - filled with algae. This would provide for not only 100% volumetric efficiency, but also absurdly simple automation - just slide the big block of algae out when it is done. 

Of course it isn't quite that simple - you probably need to ensure the algae has access to CO₂, perhaps by using many small shelves rather than a large tank - but either way, you rapidly approach complete volume efficiency using algae production like this, and the harvesting and automation is really simple.

This approach not only enables extreme efficiency, but it also can double up as a *mushroom farm*, as the algae can just be reused as substrate for fungi. This is important because mushrooms and mycoprotein generally are excellent protein sources that do not come from animals. This carries other possibilities too because mycelium - the main body of the fungi - is being explored as a potential building material (when compressed).

###### Engineered Crops
Many of us would like to continue to eat grains, related crops, or food derived from them, but right now, grains are not that volumetrically efficient. [The discussion of extreme densification](#extreme-densification-with-total-automation) provides one way to help increase this - interleaving the stalks of one layer with the productive produced crop of the layer below - but this does complicate both the design and automation of the growing areas.

Another way to solve this problem is to engineer the crops - either by genetic modification or selective breeding - to be shorter and perhaps to be more corn-like in shape, with photosynthetic leaves around the productive crop, in the extreme case with no real stalk at all. This would allow absurdly dense shelves full of crop, and much easier automation and design, and near-100% utilisation of the available volume with high-intensity lighting packed between layers of crops with their roots, the floor and supports coated in white paint to reuse as much light as possible.

##### Automation
So far, we've not really discussed the specifics of automation, and that's because it's not quite as relevant to climate resilience as many of the other aspects. But it is an important part of building a more radically free future, and making it work for everyone.

Now, we're going to examine some possibilities for fully automating farming in the context of vertical and indoor farms, and altering the way we think about how to do agriculture when we have full control of the environment all the plants grow in.

###### Crop Pipelining
Almost all agriculture today fundamentally runs on a discrete cycle - plant crop (or otherwise put the seed in some condition to grow), provide appropriate conditions for it to grow, then harvest a few months later.

In traditional agriculture, this is a requirement, because crops can only grow at one time of the year. But in indoor and especially larger scale vertical farming, this is actually entirely unnecessary, because you can keep different regions of the building at different environments to match different growth phases of the plant, automatically moving collections of shelves or some other grouping of plants through the different environmental conditions over a period of time as they grow.

This essentially allows a continuous stream of fresh crop to be produced by a vertical farm.

###### Automatic Artificial Selection
With sufficient AI observation of final crops, and some automated facilities, it may be possible to continuously refine the features of a crop by having an AI automatically take seeds from the best crops and feed them back into the collection of seeds - for instance, optimising for shorter and shorter crops to increase the densification ability as [mentioned above](#engineered-crops), or something similar.

This mixes very well with the [crop pipelining](#crop-pipelining) mentioned above, which would result in a vertical farm continuously spitting out better and better crops with minimal intervention. This idea does have a complexity cost, though, but it could work for automated seed production facilities to make seeds for other farms.

#### And Finally, The End
Hopefully this was a useful and interesting exploration of potential paths to a good future, with vertical farming, and a useful examination of both the benefits of vertical farming and current difficulties with adoption.

There are more than just the possibilities outlined here, and I'd encourage everyone to think about further potential refinements of vertical farming technology, as well as possibilities for it's usage.

[ipcc-site]: https://www.ipcc.ch/srccl/chapter/chapter-5/
[haber-process-wikipedia]: https://en.wikipedia.org/wiki/Haber_process

[^improving-photosynthetic-efficiency]: https://doi.org/10.1016/B978-0-12-809633-8.21537-9

[pv-cells]: https://en.wikipedia.org/wiki/Photovoltaics
[thermal-solar]: https://en.wikipedia.org/wiki/Solar_thermal_collector

[^soilless-future]: ISBN 978-3-319-74548-0 (chapter 10) - https://link.springer.com/chapter/10.1007/978-3-319-74549-7_10
