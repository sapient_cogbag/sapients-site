#!/usr/bin/env sh
# Source https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity
# Get a sha512 integrity of a file ^.^ nya

shasum -b -a 512 "$1" | awk '{ print $1 }' | xxd -r -p | base64
