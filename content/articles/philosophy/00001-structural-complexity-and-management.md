---
title: "Structural Complexity and Arguing In Associated Systems"
date: 2021-11-01T17:58:13Z
draft: true
---

So. You've come across some sort of problem or philosophical argument who's structure (or the structure of it's subjects) is unknown and likely complex, or perhaps you want to make an argument either with such structural complexity, or about something that is so structurally complex, while maintaining approximate formal correctness. Hell, perhaps you just want a coherent way to think about such ideas rather than floundering wildly without tools to handle arbitrary complexity! I don't know why you decided to read this, but I'm going to elaborate on how I tend to think about these things - namely, using the tools that mathematics has already developed for the purposes of wrangling and understanding arbitrary large and complex systems, as a way to think about more general concepts that don't necessarily have formal definitions but can be named and combined.



When I come across a system or argument (or anything, really) that can be analysed philosophically or sociologically or similar, I tend to employ a near universal metastrategy to manage them. The first step I often take is to try and see how a network can be formed from something and then place it on a spectrum of classification, though of course much more complex analysis comes later. 

One of the major reasons for doing this is it provides a good mathematical structure to otherwise extremely fluid concepts and can allow the use of mathematical language and ideas to pin down the descriptions and analysis of structures.

So, here is how I typically initially classify networks - note that this is a spectrum along several axes, but we can draw a line.

##### Low-Complexity/Simple Networks/Structures

Low complexity networks typically involve regular (if fractal) structure in a fixed size with a small number of nodes, and no general recursion (if we are talking about a structure with directed connections between nodes). In compsci, trees of a relatively small number of elements would be an example of this.

In terms of long-term change, the structure of the network typically doesn't change too much and connections are rarely broken in significant manners.

##### High-Complexity/Complex Networks/Structures

High complexity networks usually still involve regular structure and mostly single-direction connections (if those connections have some notion of direction), however they may contain some element of recursion. Their most important feature is an arbitrary (usually very high) number of nodes and connections that may rapidly change with respect to some variable (if the network is in a setting where it does change).

##### Hypercomplex Networks/Structures

Hypercomplex networks may have regular structure that is almost certainly fractal, but still contains many different substructures. It is also *necessarily* highly recursive and self-modifying (if in a setting where it can meaningfully modified). Examples of this are modern social networks, and in fact my term for these ("hypercomplexity") comes from the fact that the internet essentially creates these sorts of networks in the way communities and cultures interact and are constructed. 

##### Operations and Graphs

All of these structures in some way define a graph. This means we can perform certain operations on them that are graph-like. Often, it is desirable to for instance say that we can separate out some subset of the graph that is a simple network and some other set that is hypercomplex and quantify influence on the rate of change of the overall graph via treating the space of possible graphs as a linear space or similar and then using multipliers to work out net rate of change.

That though, is a topic for another article.
