---
title: "Graphs and Vector Spaces"
date: 2022-01-10T18:10:28Z
draft: true
---

For a while now, I've had a very particular interesting idea based on weighted graphs and finite and infinite vector spaces - namely, that it may be possible to work with graphs by representing them as vector spaces (and potentially, vice-versa).

I have a particular interest in non-finite vector spaces as a means of representation for a number of reasons (in fact one of my bigger projects is the use of them in weird philosophy shit). In thinking about this, however, I came to the conclusion that it is possible and potentially desirable to represent weighted graphs (finite, countable, and potentially uncountable in size) via the use of [vector spaces](https://en.wikipedia.org/wiki/Vector_space).

### Introduction - Fields & Vector Spaces
The first thing, for new people here, is the question "what is a vector space"? Which is not too hard to answer.

Before that - since it is involved in the definition of a vector space - we should look at the definition of a *Field*.

#### What is a field?
The answer to what a [field](https://en.wikipedia.org/wiki/Field_(mathematics)) is is that it is a set of objects $\mathbb{F}$; - such as the real or rational numbers ($\R$; and $\mathbb{Q}$;) - satisfying a series of axioms as follow with an associated pair of operations  $+, * : \mathbb{F} \times \mathbb{F} \to \mathbb{F}$; and identities $O, I \in \mathbb{F}, O \ne I$;. These axioms are as follows $\(\forall a, b, c \in \mathbb{F}\)$;...

###### Associativity
* $a + (b + c) = (a + b) + c$;
* $a * (b * c) = (a * b) * c$;

###### Distributivity
* $a * (b + c) = a * b + a * c$;

###### Commutivity
* $a + b = b + a$;
* $a * b = b * a$;

###### Identities
* $O + a = a$;
* $I * a = 1$;

###### Existence of Inverses
* ${\forall a \in \mathbb{F}}, \exists {-a},  {a + -a = O}$; 
* $\forall a \in \mathbb{F}, a \ne O, \exists a^{-1}, a^{-1} * a = I$;

So, essentially, a field is something that works a bit like the rationals and the reals, though there are also others like *{{< special >}}finite fields{{< /special >}}*.

While the stereotypical example of fields are the rational numbers ($\mathbb{Q}$;) and the real numbers ($\R$;), any set for which you can provide these two operations (and identity values) in a manner that conforms to the axioms can be classed as a field (and there may be sets for which multiple pairs of $+$; and $\* $; can induce a field as well. Since the field technically is not just a set $\mathbb{F}$; but the set-plus-associated-operations-and-identities $(\mathbb{F}, +, *, O, I)$; that is fine, it's just important to be aware that when referencing a field $\mathbb{F}$; it usually involves some canonical/standard $+, *, O, I$; as well.

Of course, there are a bunch of related mathematical theorems to the concept of the field, and interesting algebraic objects, but I'm not going to cover those here since it's just a summary.

#### How does this relate to vector spaces?
Well, the way it relates to vector spaces is that a vector space $V = (V, \mathbb{F}, +: V \times V \to V, * : \mathbb{F} \times V \to V, \vec{0} \in V)$; is defined over a field $\mathbb{F}$; with several axioms relating to vector addition $+$; and scalar multiplication $\* $; that amount to the following (with $a, b \in \mathbb{F}$; and $\vec{v_0}, \vec{v_1} \in V$;):
* $(a + b)\vec{v_0} = a\vec{v_0} + b\vec{v_0}$; and $a(\vec{v_0} + \vec{v_1}) = a\vec{v_0} + a\vec{v_1}$;
* $\def\v{\vec{v}}\forall \v \in V,\exists  {-\v}, -\v + \v = \vec{0}$;
* The vector addition identity $\vec{0}$; has $\vec{0} + \vec{v} (\in V) = \vec{v}$;
* The identity $I \in \mathbb{F}$; has $\def\v{\vec{v}}I\v = \v, \forall \v \in V$; 
* Multiplication doesn't break - $a(b\vec{v_0}) = (ab)\vec{v_0}$;

The typical example of a vector space is $\R^n$; - which involves tuples of $n$; real numbers  - like $\(r_0, ..., r_{n-1}\)$; - with scalar multiplication (and vector addition) defined elementwise:
$$a\(r_0, ..., r_{n-1}\) = \(ar_0, ..., ar_{n-1}\)$$; 
and
$$\(a_0, ..., a_{n-1}\) + \(b_0, ..., b_{n-1}\) = \(a_0 + b_0, ..., a_{n-1} + b_{n-1}\)$$;

This space also has a number of nice properties, like an inner product:

$$\braket{\(a_0, ..., a_{n-1}\), \(b_0, ..., b_{n-1}\)} = \sum_{k=0}^{n-1}{a_k * b_k}$$;

and a standard Euclidean norm $\def\v{\vec{v}}\|\v\| = \sqrt{\braket{\v, \v}}$; {{< aside idea inline >}}norms are a notion of length{{< /aside >}}

**However**, $\R^n$; is far from the only possible vector space. In fact there are plenty more abstract and strange ones out there, and many of them do not necessarily have finite dimension or a simply defined norm. Some examples of the more *interesting* vector spaces are listed below:
* $\Bbb{C}^n$; - that is, vectors of *n*-tuple complex numbers associated with the field of complex numbers $\Bbb{C}$;, with similar definitions to $\R^n$;
* Non-finite-dimensional vector spaces of functions, such as those where the basis vectors are the functions $f_{\omega \in \R}(t) = e^{i\omega t}$;, relevant in Fourier Transformation.
* Finite and Non-finite-dimensional vector spaces (Hilbert Spaces) used in quantum mechanics, in which the field is still $\Bbb{C}$;, but the vectors are often labelled with quantum numbers associated with different states and typical work involves switching bases.

