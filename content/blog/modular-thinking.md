---
title: "Modular Thinking"
date: 2022-07-27T19:22:53+01:00
draft: false
finalised: true
tags: ["neurodiversity", "adhd", "thinking", "learning", "psychology"]
---
In my life, I spend probably far too much time examining my own thought processes. In doing so, I have identified strategies that I engage in that allow me to learn and develop effectively, and asynchronously - that is, without necessarily knowing or understanding the prerequisites for a given piece of knowledge and all the associated background. 

I call this method of thinking and organising your understanding "modular thinking" or "capability-oriented thinking", and it emerged in myself in response to a number of factors - however, I think it may be extremely useful for almost everyone with any interest in developing or connecting wide-ranged ideas across a broad range of topics they do not necessarily have a deep understanding of. Some of the factors that I believe made me think this way are the following:
* A tendency to read information I found interesting even when I lacked the prerequisites to fully understand
* A desire for generalisation and abstraction
* A strong distaste for wrangling specifics, due to my tendencies to make "silly" errors related to ADHD-induced poor working memory
* A distaste for rote memorisation
* A recognition that knowledge and the specifics of it are increasingly accessible at our fingertips via the Internet, such that learning specific internal details is less important for actionable reasoning
* A tendency towards making connections between very disparate concepts (related to attention-wandering common in ADHD) with only vague similarities

This originally started as an aspect of my interest in mathematics, as I would oftentimes read big texts with stuff way beyond what I was actually capable of working with, and it forced me to develop the ability to run with a series of logical statements or definitions without necessarily understanding the definitions, terminology, and statements in a wider context that I had no access or knowledge of, a technique I would generally term as "blackboxing the unknowns".

However, over time this has developed into a generalisable mode of thought which allows me to grapple with concepts and facts at almost arbitrary levels of granularity in not just maths, but technology as well as general logic, and in hypothetical scenarios of alternate physical laws. 

I call this strategy "Modular Thinking", "Capability-Oriented Thinking", or "Abstraction Thinking". The core principle that my own thoughts seem to follow is the following:
> When learning or thinking about knowledge or technology, focus more on what it allows you to do - and the more general concepts surrounding it - than on the specifics of it's implementation

That's not to say that specifics aren't important - they are - but an acknowledgement that in almost all cases when developing something - be it a proof, a logical argument, a new technology, or anything else - the key task is not digging into the specifics of any arbitrary single piece of knowledge, but understanding which pieces of knowledge, technology, or similar need to be (or can be) connected together to do one thing or another. 

Furthermore, you can work this technique backward when you encounter some series of statements you know to most likely be true, to derive properties about the terminology and information being used to reach some conclusion at the end of the statements - though this is mostly applicable in mathematics and more theoretical physics. 

This is the original thinking technique of "blackboxing the unknowns", and it allows you to contemplate and get a partial grasp on various pieces of knowledge without necessarily having all the background information to it, by treating the prerequisites and results encoded by that knowledge based on what the knowledge *does* with them rather than knowing their full definitions, allowing you to keep up and fill in missing background information later. 

If the knowledge doesn't come from a reliable source - or the field is not amenable to more binary logical statements - it is not such a good idea to use this technique of blackboxing the background information, as it is more likely that you will accidentally "learn" false things or make poor assumptions, than that you will successfully "jump ahead" until your background information catches up to the requirements for the knowledge (which is the original intent of this technique).

#### Some Examples
All of this is well and good, but I find it useful to read examples of something to really get a grasp on what it means and how to use it, so that is what I will do. 

The first example of how this method of thinking has come up in my life is in the construction of mathematical proofs. During my degree, a common task was to construct a proof of some statement or another, given some other facts or statements. 

Because of the fact that when I learn things I tend to do it in the manner of Modular Thinking, even if I did not memorise the specifics of all the proofs we were meant to know, I could identify various properties associated with the prerequisites and outputs of each proof and then try and hook them together in a graph that takes the prerequisites of the question and finally produces a compound proof built out of known proofs, of the final desired statement to prove.

In essence, because my understanding of proofs was more focused on what they operated on and produced - that is, statements or objects with various properties - than on the specifics of the proof itself, it made it far, far easier to connect the proofs together based on the properties of the initial statements and the final statements than it would be if I spent more time memorising the proofs than understanding what they let me do. Of course, this did hinder me a little with "regurgitating" proofs, but because proofs tend to be dependent on other proofs, I could generally reconstruct the specifics of them.

As this method was initially something I organically came up with because of maths, maths is a very very good fit for this mode of thought, and this is where I started to first get a serious handle on my own thinking. It also illustrates most clearly the intrinsically fractal nature of this way of thinking - in particular, consider the fact that proofs are composed of smaller proofs, and the mathematical objects themselves can also be said to have any number of potential operations or uses upon them. Essentially every part of the series of connections can be divided up into a deeper series of connections, or abstracted away into a single node or connection that has some arbitrary set of capabilities.

The second example is that of developing new technology. This is a more recent development of mine, but it is an extension of the original source of this to begin contemplating technologies in terms of what they let you do with a given piece of information, material, or output of another technology. 

For example, when contemplating potential routes to 3D-printing integrated circuits, we have to contemplate finding technologies that allow us to, say, create the required precision, actually translate the "created precision" onto substrate as a circuit, or can keep air free of dust, or any other number of things. 

These can be separate or part of one uber powerful technology or discovery, but the point is that by thinking of what we require, as well as basic phenomena (like lasers or electron guns or linear motors) that may or may not allow us to create those things under certain constraints, and then looking at potential existing technologies in terms of when they can create something with one or more required properties when given another thing with some other required properties - we can efficiently sort through the space of possible solutions without having to understand every sub-technology in exquisite detail.

This mode of thought can be easily enhanced by thinking of scientific papers just as we do some existing technology, and scanning through scientific papers to rapidly discover what kinds of things they allow us to create from what kinds of things they used to test - for instance, a scientific paper that documents the effects of going above an energy threshold in a specific material can be thought of as a technology that takes something capable of going above that threshold, and something else that results in the material it studied, and produces the effect in the material. Then you could, say, examine papers relating to the results of the effect for further technological possibilities.

#### Learning To Do This
Now you've read this, perhaps you are interested in learning how to think this way? Well, I kind of accidentally learned to think this way, but I have some ideas that might or might not help in manually learning to think like this if you don't already.

Fundamentally, this mode of thought is embodied by the idea of associating a concept with its relationships (capabilities, properties, etc.) to other concepts, even if you don't know the internal specifics of a concept, in what essentially amounts to holding arbitrary concepts with their relationships in your mind, and trying to connect those concepts *using* their relationships when they are similar to those of another concept, to accomplish one goal or another by constructing a graph - upon which you can, say, locate gaps in the required graph and attempt to find new ideas and concepts to fill them, or perhaps prune concepts that turn out to be unnecessary, or locate alternate concepts that may accomplish the same overall collection of relationships as another.

So, onto the ideas about learning to do this...

##### Adjust the new knowledge and information you take in
When you're on a Wikipedia dive, about some interesting technology or mechanism, contemplate what you might be able to do with it, or what it can do with other things (e.g., does it need light input, does it produce energy in some form, etc.). 

Be creative, and try to understand or think about what constraints of that technology exist, what levels of precision or what types of material it can act on, or similar such things. 

If you have trouble with the background concepts, just keep digging through Wikipedia or other resources to learn about them. You only need an overview - that is at least half the point of this mode of thinking after all - and try and examine what properties of the various things are used by the technology referencing or using it.

This is also useful for learning how to identify what a technology can actually do for it's use in other technologies, without having to dig into the internals. In many senses, this is a form of skim-reading.

##### Go over existing knowledge in a new light
Of course, we don't live in a fantasy novel, so we can't rummage through our memories like they're some sort of archive, but when you think about some interesting technology or information, try to take a little time and think about how it relates to the outside world - what is required for it to work or to construct it, what other technologies allow you to do similar things, or perhaps what its outputs might let you do. The point is to focus on the things it relates to and how it relates to them, more than on how it works specifically. 

I don't know if this will actually work to aid in associating information with how it is used or how it relates to other things, but based on the principle of "neurons that fire together wire together" and the concept of practicing a skill to improve it, it certainly can't hurt.

##### Allow yourself to free-associate
When you have started to strongly associate knowledge or technology with its uses, when your mind wanders, you might start connecting different modules (of knowledge, proof, technology, etc.) with similar capabilities or properties either on input or output (if those are useful concepts). Don't just ignore this, but embrace it and mentally examine if that connection is actually useful. Continuing to do this should practice refining useful connections between ideas from ones which are actually nonsensical or meaningless. 

##### Practise creating mathematical proofs
Go find some university level maths exam questions and dig into the concepts around them and find some proofs. Learn some of the properties and proofs, and try to answer some of these questions. 

At least in my opinion, practicing the construction of mathematical proofs from smaller proofs which you are aware of is the best approximation of the more general concept of modular thinking, and learning to do this - that is, scanning the space of knowledge and it's relations, finding useful connections and pruning those irrelevant for the task - is almost equivalent to the general mode of thought I would describe as "Modular Thinking", and the practise should be transferrable, as it seems to have been for me. 

One of the things that helps with general modular thinking is being at least passingly familiar with a lot of relevant concepts and what they let you do, as it means they are easier to remember and there's less overhead to remembering what they let you do (especially if you don't have to look them up). This goes for maths, but also other things - for instance my familiarity with what things are available when programming, and using them, means finding something to do some collection of operations I need is quicker than if I had to dig into it more, because the initial concept in my brain is more fine-grained than if, say, I was unfamiliar with any programming and had to dig in to the basics before I could find components I needed.

##### Deconstruct technologies into subtechnologies and relationships
It is also probably useful to mentally deconstruct a single technology and break it apart into components and their relationships to some arbitrary depth, and attempting to reconstruct it with a slightly different ability or come up with a new addition. For instance, splitting up "computing" technology into "CPU" and a whole bunch of others combined, and start thinking about their relations and capabilities, and then go deeper and deeper.

##### Try and perform tasks using tools on hand rather than buying a specialised tool
One of the things I take pride in is my ability to hack together a solution to a problem with the tools I have on hand rather than needing a specialised tool, by combining what different tools let me do. This is an example of modular thinking and good practise for the technique. 

For instance, imagine that you can't screw something in. Instead of thinking "oh crap, I don't have a screwdriver, I'm screwed!", instead think of *something else that can potentially perform the required action*. Perhaps you have a set of pliers that can grip the screw, or you have an adaptably-sized wrench that could do the same job, or something else. It may well be that you don't have anything like that, but in the case you do, you can do what you want without needing to go out and obtain a screwdriver. 

Here, instead of assuming that a screw requires a screwdriver, you have identified the actual thing that the screw needs done to it, and something else that can do that action, and combined them to perform the same task. You are thinking of the screw as a module with actions, as well as the other tools.

You could also go further up and attempt to figure out other ways of performing the job that you are using the screw to perform - for instance, if you are connecting two objects, there may be other ways to do that. 

This is a generally useful idea to learn more modular thinking, and also allows you to save resources if you start obtaining tools that are more generally applicable and adaptable. It also readily applies to things like cooking, too.

#### Conclusion
Hopefully this was both an interesting insight into my own psychology, and useful as a tool for others looking to learn the same skill of viewing things more focused on their relations and abilities rather than their internal specifics - even if those are useful and necessary in some cases. Bear in mind that understanding background concepts usually greatly improves your ability to even identify relationships between other concepts, and more knowledge will always improve this technique in whatever field you have it, but this should allow you to reason on even fairly unfamiliar fields of interest. 
