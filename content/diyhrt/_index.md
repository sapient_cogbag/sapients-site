---
title: "Copies of the Ultimate Transmasc/fem DIY Guides (for reference)"
date: 2022-02-03T07:26:44Z
draft: false
tags: ["trans", "hrt", "diyhrt"]
---
This holds site-local copies of the [transfem guide google doc](https://docs.google.com/document/d/1C8nqyvusNYg19CjnlZvGa-lFhoaitdxE8XuTry-1-YY/edit#) and [transmasc guide google doc](https://docs.google.com/document/d/1-PQjbeL14fw8Rwh6F0ElaOvsqnDbBvVcFvOHibzVRbA) listed at https://diy-hrt.carrd.co/ - using a [simple trick to download as HTML with postprocessing](https://askubuntu.com/questions/998384/how-to-convert-document-from-google-docs-to-text-file), in case the original gets lost or taken down.

Note the raw copies are here - the listed versions have HTML translated to be nicer in this site:
* [transmasc](ultimate-transmasc-guide/raw/DIYTestosteronetheUltimateGuide.html)
* [transfem](ultimate-transmasc-guide/raw/DIYHRTTheUltimateGuide.html)

Downloading and processing this is done through the following pair of scripts:
* [process-html.py](process-html.py)
* [rebuild-contents.sh](rebuild-contents.sh)

Notably, the important part is being able to download a google doc into different formats.

This script will download HTML and images as a zip folder containing the document named `$zipfile`. Feel free to use this for easy archival purposes.
```bash
# -L means following redirects ~ necessary for google docs download system
curl -L "https://docs.google.com/document/d/$google_id/export?format=zip" -o "$zipfile"
```
##### Transfem Document 
```bash
export google_id="1C8nqyvusNYg19CjnlZvGa-lFhoaitdxE8XuTry-1-YY"
```

##### Transmasc Document
```bash
export google_id="1-PQjbeL14fw8Rwh6F0ElaOvsqnDbBvVcFvOHibzVRbA"
```

Last Update: 2022-02-03 10:58:55+00:00
