---
title: "What Do We Mean by \"Political\"?"
date: 2022-01-30T03:11:28Z
draft: true
tags: ["politics", "philosophy"]
or: "questioning the fundamental assumptions of what we can really change"
---

Oftentimes, those of us who are more radical or just discuss certain topics will be told variations of the phrase "stop being so political". Or, perhaps, there's a minority person in some peice of media or another and that media gets condemned as "political" for it. Or even, some people or organisations engaging in activism for minority or general human rights will make the rhetorical statement that "this isn't political", or "this shouldn't be political". But what do people usually mean when they say this and what does it say about the current social context in which it is stated? 

### Disagreement
The first important notion in describing how the word "political" is commonly used is that of disagreement. An initial hypothesis of the nature of the word "political" and politicalness in general is that it is based by most people purely on whether something is disagreed upon in a group. 

This is of course incorrect - after all, while people joke about pineapple vs non-pineapple on pizza, or cheese on jacket potato being under or over the baked beans, and can in fact take surprisingly rigid and heartfelt positions on these matters - they generally would not be considered "political" by most.

However, it *does* provide a good foundation as for how it is used in casual conversation in that it allows for the idea that what many people consider "political" can and has shifted over time based on what people accept as "common, societally-significant, and yet to some degree acceptable disagreement" within any given group. A few years ago, "kill Nazis in fictional works" was considered a relatively apolitical statement, as can be seen by the many games and films that treat them as general enemies for casual shooting/axing/poisoning/killing etc. Whereas now, something like [killing Nazis in Wolfenstein 2](https://www.theverge.com/2017/6/12/15780596/wolfenstein-2-the-new-colossus-alt-right-nazi-outrage) can be considered "political" by many Nazis, alt-righters, and those sympathetic to them.

Essentially, in a given sufficiently large crowd, people often describe things as "political" when they sit near the edges of the [overton window](https://en.wikipedia.org/wiki/Overton_window) of a large enough proportion (whatever that may be) of the crowd in question. This is a potentially oversimplified explanation however, as it fails to account quite so simply for things like the mere *existence* of a minority in a videogame or something similar being classed as "political", and it still has many gaps in it.

### Assumptions and Politicality
A more accurate description of politicality in my view - and one much less fragile than the fickle overton window definition I provided above, at least to me - is one based on the *frameworks* people in a given group are using to view the world and the assumptions that they generally hold to be true. This definition also allows for examination of why a given group or subgroup of people may consider an *external object or subject* to be "political" without needing some internal disagreement.

In particular, we can instead say that what a given group of people consider "political" is, at least partially, composed of the *assumptions they hold* about both society and their place in it - and the degree of disagreement and challenge they can tolerate to that - as well as the probability that the *differences in these assumptions* within the group may provoke conflict between members of the group when exposed to a given thing. This definition also introduces the notion of "more" and "less" political rather than simply "political" or "apolitical" into our definition.

For instance, if a group consisting of a spread of people from hardcore anarchists (hi!) to alt-right fascists were to exist with those at the more extreme being sufficiently populous to affect the conversation, then that group may consider something as simple as "I'm trans" to be a political statement because such a statement - in the context of the anarchist and fascist subgroups present - is likely to provoke significant conflict *related to their views and assumptions of how society both **is** and **should be***. Now in practise such a group is unlikely to be sustainable because the worldviews of anarchists and fascists are so directly in conflict almost anything would rip it to shreds. 

In summary, this component of a potential description of how the word "political" is used means that something is more or less considered political by many depending on:
* The degree to which it disagrees with the *most common assumptions* of a group (or the people who speak most in that group)
* The degree to which it provokes disagreement *between* members of a group (or the people who speak most in the group), where this disagreement is based on differing assumptions and frameworks (as opposed to what is considered personal preference, like the pineapple or no pineapple on pizza debate)
* (in some cases) the degree to which it provokes collective disagreement in the group with a wider scale worldwide or national set of general frameworks and assumptions. 

  For instance, a community of vegans discussing recipes may consider a discussion of the meat industry political even if they all generally agree on it, because the country in which the group is founded does *not* agree with their views and assumptions on the whole or it is considered a "political" issue in that wider group, or people disagree too much on *solutions* to the issue. 

In the case of the Gamer™ complaining about non-white or non-cis or non-straight or non-dude characters being political, that's because the existence of these characters challenges their assumption that *they are the default* - not only this but in a general audience this induces conflict between those who assume this less and those who assume it more, which means it is considered by people in those groups to be "political" for both major reasons.

The Nazi case is fairly similar. Nazis as a generic enemy is now considered political in some groups because the number of people with assumptions that are tolerant or OK with Nazis in those groups has increased significantly and as such the statement now provokes conflict rather than say a single Nazi-sympathiser just being ignored or pushed out of the group as it used to be. This isn't necessarily a shift in the Overton Window (though that is definitely a cause for this specific case) but a shift in the distribution of potential conflict-igniting assumptions within the group.

### The Second Component - Government and Social Organisation
While all the discussion above is all fine and dandy for one half of the usage of the word "political", it misses out on another very common usage. That usage being that something is considered "political" when it relates to *elections*, *governments*, and more generally non-economic state policy. This is a very different and less fundamental notion of politicality related to the specific structures involved in "democratic" states. For instance, discussing whether or not you vote Republican, in America, would probably be considered a political statement by most people, and "not voting", by many, is considered being "apolitical".

People often also treat *economics* as apolitical - especially in the modern context where most (not all though, to be clear) parties' economic policies within countries only really vary between welfare liberalism, Thatcherite neoliberalism (which for American audiences is *conservative*, since the word "liberal" is very overloaded) and protofascist (or outright fascist) reactionary policies - all of these being built in context of assumptions present in capitalism like the legitimacy of private property and of hierarchical corporate organisation.

This is quite an incoherent set of things to make sense of when attempting to describe politicalness, so we'll start with the big one. 

#### Voting 
When discussing *political* actions, one of the things that comes to most people's minds is voting for a member (or members) of some organisation or party into a seat of power, like a seat in the UK parliment or the US congress.[^voting-is-all-politics] This is usually done with the intent to influence *policies set by the state/government in question* and thus to some degree change existing society, if only a little. There are many who consider voting *itself* an *apolitical* action (even if who you vote for is considered political), and some that consider not voting as apolitical and voting as political.

Examining through the lens of an assumption-oriented description of politicalness lets us make insights here as to how this is related to the assumptions people are using when they describe these things as apolitical or political - as in the context of our original definition. For instance, someone describing voting as an apolitical action suggests that their framework for politics either:
 * views the *current system of voting and surrounding assumptions* (or *an individual deciding to vote*) as a minor personal preference.
 * views the *current system of voting and surrounding assumptions* (or *an individual deciding to vote*) as something that is or should be shared amongst all in the group (here, usually, that group is the country) - i.e. as something that is not (or should not be) disagreed with by most people's personal political assumptions and frameworks. This isn't necessarily a recognised decision but instead is often completely automatic if the person hasn't questioned some assumption in their own mental framework for what politics is.

We can generalise this into a more powerful idea of what it actually means for something to be described by someone as "apolitical" and how that is affected by assumptions, placing - for a given person - their assumptions along a spectrum.

This spectrum goes from Foundational Assumptions (these are assumptions that almost all members of a group have (either conciously or unconciously) and work within - the further in this direction you go, the less likely someone is to have questioned those assumptions and the more likely they are to view those assumptions as apolitical) - to Disagreeable Assumptions. 

Disagreeable Assumptions or views are those that someone is more likely to have thought about and that they *expect* to find people who do not accept that assumption or media that conflicts with it.

On an individual level this spectrum essentially describes how deep someone has gone into why they think what they do and what possible alternatives there are, and on a group level it describes how likely someone is to find another within a group that disagrees on it. After all, the more people who have thought in detail about their foundational assumptions and turned them into more disagreeable assumptions on a personal level, the greater the chance that some people changed their minds on a foundational assumption such that it becomes less foundational group-wide.


<svg width="100%" height="4rem" style="display:inline" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <linearGradient id="warning">
            <stop offset="0" stop-color="#0000FF"/>
            <stop offset="1" stop-color="#FF0000"/>
        </linearGradient>
    </defs>
    <rect width="100%" height="100%" fill="url(#warning)"/>
    <rect x="49.5%" height="100%" width="1%" fill="#FFaa00"></rect>
    <text x="15%" y="1.4em" text-anchor="middle">Foundational/Common</text>
    <text x="15%" y="3em" text-anchor="middle">Assumptions</text>
    <text x="90%" y="1.4em" text-anchor="middle">Disagreeable</text>
    <text x="90%" y="3em" text-anchor="middle">Assumptions</text>
    <desc>Gradient from foundational/common assumptions, to politicalised assumptions.</desc>
</svg>

In the case of the person who describes voting of any sort as apolitical in some way - as a civil duty or as a "make your voice be heard, doesn't matter who you are" thing and that everyone should do it - they are holding some view or assumption that is about halfway between foundational and disagreeable. 

This is because they are, usually, *assuming* that the reason another person *doesn't* vote is wrong but often don't question what more foundational assumption makes them think that. It's close to disagreeable because, of course, there are plenty of people who don't vote, but the reasons why or why not are not often examined beyond a single level of "civil or national duty" and "make your voice be heard" - or in the other case "voting makes no difference".

That's not to say that that reasoning doesn't or can't exist, or that you can't dig deeper into alternate reasons you think people should vote or don't vote, but in many cases, this is a near-foundational assumption for people in favour of others universally voting or against voting.

For the person who doesn't care if someone does or doesn't vote, they have a very different set of assumptions involved here - and someone's choice to vote or not is not in conflict with either their near-foundational or more disagreeable assumptions, instead being *acceptable* by one of the person's assumptions that means they view voting as a personal choice.

#### Complexity
It is important to note a source of much increased complexity for the model. Someone's more foundational assumptions can affect their less foundational views significantly as well as how much they are capable of reconsidering disagreeable assumptions/views, as the more foundational an assumption the more it affects how they think about *everything* as opposed to more specific topics - after all, the more foundational an assumption generally the wider-ranging it is in terms of effect on how someone views the world.

For example, someone who takes as a *highly foundational assumption* that capitalism is the only valid economic system may well be working this assumption into their other views - for instance, if they are somewhat leftist, they may consider things like increasing taxes but until they think about capitalism as an implicit, foundational assumption and convert it into something more disagreeable, they cannot even begin to question it.[^capitalist-realism]

Luckily for our ability to question things, questioning a more disagreeable assumption and realising potential alternate viewpoints or assumptions that do not even fit into a foundational assumption we didn't even know we had, can allow us to question the foundational assumption as well. In the case of our hypothetical leftist, describing options like worker-owned cooperatives or extreme automation may cause them to realise they are making implicit foundational assumptions as to how work and labour "has" to be organised, when really there are more options.

#### Government
Next on the list is an examination of the idea of government as the primary or only source of "politicalness" on a larger scale under the foundational-to-disagreeable-assumption/view lense. Generally, when people call something political, they are expressing that something either conflicts with or is related to one of their more *disagreeable* assumptions/views as opposed to their more foundational views and assumptions. So when people express the idea that *government is politics*[^nongovt-politics], I would suggest strongly that this is because they are expressing that the government is the only way they see to apply their disagreeable assumptions in practise or move towards them.

The foundational assumption involved here - that is, the reason that someone would say that "politics is the government" is itself an apolitical or neutral statement - is that political change only comes through government channels and official methods of representation. 

This particular assumption is tenacious and easy to unconciously internalise, while providing a nice outlet for frustration at the system via voting and reducing other challenges to foundational assumptions people have absorbed from surrounding society while growing up about the nature of power and the state itself. 

It also means that when people change their disagreeable views for one reason or another they are less likely to reexamine more foundational assumptions because their change in disagreeable views can still *appear* to be accommodated for by the foundational assumptions around the state and governmental systems.

Along with this, it provides a unification point to reduce conflict between groups with different *more disagreeable* assumptions since the acceptance of the voting system as legitimate (as a more foundational assumption) provides a commonality that *according to the foundational assumptions around voting and legitimacy* will resolve the conflict "fairly" even if in practise the system is not at all fair (for some definition of "fair").

This is a good illustration of how a more foundational assumption can severely affect how less foundational views and assumptions are handled, how they can affect the ways in which group-level disagreeable-view conflicts are resolved, and how disagreeable views that are less foundational may be restricted or opened to different options when changing as person reexamines things, based upon existing foundational assumptions.

### The Other Definition of Politicality
While our current work has very effectively (in my opinion ) illustrated the how and why people call things "political" as well as providing a framework for handling some hefty political philosophy ideas in the context of assumptions, foundationality and disagreeability, we still technically speaking have not examined the [dictionary definition](https://www.dictionary.com/browse/political) of the word political:

1. of, relating to, or concerned with politics[^poldef]: political writers.
2. of, relating to, or connected with a political party: a political campaign.
3. exercising or seeking power in the governmental or public affairs of a state, municipality, etc.: a political machine; a political boss.
4. of, relating to, or involving the state or its government: a political offense.
5. having a definite policy or system of government: a political community.
6. of or relating to citizens: political rights.

The first definition is essentially completely recursive (the definition of `politics` just rolls right back round to the definition of `political`). The second we've mostly examined but it's good to go over - here, "political" is in respect to the fact that a political party represents a collection of disagreeable views that they are advocating for (members of the party of course do not share the *same* views but they're close enough to work together). 

The third is not really the use of "political" that we have been discussing, though the statement itself does contain some interesting foundational assumptions like the existence of a state or the ability to accumulate large amounts of power over others within a structure - the fourth, we've covered as a subset of the idea that the government is politics but it does provoke a discussion on precisely *why* we consider the government to be political in nature and what that says about generally classing things as political independent of personal and collective assumptions classing it as such - i.e. how we determine what is a "personal preference" versus what conflicts with our disagreeable views.

Fifth, we have covered - a clear policy means taking a stance on a disagreeable view, which is something people describe as political.

Sixth is the most interesting and important one because it provides us with a way of finding at what point do personal preferences become political and vice versa. So far, we've primarily discussed how people view politicalness and how it can be understood on an individual and group basis in the context of foundational and disagreeable assumptions.

[^voting-is-all-politics]: In fact there are a (scarily) large number of people who think that this is the *totality* of *all* politics.

[^capitalist-realism]:  This is, to my understanding, a major part of Capitalist Realism - the idea (and often, *foundational assumption*) that no alternative to capitalism can ever exist in the future.

[^nongovt-politics]: This is not the case. Things like unionisation, worker coops, protests and riots, activism, non-governmental organisations - and other alternate organisational systems, behaviours, and structures to the government - are definitely political.

[^poldef]: https://www.dictionary.com/browse/politics
