#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

TRANSFEM_GOOGLE_DOC_ID="1C8nqyvusNYg19CjnlZvGa-lFhoaitdxE8XuTry-1-YY"
TRANSMASC_GOOGLE_DOC_ID="1-PQjbeL14fw8Rwh6F0ElaOvsqnDbBvVcFvOHibzVRbA"


function download_and_process_id {
    local doc_title="$2"
    local subdir="$3"
    echo "Grabbing html contents of document $doc_title and placing in ./$subdir"

    echo "Making tempdir"
    local tempdir="$(mktemp -d --tmpdir 'hrttmpdir.XXXXXXXXXXXXXXXXXXXX')"
    echo "Tempdir location: $tempdir"

    local google_id="$1"
    echo "Google Docs ID: $google_id"

    # Download the document as a zip of HTML and images.
    local url="https://docs.google.com/document/d/$google_id/export?format=zip"
    echo "Target URL: $url"

    local zipfile="$tempdir/hrtpagezip.zip"
    # L means follow redirects (google seems to use redirects internally for this stuff nya)
    curl -L "$url" -o "$zipfile"
    echo "Successfully downloaded zipfile. Unpacking..."

    # Unzip the file out into the folder ^.^
    local unpackfolder="$tempdir/unpacked/"
    unzip "$zipfile" -d "$unpackfolder"


    echo "Processing file contents into Hugo document ^.^..."
    # Glob all the html stuff together, process, and output into a temp result file
    # This will also transform image links to raw/images 
    ./process-html.py "$doc_title" "$tempdir/unpacked"/*.html > "$tempdir/index.html"

    # Remove initial unpacked html contents
    echo "Moving all initial contents and final index.html into unpacked folder."
    mkdir -p "$tempdir/raw/"
    mv "$tempdir/unpacked"/*  "$tempdir/raw/"
    mv "$tempdir/raw" "$tempdir/unpacked/raw/"
    mv "$tempdir/index.html" "$tempdir/unpacked/index.html"


    # Note: Can't use mv here because it will not overwrite directories in the target folder
    # recursively nya
    # Instead use cp and rm with reflink ^.^
    echo "Copying contents into final directory ./$subdir recursively..."
    mkdir -p "./$subdir"
    # * here because otherwise it puts an empty unpacked if not :/ nya
    cp -v --reflink=auto -R "$tempdir/unpacked"/* "./$subdir"
    echo "Removing temporary directory."
    rm -r "$tempdir"
    echo "Done!"
}

download_and_process_id "$TRANSFEM_GOOGLE_DOC_ID" "Ultimate Transfem DIY HRT Guide" "ultimate-transfem-guide"
download_and_process_id "$TRANSMASC_GOOGLE_DOC_ID" "Ultimate Transmasc DIY HRT Guide" "ultimate-transmasc-guide"

function update_date {
    local new_date="$(date --rfc-3339=seconds)"
    local tmpfile="$(mktemp --tmpdir 'tmpfile.XXXXXXXXXXXXXX')"

    cat "./_index.md" | sed "s/Last Updated\s*:.*$/Last Update: $new_date/i" > $tmpfile
    mv $tmpfile "./_index.md"
}

update_date
