---
title: "Stop Bootlicking Death"
date: 2022-09-10T10:33:06+01:00
draft: false
finalised: true
or: "a rant on people's attitude towards those who do not accept mortality"
tags: ["politics", "transhumanism", "philosophy", "technology", "rant"]
---
<br/>
{{< aside Warning >}}
This is at least partially a vent of my frustration at the attitude towards those of us who refuse to accept death, spurred on by a recent reddit thread. It contains arguments, but is also an expression of frustration and anger, and I might be more vicious than in other articles. If you don't like that style of writing, feel free to [click away now](/).
{{</ aside >}}

I recently [came across a reddit thread](https://www.reddit.com/r/NoStupidQuestions/comments/x708ym/how_are_people_not_freaking_out_about_death/) in which someone is asking about why people are not so bothered by death. While I have always opposed death - as a {{< tref "/articles/philosophy/00002-an-intro-to-transhumanism" >}}transhumanist{{</ tref >}}  - and been frustrated at people's attitudes towards people who do, this thread finally spurred me to write about it and express that frustration.

### Responses
In this particular thread, there are a fair few responses to someone not wanting to die...

* "Life does not have meaning without death"
* "Your life has no meaning anyway so it doesn't matter if you die and you should be OK with it"
* "Death is a part of life/the cycle/the universe/nature so you should accept it"
* "Death is just like what it was like before you were born, so you shouldn't worry about it"
* "Life/aging sucks so I actually would be ok with dying and so you should or will be too"
* "It is narcissistic to want to live forever"
* "Overpopulation means we all have to die"
* "Only rich people would get immortality treatment so you shouldn't care about it/should accept dying"
* "It would lead to rule by the elderly/rich/powerful and society would stagnate, so people need to die"
* Actual agreement that death is awful and we should not just accept it

These responses fall into two main categories.

#### The Most Disgusting

##### "Wanting to live forever is narcissistic"
I have no words to descibe the extent of my negative thoughts towards the people saying this. I find it utterly disgusting, that not being OK with people or yourself dying is seen as narcissistic.

Is it wrong to want to see the further future, to want to continue to experience things and not have it stop, to want other people or yourself to not be forcefully deleted from reality via biological restrictions on lifespan?

Is it narcissistic to want the world to be better for yourself and others, to not *cease to exist*, to *have a choice*, to aspire for a world that isn't drenched in death and ageing-enhanced diseases rather than just accepting the shittiness? 

I think this is a result of a certain mode of thought that is promoted by ideas like the "protestant work ethic" concept, many religions around the world, capitalism, and similar. The idea that suffering and misery and putting up with being treated awfully and being abused - and more generally, compliance to those with more power or current structures of power - makes you a better person than someone who isn't OK with that and attempts to change it.

##### "If people lived forever, the elderly/rich would rule us and society would stagnate"
I would argue that this is already the case anyway, and that any solution to the current problem would also solve it in the case of immortality technology. If you are OK with condemning those of us with less power to death based on fear of stagnation, surely it would be far more productive and less murderous to only apply that attitude towards those individuals you deem it acceptable to die instead of towards everybody, and avoid the cowardice of letting nature do your dirty work for you, and upon every person in existence.

Furthermore, even in the case of technology like mind uploading that would make it very very difficult to perform physical harm upon someone, that same technology would far reduce the ability for people to "rule" others in the first place because the same resistance to coercion would apply to those who would be victims of would-be rulers. At least as long as the technology is publically developed, which is most definitely the case in the current era as most anti-aging research is published in scientific journals. If a cure to aging was discovered and withheld from general use, there would undoubtedly be radical social pressure to release it to everyone.

Even beyond that, the people - like myself - who would embrace immortality technology would tend to be more radical or otherwise unaccepting of the norm and what is "natural". Immortality technology selects out those who are more conservative because those sorts of folks would not take it on the basis of it being "unnatural" or similar. Furthermore, the factors that lead to elderly people having trouble with new ideas - either because of social conditions or because of some aging process - are likely to be radically altered or eliminated by immortality technology:
* Immortality tech implies the complete prevention of aging - that is to say, eternal youth is an intrinsic component of immortality technology - so if you buy the idea that aging makes people more conservative and resistant to new ideas, immortality and anti-aging technology would actually imply a larger percentage of the population acting as younger people do today, causing the opposite effect to what people normally think would occur.
* If you buy the idea that older people become more resistant to change because of social factors - usually things like stability and inheritence of wealth from dying elders - this would tend to radicalising younger people in the case that the oldest people don't die anymore as more wealth piles at the top. 
  
  This is already happening today anyway, for a start, and I suspect immortality technology would accelerate the radicalisation of anyone younger than the current batch of people with large amounts of wealth, rather than causing society to stagnate, and it would give the "younger-equivalent" people more power as they are a greater proportion of the population.
  
  The idea that society would stagnate with immortality is nowhere near a certainty, it is just as likely immortality technology would have the total opposite effect, and rather difficult to predict. Even if it was to cause some kind of stagnation, are you willing to let billions of people die in service to preventing that?

##### "Overpopulation Means We All Have To Die"
This one is a very common refrain when immortality is brought up, and is arguably an extension of Malthusian, or Neo-Malthusian, attitudes. Sometimes, it's propagated alongside ecofascist ideology. 

It is a somewhat more valid criticism of immortality than (Neo-)Malthusianism is of population growth (where (Neo-)Malthusianism is often steeped in extreme racism or patronisation of non-rich people in general), if you assume no-one will die, but it still shares the same flaws and condemnation of people to death as Malthusianism - and it has a worrying "greater-good" type aspect to it just like (Neo-)Malthusianism.

The Earth is actually capable of supporting orders of magnitude more people than what a lot of people currently think, simply with more efficient farming, building, etc. techniques than are currently used - for some information on that, take a look at {{< tref "/articles/climate-adaptation/01-vertical-farming" >}} my article on vertical farming{{</ tref >}}, as well as the following interesting video:
{{< yt id="8lJJ_QqIVnc" title="Can We Have a Trillion People On Earth?" >}}
<br/>
{{< aside note >}}Note that I disagree with the overly-deterministic and monolithic way Isaac represents futurism and 'civilisation', but his videos still have excellent information on technology, statistics, and resource limits.{{</ aside >}}

There are a lot more ways that this planet can support orders of magnitude more people than it currently does in a non-environmentally-destructive manner that I may go over in future articles - like the ending of car-oriented cities, High-Energy Cyclic Economies, ultra-densification, etc., but that's a little offtopic here. This isn't even getting started on the possibility of orbital rings and mass-scale spacehabitats that should allow even further orders-of-magnitude increases in the human population (and ability for orders-of-magnitude more people to efficiently leave Earth), via offworld colonisation, without significant environmental damage.

Even beyond this and in the depressing-future scenario in which none of the space-oriented orbital ring stuff happens, there is still the fact that as long as resources are tight and people live longer with less death, people produce fewer and fewer kids. We can already see this today, and in the case of medical immortality the trend would likely continue. Even if the trend did not continue enough to entirely halt population growth, though, there are plenty of existing ways to so radically improve efficiency that it is not a problem.

Furthermore, it seems likely that there are a lot of people that want to live much longer but not *forever*, based on my experiences of talking to people about this, and based on the number of people who claim they only find their lives have meaning because they are very finite, at least for those where that is not just Stockholm Syndrome. The population, while it would certainly increase a lot, would not enter into hyperexplosion like some people's worst fears, at least based on the way people currently behave as their lifespans increase. That is to say, the "immortality tech means no-one will die" assumption of the Malthusian ideology, is actually incorrect even with perfect technology for preservation of someone's life.

Certainly, there will be many like myself who would (at least as far as I know of myself today) never ever willingly choose to die, but I would suspect strongly that many people would choose to do so after a few hundred years. The concept of living truly forever is actually extremely radical and not something many people seem willing to do even if they would like to live much longer.

Regardless, the idea that we should not give people the power to resist their aging and death based on vague notions of "overpopulation" disgusts me almost as much as the original Neo-Malthusian ideological concepts, even if I have taken the time to refute it here as on the surface it can be appealing to many.

##### "Only rich people would get immortality treatment so you shouldn't care about it/should accept dying"
Generally speaking, this is a common argument against several types of technological development, and it is deeply flawed especially for something so profound as medical immortality. In particular, biotechnology like drugs, vaccines, etc. that are effective even at tiny doses because they don't work via brute-force modification but by altering the fine details of biochemical reactions, are very amenable to affordable production once they are researched.

If we are talking about any sort of biomedical immortality as opposed to mind uploading, any attempt to withhold such an aging cure from the general population would result in several things:
* Riots at the injustice, almost certainly, until the formula is either given to the public or seized directly by activists
* Leaks of private production information such that others can use the research
* Black market production and distribution, as the price of production per-unit of biological anti-aging technology is likely to be rather small

If the research is open source from the get go - as more and more scientific research is, nowadays - this process would be radically accelerated.

For mind uploading, this is certainly a somewhat easier argument to make, but biomedical immortality in the form of drugs or a vaccine (that causes your body to remove proteins or something else that contributes to ageing, as after all mRNA lets us target any kind of protein) is likely to emerge first (that is, after all, where research is currently very active - it's not an "in the future" topic, it is already being heavily researched). 

This will probably induce radical social changes, and in combination with the increasingly open-source aspects of science research, I think that any mind uploading technology is likely to be publically known at the very least and if denied to the general population would result in unknown and unpredictable consequences.

In fact I think that the main difficulty with such a technology would be in terms of production of new computational substrate for mind-uploaded people, and in producing enough brain scanners for it, as opposed to the technology being hidden from the public.

#### The Philosophical
There are several philosophical reasons people provide for accepting death. Most of these - at least to me - come across as some kind of coping mechanism for the current situation.

##### "Life Has No Meaning Without Death"
This is one of those that doesn't even make sense to me and seems more like a coping mechanism or platitude to me. In particular, the finiteness of some thing is not logically connected to it's degree of meaning. For me, also, I personally have a sense of meaning that I define for myself that includes not dying as making my life more meaningful, and in fact I tend towards self-defined concepts of meaning as opposed to externally imposed meaning.

This idea in particular, at least in my observation, for a lot of people comes once again from the idea that if something bad happens it must serve some meaning or purpose. A lot of people seem to hate the idea that something shitty can happen for no good reason at all and then they back-justify it as being meaningful as a way to cope. To me, at least, it seems that a lot of people have gone down that route. 

For some folks, it may well be the case that they assign meaning to the shortness of their own lives, and I guess good for them if they do that, but don't pretend it's a remotely acceptable solution for everyone, especially as a lot of us find it insulting in the face of losing folks we care deeply about. In that case, it can come off as cruel and contemptuous of their life and our pain - or at least it does to me, in the same way that more religious folks saying things like 'she's happy and in heaven now with God's blessing' can be.

##### "Your life has no meaning anyway so it doesn't matter if you die and you should be OK with it"
This is basically a worse, nihilistic version of the "life has no meaning without death" concept. Maybe *you* don't think your life is meaningful, but I certainly don't agree. I think this may well be comforting to some people - in particular, it's a common refrain in "optimistic nihilism" philosophy - but if they use it to try and justify not worrying or being in favour of biomedical immortality technology it once again seems contemptuous of other people wanting to actually have agency and choice. 

If someone actually thinks this about their own life and is just chill, though, I don't really care. But for me, the development of immortality technology is something I do find highly meaningful, but I don't advocate for nihilism (whether the optimistic kind or any other), so this phrase comes off as unhelpful at best and apathetic of suffering and death at worst, at least for me personally.

##### "Death is a part of life/the cycle/the universe/nature/god's plan so you should accept it"
This, to me at least, is just straight up Stockholm Syndrome. It exemplifies the attitude a lot of people had to diseases like Smallpox before medical science seriously accelerated. 

In particular I more generally find the entire concept of accepting "the way things are" to be awful and on a personal level it goes against everything I stand for. I think this is a philosophical position that is common in almost all religions and also in capitalist ideologies. 

On a personal level, I find such philosophical concepts to near universally be used as a means of encouraging those of us with less power to accept abuse and subjugation and horror as being OK and valid because "that is simply how things are". It is an attitude that is fundamentally antagonistic to the concept of people having agency, and for me this criticism of "just accept and be OK with the circumstances as they are rather than fighting to change them" applies even more so in the case of attitudes towards death.

It comes across, as it does in many other cases where this attitude is prevalent, as pseudo-wisdom. At least to me.

##### "Death is just like what it was like before you were born, so you shouldn't worry about it"
This to me, again, comes across as a method of coping with death for people who are unsure about what "comes after" it. It is often used to help people who have recently left some kind of religion to come to terms with the idea of lacking an afterlife.

Once again, though, it is not a reason to not accelerate development of immortality technology. It only helps ease those who's only fear around death relates to "what comes after", as well. For many of us - me included - I utterly loathe the fact that I may *cease to exist* in the first place. I'm not afraid of "what comes after", I just straight up *don't want to stop being alive in the first place*. I'm afraid of (well, more than that, I *hate the idea of*) death because I don't want to not exist again, I don't want to end. It's not about what non-existence is like, but about the fact I don't want to cease existing in the first place.

I also don't want me or others to suffer the cruel loss of people to the ravages of time. I consider my own life - and that of other people - to have value. I don't want my life and collection of experience - or other people's - to be lost to the world, to cease forever, to have the choice taken away. Furthermore, many people suffer immensely as they die or even just age, and this itself is yet another cruelty we endure in reality as it currently exists today.

#### Other

##### "Life/aging sucks so I actually would be ok with dying and so you should or will be too"
This one is just rather depressing, though it is important to note that aging and death are fundamentally tied together. Any immortality technology intrinsically involves solving aging.

As far as life sucking though, well, I guess my answer to that is *destroy capitalism and make life better*. Or, perhaps, see a doctor and see if you are suffering from depression or a similar condition. Ultimately, though, if your life sucks enough you don't even care about living much that doesn't mean you shouldn't be in favour of technology to give other people the choice.

For me, my opposition to death is a direct derivative of my fundamental, philosophy-defining hunger for morphological autonomy and agency in general, which is the same reason I am in favour of assisted suicide for those who really want it. But we must fight to make the world a better place to live in rather than just accepting the current levels of shittiness.

##### Actually thinking death is bad
Some people actually do think death is horrible and we should fight for immortality technology. And of course, I agree with those people. However, a few people cite *Harry Potter and the Methods of Rationality* as a good story to read about this and discover concepts/ideas (it is a fanfiction of Harry Potter that has it's own cult following).

For a lot of people, including myself at a very difficult time during my life, that story was a good introduction to the idea of not just accepting death as OK and good, and fighting for people to be able to live forever and of questioning things other people take to be the norm. However, that story is also awful at almost all other philosophy, including it's concept of transhumanism and the idea of closing off and hiding technology from mass distribution, and being in favour of the creation of a technocratic elite, which goes in direct counter to the scientific method, radical open-tech, and the knowledge and autonomy needed for people to truly understand and make real choices about their life and the technology they use upon themselves. 

It is a problem I see to a more general degree in sci-fi, and comes across as patronising and technocratic, and also out of touch with modern scientific research. Of course, I tend towards techno-accelerationism - that is, a desire to accelerate the development of new and radical technologies to push for social change, not what people usually mean by accelerationism - and I think the presented concept to go against both the necessary attitude to technology for the enablement of morphological-autonomy, and for better scientific research in general, and I think that it promotes a very patronising view as to the decisions people should be able to make for themselves as opposed to being forced upon them by others.

HPMOR should not be the face of those in favour of immortality.

### On the presentation of immortality-seeking in culture
I want to quickly address the way wanting to be immortal is presented in culture (note that this comes from a primarily Anglo and Western perspective, I'm less familiar with how it is in other cultures and in particular my understanding is that it's not presented so negatively in things like Xianxia). In particular, the way in which wanting not to die is usually presented as something evil.

In myths, legends, and fiction, and even sci-fi, wanting to live forever is often presented as evil or misguided at best. In almost all fantasy, the heroes are never the ones to seek immortality, it's always someone who wants to control or subjugate others or be generically Evil™.

Even in science-fiction, self-alteration to live longer, or changing your own body to age less, or mind uploading, is often presented as some kind of sign that someone is crazy or just plain evil. I have yet to find more than two works of fiction where immortality isn't presented as something either too dangerous to exist or just outright evil, and where accepting death is presented as the moral and ethical choice.

In one example, during the Harry Potter series (written by That TERF JK Rowling), we see two examples of immortality presented:
* The Flamels, where their source of immortality (the Philosopher's Stone) gets destroyed, because of...
* Voldemort/Tom Riddle, who ripped their 'soul' apart into pieces to preserve life, and attempted to use the PS to restore himself to life. This guy is basically a Nazi with magic powers.

For the first (the Flamels), it is presented as ethical and good for those two people to accept death because the Philosopher's Stone is "too dangerous to exist" after the second (Tom Riddle) attempts to obtain it. The second guy is obviously evil and being immortal is presented as part of that. His alias name even means "Flight from Death".

In another (sci-fi) example, we see something like Star-Trek, where self-modification is generally banned in the Federation after some weird thing with genetic supersoldiers. Naturally, they see genetic self-modification as wrong, at least to my understanding, and the people still age and die. To be honest, I am less familiar with Star-Trek than with Harry Potter's plot, but this is my understanding.

More generally, we never see seeking immortality as something presented as ethically good, and it usually comes along with some kind of grand sacrifice or is highly restricted such that only one or two people can become immortal. Or, it is presented as being in the realm of "higher beings" and any human trying to seek it is, again, usually presented as arrogant and probably evil, and "accepting their place" is then presented as some kind of character development.

Probably the only two stories with a positive presentation of attempting to end death that I've come across are *Harry Potter and the Methods of Rationality* (which has it's own major issues), and *The Fable of the Dragon Tyrant* which is a kind of modern-fairytale sort of thing (and it's pretty neat).

#### But Why?
It's worth trying to understand why wanting immortality is seen this way. To be honest, I think it originates from a couple of factors:
* Religion, and the hatred of "playing god", I think comes into this a lot. The idea of "knowing your place" is attached to this.
  
  This is translated to a secular context with the idea that actually fighting for things to be better for yourself or others is often treated as 'morally wrong' as opposed to enduring suffering and being subservient to the-way-things-are.
* The idea that seeking power over yourself and your life, and seeking power over others, are equivalent, and then using immortality as a cheap and generic "oh this guy is super evil and wants lots of power" shoe in.
* It's a good target for the trope of "forbidden knowledge" and can latch on to things like Christian ideas of original sin, where innocence and lack of understanding is seen as virtuous, in comparison to trying to learn and aspiring to understand things or do things that deviate from "god's plan" which is seen as sinful and wrong.
* Often, immortality in stories or even in real life is treated as some kind of sacrifice of some other aspect like "your humanity" or something more tangible like feeling love or similar.
* Immortality in fiction is also commonly presented as something you only have for yourself, and hoard for yourself. 

  In that case I would strongly agree that doing so would be awful, but it isn't a problem with immortality, instead it is a problem of hoarding information/technology instead of spreading it.

  Immortality in real life can't end up like this, because that isn't how technology works. Almost by definition, technology can be replicated over and over again. However, this presentation is so common in fiction that I think it taints the way people think of immortality. Certainly, if life extension was only available to me for some reason, I'd be pretty upset by that (though I would probably still take the offer and then spend a lot of effort trying to replicate it for others). 

### Final Thoughts and Comments
So, this half-rant and half-argument is pretty much finished. I could write about the religious arguments people make against immortality technology, or the weird naturalness fetishisation that some people have that causes similar reasoning against this sort of technology, but that's pretty boring and has essentially already been covered.

My final thought then, is on the seeming fear a lot of longevity groups have of advocating immortality. It is a trend lately that a lot of these groups that attempt to develop longevity research are only willing to advocate for "healthspan" (aging without losing function in the last 20-30 years) as opposed to true life extension (even though that kind of naturally comes about), because of people hating on the concept of immortality. To them, I say, stop being afraid. Advocate for the best option, the development of actual medical immortality, instead of kowtowing to bioessentialists who have an over-attachment to death as if it is somehow *good*. 

Acknowledge natural death as the atrocity that it is, instead of bending the knee to those who would keep so much of humanity unwillingly chained to our lifespans and doomed to oblivion.

