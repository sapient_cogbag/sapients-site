---
title: "Why I Prefer Viral Licenses"
date: 2021-10-26T10:02:21+01:00
draft: true
or: "open-source as an infosymbiotic superorganism"
tags: ["foss", "technology", "philosophy", "politics"]
---

#### Introduction
The topic of whether or not to use viral/copyleft or permissive/copyneutral licenses for any given project is one of significant contention in open source and free software communities. For those unfamiliar with the terms take a look at my {{< tref "/blog/foss/how-i-classify-foss-licenses" />}} to see what I mean by them.

Over the past decade, the trend of public projects seems to have shifted towards more permissive licenses - such as the [MIT License][mit] - as opposed to the more viral licenses such as the [GPL][gpl], though hard data to prove it is difficult to come by (the closest I found was [this GitHub blog post from 2015](https://github.blog/2015-03-09-open-source-license-usage-on-github-com/) which shows the start of this trend to a degree).

A large reason for this is the continued shift of large corporations - such as Google, Microsoft, and other tech giants - towards a more milquetoast "open source" (such as that promoted by [Eric Raymond][ESR], the (former) leader of the [Open Source Initiative][OSI]) that primarily focuses it's arguments on - in the authors rather cynical view - how open source can be adopted by making it trivial for corporations to use in the service of their own profit via {{<tref reproprietization />}}. There are articles out and about like [this one by Brian Cantrill](http://dtrace.org/blogs/bmc/2018/12/14/open-source-confronts-its-midlife-crisis/) claiming "open source has won" (just search on the internet with that phrase and you will find hundreds, this one was just on my mind).

This is in contrast to the moral/ethical arguments as presented by [Richard Stallman][RMS] and the [Free Software Foundation][fsf], which tend to more readily push for the [GPL][gpl] or [AGPL][agpl] license and more viral copylefting, even when it makes the software much harder to be used for larger corporations for profit purposes - though to be clear, only the [AGPL][agpl] is a significant obstacle to web companies (such as Google) in particular, since the other licenses do not prevent "cloud" companies from keeping their services locked down. Other software companies, however, do end up avoiding stronger copyleft licenses.

#### So why do I prefer viral licenses in most cases?
Well, the answer to that is fairly complicated and multifaceted. To start with, I want to look at what we use programs for, and more importantly, what a program can actually be said to *be*.

##### What is a program, really?
When you run code - be that compiled into a binary, or something like a python script, or javascript on a web browser - the fundamental process occurring is that of data input, manipulation, and output - whether that be sequentially (as in a streaming commandline tool like `grep`) or in a constant continuity of execution (as in, for instance, a simulation or any GUI program continuously taking user input).

You can start to think, then, that a computer (and in fact a computer network, or a circuit with microchips, or similar), is essentially a constantly changing, constantly adapting, constantly flowing network of information being received, processed, modified, and sent along.

This is all fairly obvious in parts, but recognising the intrinsic network created by the fact is important, because we are going to extend that abstraction.

##### What About Humans
Human beings, in this model, are also part of this network in two primary roles - they can consume data from some endpoint (by looking at it on a display, for instance, or any other similar method), or they can inject data into it (for instance entering values in a form, or something like a heartbeat monitor, or creating a drawing using a drawing tablet). Of course, they can do both in an ultra-complex manner and act as relays or an extra connection - or node with connections - in the network as well.

This fact also applies to things like sensors (e.g. a camera) and effectors (e.g. a motor), which currently are an intermediary between the outer world (including humans) and the global data/information network itself. In the case where a person is acting as a relay or node-with-relays, these are essentially a highly complex translation system for data so humans can process it, even as they remain physically detached.

##### Code is Data[^dataiscode]
In the context of what a program actually is, we can think about what a compiler or a runtime is - namely, it is a program and hence takes in, manipulates, and outputs data - from a human readable format like, say, Rust files, into data that your CPU can then interpret again as instructions to run (either directly or through another program like the JVM or python executable) [^1]. Thus we can say "code is data" - though of course lisp programmers already knew this.

Then we can extend the idea. The list of programs, networks, and all other data processing machinery that manipulates code-formatted data is not limited to compilers. Repositories like github or gitlab, `git` itself, any templating engine or code generator - all of these also constitute a vast, complex network through which chunks of code-data flow and are manipulated. 

The datastore backing any given code repository or project can be said to be a node of this network - it takes in new code from developers and if it is open source (or there is more than one 'node' within a closed-source organisation) it can "emit" code-data into other projects via developers sharing it or it's inclusion as a dependency. Often it also contains a history (like `git` does), and hence all states the repository - as a datastore - has ever been in are encoded and essentially can also flow into and out of the wider code-data network that we discussed earlier.

Of course, that node is itself an abstraction over a microscopic subregion of the global data processing network that is mostly self-contained with well defined inputs and outputs, where code-data is one subtype of data that it can process. But we abstract it even as we recognise the fact that it is, in itself, a subregion of the data-processing network and has all the same properties.

But code being data does not change the fact that code also manipulates data, and hence acts as a strong mediator for the wider, more general, network of data flow and processing that makes up all computational infrastructure. At each end of any software-mediated connection in the wider data-network, there exists a snapshot of all relevant code repositories in the global code-data network, compiled or interpreted into data that can be executed by the myriad of CPUs in any modern machine and most probably fed further through the computer to be emitted to the wider data network or to a human being.

In that sense, then, any time data is processed at the endpoints of the connections, you are *embedding the graph* of the code-data network snapshot as well as the overlayed graph of data flow *within* that network into the original network. Quite headache-inducing.

##### What Sort of Data is Code?
It's all well and good recognising that code is data, but understanding what sort of data - in terms of structure beyond "streams of UTF-8 or ASCII or other textual bytes" - code actually is. To me, at least, there are several possibilities for what it can be. 

The first - and one of the most important - is *metadata* for some format or protocol. The amount of code we run that is primarily oriented around turning raw byte streams/sequences into structure - that is a parser - or vis versa - that is a serialiser - is absolutely, comically large. In providing raw bytes *with* structure (or vis versa), the code intrinsically encodes that structure - that is, it *is* data *about* that structure and inherently describes how it is structured. 

We see this most blatantly in libraries which generate parsers and serialisers based on a formal definition of the structure ("language") provided to them, like 



 
##### A Lot of Code Is Metadata
How many times have you written or used code dedicated to parsing data? The answer is probably "more than anyone can count" - after all, every time you open a webpage you are running at least four or more implementations which involve giving raw bytes appropriate structure - the TLS protocol, HTTP parsers, HTML parsers, CSS parsers, and probably JavaScript parsers as well, at a minimum. And of course that JavaScript may be running any number of it's own parsers. Not even starting on the parts of the CPU that are also, essentially, parsers, or any number of other layers in the stack.

What I'm saying is, a lot of shit we run is parsers. So then we ask - given the context of 'code is data', what on earth parsers actually are. What type of data are they? 

Well, I would argue they are essentially a form of *metadata* of a format. The code itself is descriptive of the structure of the data - to parse a chunk of data, some peice of code intrinsically encodes the structure of the data by the very act of parsing it into a structure it can manipulate.

In the case of standardised formats or protocols, this code-metadata, when correctly constructed, should encode the structure of the format in a way that matches the standard. However, this does not merely apply to open formats or open protocols - it also applies (often even to a more extreme extent in practical consequences) to proprietary formats like photoshop files or Microsoft Word documents. Hell, to my understanding the way Word parses and displays documents from older Word versions is literally to pass it to the original code for that version because that code is the only data detailing how to parse such a format.

This idea can be pushed further, of course. In specialised domains, code also acts as a store of information for other operations on structured data - for instance, specialist digital image filters or a specific algorithm for forcing an out of date component to do what you need to do, like code containing some kind of cryptographic key any newly flashed BIOS image needs to be signed by to be accepted by a motherboard. Of course, I am not the first person to come up with the idea of code essentially being a store of domain knowledge - several people have mentioned it before even if I don't actually remember who - but it is another important aspect of the way in which code functions as a form of metadata for formats and data for domain knowledge.



[mit]: https://en.wikipedia.org/wiki/MIT_License
[gpl]: https://choosealicense.com/licenses/gpl-3.0/
[agpl]: https://choosealicense.com/licenses/agpl-3.0/

[ESR]: https://en.wikipedia.org/wiki/Eric_S._Raymond
[OSI]: https://opensource.org/
[RMS]: https://en.wikipedia.org/wiki/Richard_Stallman
[FSF]: https://fsf.org/

[^1]: (We're ignoring the deeper layers of the stack here like microcode, but you get the general idea.)
[^dataiscode]: The reasoning here also applies in reverse - every program can be argued to be an interpreter for an adhoc programming language consisting of it's input data which it is translating into CPU instructions, just like the JVM or the CPU's intrinsic instruction translation phase.

