---
title: "food desert"
tags: ["terminology", "food", "urbanism"]
type: "word"
draft: false
---
A food desert is an area in which access to grocery stores is very limited and typically requires a car and an extended drive. 

These are most common outside of well-urbanised areas, and usually means people within the food desert do not have accessible options for diet other than unhealthy, expensive junk from petrol stations or convenience stores. 

It is often a symptom of car-centric urban design and rapid suburb development (and by proxy, the complex causes of each of them).
