---
title: "Getting Linux on a Lumia 635 RM-974"
date: 2022-03-27T06:43:00+01:00
draft: true
---
Recently, I obtained some hardware from a friend that they otherwise would have chucked away - a Nokia Lumia 635, one of the ancient and now somewhat dead collection of Windows phones. I accepted it for a number of reasons but most importantly being that there is currently a chip shortage and I could do with some hardware to host this site and various other random shit on.

So, the challenge, then, was to get Linux to boot on this thing from my current access to hardware, which is apparently really fucking hard since almost all of the guides presume that the user is running Windows and the software available is oriented more around Windows as well. Obviously, I do not have Windows, so that is not an option.

The best resource I came across in my travels was the [PostmarketOS Windows Phone page](https://wiki.postmarketos.org/wiki/Windows_Phone) and the 630/640 pages linked there. This page contains several other resources - namely [a page with a bunch of firmware](https://www.lumiafirmware.com/model/RM-974/hwid/059W0K7), and the ultimate tool, [wpinternals](https://github.com/ReneLergner/WPinternals). 

#### Building WPinternals for Linux
The project WPinternals is primarily written in c#, and c# from well before the advent of Linux-compatible Mono and [DotNet](https://docs.microsoft.com/en-us/dotnet/core/install/linux). Luckily, the WPinternals project itself is set to use dotnet v6, but actually building it (using the command `dotnet build`) in the `WPinternals` subfolder of the main repository is unfortunately broken out of the box with an error:
```
/usr/share/dotnet/sdk/6.0.102/Sdks/Microsoft.NET.Sdk/targets/Microsoft.NET.Sdk.targets(1191,3): error MSB4019: 
The imported project "/usr/share/dotnet/sdk/6.0.102/Sdks/Microsoft.NET.Sdk.WindowsDesktop/targets/Microsoft.NET.Sdk.WindowsDesktop.targets" was not found. 
Confirm that the expression in the Import declaration ";/usr/share/dotnet/sdk/6.0.102/Sdks/Microsoft.NET.Sdk/targets/../../Microsoft.NET.Sdk.WindowsDesktop/targets/Microsoft.NET.Sdk.WindowsDesktop.targets" is correct, and that the file exists on disk. [<filepath to .csproj file>]
```

To be frank, I was not familiar with .NET build systems at all, though my guess was that some particular platform-specific GUI thing was not present on my Linux system, needed to build the software designed for Windows stuff. A little more testing seemed to confirm this notion, removing the `-windows` at the top of the specification for the `.NET` SDK, producing the following error:

```
/usr/share/dotnet/sdk/6.0.102/Sdks/Microsoft.NET.Sdk/targets/Microsoft.NET.Sdk.DefaultItems.targets(140,5): error NETSDK1136: 
The target platform must be set to Windows (usually by including '-windows' in the TargetFramework property) when using Windows Forms or WPF, 
or referencing projects or packages that do so. [<filepath to .csproj file>]
```

Using a simple `ag Forms` and `ag WPF` indicates that the program apparently uses *both*, which is very annoying and meant we probably needed to fuck around with `mono` to actually build thngs. To start with, I installed the `mono-tools` package, for development (after a lot of effort trying to build stuff just with `mono` and `mono-addins` :/). 

After some searching, I eventually tried looking [here](http://askjonskeet.com/answer/42861338/Building-VS-2017-MSBuild-csproj-Projects-with-Mono-on-Linux) for information on the build process - it seemed like `-f` and `-r` would allow overriding the framework and runtime, respectively (which are needed for the mono WinForms implementations I think). In my experimenting, I also removed the following bit from `WPinternals.csproj`, since they seem very Windows-specific and just cause errors on Linux (and why *any* build spec has a `pkill` equivalent command being run I have no bloody idea).

```
<PropertyGroup>
  <PreBuildEvent>
    "C:\Program Files\PsTools\pskill.exe" XDesProc.exe 2&gt;nul 1&gt;nul
    EXIT 0
  </PreBuildEvent>
</PropertyGroup>
```

Trying to run `dotnet build -f netstandard` gave some error spew:
```
/usr/share/dotnet/sdk/6.0.102/Sdks/Microsoft.NET.Sdk/targets/Microsoft.PackageDependencyResolution.targets(267,5): error NETSDK1005: 
Assets file '~/repos/WPinternals/WPinternals/obj/project.assets.json' doesn't have a target for 'netstandard'. 
Ensure that restore has run and that you have included 'netstandard' in the TargetFrameworks for your project. [<path to .csproj file>]
```

What this indicated to me from both the previously mentioned resource and the error messages was that we needed to figure out target frameworks or *something* for using the `mono` framework rater than however the fuck it's using WinForms right now with some presumably legacy means of including the WinForms-including framework in the build when building for the `windows` platform.

Reading the [MS Documentation](https://docs.microsoft.com/en-us/dotnet/standard/frameworks) is somewhat helpful in clarifying the framework stuff, but it doesn't yet provide much indication of how to target `Mono`, though trying for `net48` seems like a shot since that's the largest prefix I found in the `mono` API files at `/usr/lib/mono`. This, however, didn't work, and there was no package for the dotnet SDK v48.

More searching and I found [this stackoverflow thread](https://stackoverflow.com/questions/42747722/building-vs-2017-msbuild-csproj-projects-with-mono-on-linux), and the first thing I was going to try was the non-horrible answer. To be honest the entire build system has been an unmitigated headache. A

fter some more reading and crawling through documentation, I eventually decided the most viable way to build it was probably to instead build a *framework dependent executable* that is ostensibly for windows, then try to run it indirectly with the `mono` framework+runtime - since Mono seems to not have proper integration into the `dotnet` build system and instead depends on `Makefiles` that call their own c# compiler, a massive inconvenience since the build system for WPinternals uses the more c#-y standard of a `.csproj` file.

Reading the [documentation on publishing](https://docs.microsoft.com/en-us/dotnet/core/deploying/) makes it pretty clear what command we need to run. 
```
dotnet publish -r win-x64 --self-contained false
```

However, this still gives the error...
```
/usr/share/dotnet/sdk/6.0.102/Sdks/Microsoft.NET.Sdk/targets/Microsoft.NET.Sdk.targets(1191,3): error MSB4019: 
The imported project "/usr/share/dotnet/sdk/6.0.102/Sdks/Microsoft.NET.Sdk.WindowsDesktop/targets/Microsoft.NET.Sdk.WindowsDesktop.targets" was not found. 
Confirm that the expression in the Import declaration ";/usr/share/dotnet/sdk/6.0.102/Sdks/Microsoft.NET.Sdk/targets/../../Microsoft.NET.Sdk.WindowsDesktop/targets/Microsoft.NET.Sdk.WindowsDesktop.targets" is correct, and that the file exists on disk. [<filepath to .csproj file>]
```

Checking that directory indicates that we have a `Microsoft.NET.Sdk/targets/Microsoft.NET.Sdk.Windows.targets`, but no `.WindowsDesktop` anywhere, and to be frank I didn't know where the hell to even get that collection of directories (searching for a target pack was no good, either ;-;).

###### Attempting to Build The Commandline-Only Program
First thing I tried was setting `useWPF` and `useWindowsForms` from `true` to `false`.

Then I removed - `*.xaml.cs`, `View/*.xaml.cs` `ViewModel`, `FolderSelectDialog.cs`, after which I systematically stripped out all things in `HelperClasses.cs` dependent upon `System.Windows`.

