---
title: "Just Getting Started"
date: 2021-10-13T18:40:19+01:00
draft: false
tags: ["personal"]
---
Hey folks, I'm going to be writing all kinds of fun things on this site, all with associated tags (I had a lot of fun coming up with a CSS-only filter system):
* Philosophy and Metaphysics-y
* Politics and Political Strategy.
* Transhumanism and Futurism.
* Neurodiversity & LGBT+ related issues.
* Development and Sysadmin topics.
* Privacy, Security, and Linux.
* Maths and Science.
* General Tips and Tricks.
* My Personal Life.
* An assortment of other topics.

This is my first post, so hopefully it gives a good idea of the sorts of things you will be able to read here.
