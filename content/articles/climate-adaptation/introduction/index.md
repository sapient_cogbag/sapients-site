+++
title = "Introduction"
draft = false
series = ["climate-adaptation"]
or = "why adapting matters more than most people think"
+++

As anyone who hasn't been living under a rock the past 20 years would know, climate change is already here and already a threat, thanks in major part to obstruction and obfuscation of it's long-term risk and effects by fossil fuel companies and the far-right.

Not only this, but the temperature effects of CO2 are well known to have about a [10-20 year lag (this paper is one example, there are many others)](https://iopscience.iop.org/article/10.1088/1748-9326/10/3/031001), which means the current temperature rise relative to pre-industrial levels (about 1.1°C, at time of writing) is not the total warming we have locked in.

Furthermore, [the sixth International Panel for Climate Change report](https://en.wikipedia.org/wiki/IPCC_Sixth_Assessment_Report), summarised by wikipedia on the linked page, indicates we are - with current actions - likely to head towards 2.0°C-2.7°C degrees of warming in the next century, long term looking at up to 3.5°C. The more unlikely scenarios include much more extreme warming even in the 21st century, up to 4.4°C.

Even the lowest temperature of likely warming - 2.0°C - is estimated to increase the freqency of "twice a century" heatwaves to a hefty once every 5 years minimum and once every 1.5 years at the higher ranges of warming (3°C to 4°C). It is also important to note that this baseline increase in frequency goes up for less likely events - that is, climate is a generally stochastic process in which increased energy to the system pushes up the extremes it can reach, at least for heatwaves. 

This means that even much more intense heatwaves (and other events) are likely to occur at significantly increased rates as well.

We aren't just talking about heatwaves, though. There are also significant risks from ocean rise - in particular to coastal cities, because even if the city is above water right now and would be with oceans rising, flooding - like heatwaves - is a stochastic process where a small increase in the baseline level (that is, sea level), hikes up the probability of intense flooding dramatically because the threshold of precipitation or storms to be "intense enough" to induce flooding has decreased. 

And since smaller deviations from the norm are vastly more frequent than larger deviations in the weather, the chance of extremes goes up massively. The predicted increase in heavy precipitation from the IPCC report is only the cherry on top, here.

Both of these are large risks to vital resources needed for the survival of billions of people - things like agriculture, energy production, transport - as well as directly to our health. Climate action - in the view of the author - is primarily focused on preventing CO2 emissions, which is extremely important to reduce further more catastrophic warming. 

However, the importance of climate adaptation - even in the face of solely the warming we have currently locked in - is often severely underrepresented despite it's necessity for the sake of the lives of hundreds of millions if not billions of people across the planet. 

This series intends to place a focus on doing exactly that.
