---
title: "Microwave Cooking Guide"
date: 2022-01-07T17:31:15Z
draft: false
tags: ["food", "cooking", "recipes", "cooking-capability: microwave", "cooking-capability: boiling-water", "cooking-capability: hot-water", "cooking-capability: cold-water"]
---
### What is this page?
This page contains data and information on using a microwave or similar device to cook food that is not normally done in a microwave.

It will be updated with new recipes and information over time.
