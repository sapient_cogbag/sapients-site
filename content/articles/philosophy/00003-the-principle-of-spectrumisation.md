---
title: "The Principle of Spectrumisation"
date: 2022-06-16T18:18:05
draft: true
---
The term "spectrumisation" is one I came up with myself, as far as I know (though I'm sure someone has had the idea already), and it refers to two separate processes-of-thought which are close in basic principle. It all started when I began examining my mental model of gender and the way other people thought about it - this is the original application of spectrumisation but the process-of-thought is fairly usable for any arbitrary collection of possible labels, concepts, or similar.

##### Scalar Spectrumisation
To examine spectrumisation, we will start with the "cisheteronormative" gender identity model (not addressing gender roles), and move forward from there. In the cisheteronormative model, we would have two genders ("man" and "woman"), per se[^other-society-notes]. The first step, then, is to recognise the possibility of "drawing a line" between them, to construct a simple scalar *spectrum of gender*. This is a useful property for any number of analyses of the concepts, though in this case is far too unsophisticated for notions of gender. This process of recognition I term {{< special >}}scalar spectrumisation{{</ special >}}.

However, in some cases, mentally re-recognising a binary dichotomy as a simple scalar spectrum (scalar because it's 1-dimensional or a line), can be useful on it's own. In this example case - that of gender-identity - it is woefully insufficient. One way of recognising this is to note when there is not one clear way to combine the two ends - for example, in the case of a "man <-> woman" spectrum, the middle point could be bigender, or agender, or some similar identity - another is to note that many societies have some conceptualisations of gender not lieing between those two or in a combination of those two.


##### Vector Spectrumisation
While in some cases scalar spectrumisation is a sufficient re-organisation of some conception, idea, phenomenon, etc. when expanding a model, in most cases it is insufficient. In the case of examining gender identity, this is made very clear in multiple ways as described above. In other cases, the use of vector spectrumisation as a process even when not strictly necessary as a means of describing existing systems, structures, ideas, or similar, could be used to examine new frontiers of possibility.

The method of vector spectrumisation is a means I developed of universalisation free of simplification and encompasses an abstract space of arbitrary and modifiable possibility via adding any instantiation of a category, concept, identity, state, etc. as a vector in some collection of all possible categories, concepts, identities, states, etc., then attempting to break it apart into combined components if possible to derive basis vectors.

In the example of gender identity, we can initialise our space of gender identities with the axis vectors $\ket{man}$; and $\ket{woman}$;. This alone allows us to describe the identity of most people describing themselves as bigender (some may use some other pair of identities, or have an uneven distribution of identity), e.g. $a(\ket{man} + \ket{woman})$;. Anyone agender can be described by the zero vector $\vec{0}$; for the space (though this may also be described as not even having a gender identity in the first place, and have $\vec{0}$; be neutrois or similar, this is dependent on how people describe it). Demiboys and demigirls could theoretically be described here as $a(\ket{man}); 0 < a < 1; a \in \Reals$;, and $a(\ket{woman}); 0 < a < 1; a \in \Reals$;, respectively.

When we talk then about nonbinary identities, it is fairly simple to add in some other identities and pull out some potential basis vectors. For instance, I myself describe myself as slightly femme-leaning maverique - that is, I have a strong sense of gender that is neither $\ket{man}$; or $\ket{woman}$;, and instead some distinct as-yet-unlabeled other gender. In this case, my gender identity can probably be described as roughly $\ket{other_{maverique}} + 0.2\ket{woman}$;.

This general process of vector spectrumisation is generalisable to much more than just gender, it is certainly applicable to things like notions of "autism spectrum", gender presentation, gender roles, arbitrary identities and cultural phenomena, and anything else.

##### Complexity and Variation
So far, I've written this in precise terms like $GenderIdentity = \ket{other_{maverique}} + 0.2\ket{woman}$;, or $GenderIdentity = \ket{man}$;, or $GenderIdentity = \vec{0}$;, or perhaps similarly for something other than gender identity.

This is a useful start when describing simpler things, but for human systems and identity labelling/comprehension








[^other-society-notes]: Note that this is talking about 1950s-era american and "western" stuff here. Other societies may have 3 or more genders, or combinations, or various other systems - these can be more directly integrated into a model of gender more complex than the initial baseline model we are using here.
