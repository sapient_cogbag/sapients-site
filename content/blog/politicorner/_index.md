---
title: "The Politi-Corner"
date: 2022-01-30T03:05:22Z
shorttitle: "politicorner"
draft: false
flatten: true
tags: ["politics"]
cascade:
  tags: ["politics"]
---

This is where I write about politics and political philosophy or just general philosophy if it relates to politics. Prepare for spicy transhumanism, lgbt+ rights, futurism and more.


