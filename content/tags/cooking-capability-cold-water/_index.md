+++
title = "Cooking Capability: Cold Water"
default-hide-tag.cooking-capability-cold-water= false
+++

All Adaptable Cooking pages listed here require at least ready access to drinkable cold water to be relevant to the reader.
