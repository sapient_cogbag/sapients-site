+++
title = "personal"
default-hide-tag.personal= false
+++

All pages tagged personal.

This tag is used to tag things primarily orienting around my personal experiences and life in general, most often to be in [the blog section](/blog).
