---
title: "Guides"
date: 2022-01-07T19:22:54Z
draft: false
flatten: false
shorttitle: "guides"
---
Page for all the guides - at varying levels of cooking capability.

Recommended reading order is from lowest capability - the No Cooking Eating Guide - up to the highest capability.

Note that the pages listed here are prone to change and updates - several are empty for the moment, they will not stay that way. 

